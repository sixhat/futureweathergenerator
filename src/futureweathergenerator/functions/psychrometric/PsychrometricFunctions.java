/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions.psychrometric;

/**
 * Calculates various properties of moist air. Source: ASHRAE Fundamentals,
 * 2005, SI Edition Singh et al. "Numerical Calculations of Psychrometric
 * Properties on a Calculator". Building and Environment, 37, 2002.
 *
 * @author eugenio
 */
public class PsychrometricFunctions {

    /**
     * Computes partial vapour pressure in [kPa].
     *
     * @param ambient_pressure Ambient Pressure [kPa]
     * @param humidity_ratio Humidity Ratio [kg/kg dry air]
     * @return Partial Vapour Pressure [kPa]
     */
    public static double getPartialVapourPressure(double ambient_pressure, double humidity_ratio) {
        return ambient_pressure * humidity_ratio / (0.62198 + humidity_ratio);
    }

    /**
     * Computes saturation vapour pressure in [kPa].
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @return Saturation Vapour Pressure [kPa]
     */
    public static double getSaturationVapourPressure(double dry_bulb_temperature) {
        double c1 = -5674.5359;
        double c2 = 6.3925247;
        double c3 = -0.009677843;
        double c4 = 0.00000062215701;
        double c5 = 0.0000000020747825;
        double c6 = -0.0000000000009484024;
        double c7 = 4.1635019;
        double c8 = -5800.2206;
        double c9 = 1.3914993;
        double c10 = -0.048640239;
        double c11 = 0.000041764768;
        double c12 = -0.000000014452093;
        double c13 = 6.5459673;

        double dbtk = dry_bulb_temperature + 273.15;

        if (dbtk <= 273.15) {
            return Math.exp(c1 / dbtk + c2 + c3 * dbtk + c4 * Math.pow(dbtk, 2) + c5 * Math.pow(dbtk, 3) + c6 * Math.pow(dbtk, 4) + c7 * Math.log(dbtk)) / 1000.0;
        } else {
            return Math.exp(c8 / dbtk + c9 + c10 * dbtk + c11 * Math.pow(dbtk, 2) + c12 * Math.pow(dbtk, 3) + c13 * Math.log(dbtk)) / 1000.0;
        }
    }

    /**
     * Computes humidity ratio [kg H2O/kg air].
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @param wet_bulb_temperature Wet Bulb Temperature [°C]
     * @param ambient_pressure Ambient Pressure [kPa]
     * @return Humidity Ratio [kg water/kg air]
     */
    public static double getHumidityRatio(double dry_bulb_temperature, double wet_bulb_temperature, double ambient_pressure) {
        double saturation_vapor_pressure = getSaturationVapourPressure(dry_bulb_temperature);
        double ws = 0.62198 * saturation_vapor_pressure / (ambient_pressure - saturation_vapor_pressure);
        if (dry_bulb_temperature >= 0) {
            return (((2501 - 2.326 * wet_bulb_temperature) * ws - 1.006 * (dry_bulb_temperature - wet_bulb_temperature)) / (2501 + 1.86 * dry_bulb_temperature - 4.186 * wet_bulb_temperature));
        } else {
            return (((2830 - 0.24 * wet_bulb_temperature) * ws - 1.006 * (dry_bulb_temperature - wet_bulb_temperature)) / (2830 + 1.86 * dry_bulb_temperature - 2.1 * wet_bulb_temperature));
        }
    }

    /**
     * Computes humidity ratio [kg H2O/kg air].
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @param relative_humidity Relative Humidity [%]
     * @param ambient_pressure Ambient Pressure [kPa]
     * @return Humidity Ratio [kg water/kg air]
     */
    public static double getHumidityRatio2(double dry_bulb_temperature, double relative_humidity, double ambient_pressure) {
        double saturation_vapour_pressure = getSaturationVapourPressure(dry_bulb_temperature);
        return 0.62198 * relative_humidity * saturation_vapour_pressure / (ambient_pressure - relative_humidity * saturation_vapour_pressure);
    }

    /**
     * Calculate relative humidity [%].
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @param wet_bulb_temperature Wet Bulb Temperature [°C]
     * @param ambient_pressure Ambient Pressure [kPa]
     * @return Relative Humidity [%]
     */
    public static double getRelativeHumidity(double dry_bulb_temperature, double wet_bulb_temperature, double ambient_pressure) {
        double humidity_ratio = getHumidityRatio(dry_bulb_temperature, wet_bulb_temperature, ambient_pressure);
        return getPartialVapourPressure(ambient_pressure, humidity_ratio) / getSaturationVapourPressure(dry_bulb_temperature);
    }

    /**
     * Computes the relative humidity [%].
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @param ambient_pressure Ambient Pressure [kPa]
     * @param humidity_ratio Relative Humidity [%]
     * @return Relative Humidity [%]
     */
    public static double getRelativeHumidity2(double dry_bulb_temperature, double ambient_pressure, double humidity_ratio) {
        double partial_vapour_pressure = getPartialVapourPressure(ambient_pressure, humidity_ratio);
        double saturation_vapour_pressure = getSaturationVapourPressure(dry_bulb_temperature);
        return partial_vapour_pressure / saturation_vapour_pressure;
    }

    /**
     * Computes the dew point temperature [°C].
     *
     * @param ambient_pressure Ambient Pressure [kPa]
     * @param humidity_ratio Humidity Ratio [kg/kg dry air]
     * @return Dew point temperature [°C]
     */
    public static double getDewPointTemperature(double ambient_pressure, double humidity_ratio) {
        double c14 = 6.54;
        double c15 = 14.526;
        double c16 = 0.7389;
        double c17 = 0.09486;
        double c18 = 0.4569;

        double partial_vapour_pressure = getPartialVapourPressure(ambient_pressure, humidity_ratio);
        double alpha = Math.log(partial_vapour_pressure);
        double dpt1 = c14 + c15 * alpha + c16 * Math.pow(alpha, 2) + c17 * Math.pow(alpha, 3) + c18 * Math.pow(partial_vapour_pressure, 0.1984);
        double dpt2 = 6.09 + 12.608 * alpha + 0.4959 * Math.pow(alpha, 2);

        return dpt1 >= 0 ? dpt1 : dpt2;
    }

    /**
     * Computes Wet Bulb Temperature [°C] using Newton-Rhapson
     * iteration to converge quickly. Solves to within 0.001 % accuracy.
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @param relative_humidity Relative Humidity [%]
     * @param ambient_pressure Ambient Pressure [kPa]
     * @return Wet Bulb Temperature [°C]
     */
    public static double getWetBulbTemperature(double dry_bulb_temperature, double relative_humidity, double ambient_pressure) {
        double w_normal = getHumidityRatio2(dry_bulb_temperature, relative_humidity, ambient_pressure);
        double result = dry_bulb_temperature;
        double w_new = getHumidityRatio(dry_bulb_temperature, result, ambient_pressure);
        while (Math.abs(w_new - w_normal) / w_normal > 0.00001) {
            double w_new2 = getHumidityRatio(dry_bulb_temperature, result - 0.001, ambient_pressure);
            double dw_dtwb = (w_new - w_new2) / 0.001;
            result = result - (w_new - w_normal) / dw_dtwb;
            w_new = getHumidityRatio(dry_bulb_temperature, result, ambient_pressure);
        }
        return result;
    }

    /**
     * Computes Dry Bulb Temperature [ºC] back from Enthalpy
     * [kJ/kg] and humidity ratio [kg/kg dry air].
     *
     * @param enthalpy Enthalpy [kJ/kg]
     * @param humidity_ratio Humidity Ratio [kg/kg dry air]
     * @return Dry Bulb Temperature [ºC]
     */
    public static double getDryBulbTemperature(double enthalpy, double humidity_ratio) {
        return (enthalpy - (2501 * humidity_ratio)) / (1.006 + (1.86 * humidity_ratio));
    }

    /**
     * Computes the Dry Air Density [kg dry air/m3].
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @param ambient_pressure Ambient Pressure [kPa]
     * @param humidity_ratio Humidity Ratio [kg/kg dry air]
     * @return Dry Air Density [kg dry air/m3]
     */
    public static double getDryAirDensity(double dry_bulb_temperature, double ambient_pressure, double humidity_ratio) {
        return 1000.0 * ambient_pressure / (287.055 * (273.15 + dry_bulb_temperature) * (1 + 1.6078 * humidity_ratio));
    }

    /**
     * Computes Enthalpy [kJ/kg dry air].
     *
     * @param dry_bulb_temperature Dry Bulb Temperature [°C]
     * @param humidity_ratio Humidity Ratio [kg/kg dry air]
     * @return Enthalpy [kJ/kg dry air]
     */
    public static double getEnthalpy(double dry_bulb_temperature, double humidity_ratio) {
        return 1.006 * dry_bulb_temperature + humidity_ratio * (2501 + 1.86 * dry_bulb_temperature);
    }

    /**
     * Returns absolute pressure of water vapour at saturation from dry bulb
     * temperature.
     *
     * @param dry_bulb_temperature dry bulb temperature [°C]
     * @return absolute pressure of water vapour at saturation
     */
    public static double getAbsolutePressureOfWaterVapourAtSaturation(double dry_bulb_temperature) {
        int ndbt = (int) dry_bulb_temperature;
        switch (ndbt) {
            case -60 -> {
                return 0.00108;
            }
            case -59 -> {
                return 0.00124;
            }
            case -58 -> {
                return 0.00141;
            }
            case -57 -> {
                return 0.00161;
            }
            case -56 -> {
                return 0.00184;
            }
            case -55 -> {
                return 0.00209;
            }
            case -54 -> {
                return 0.00238;
            }
            case -53 -> {
                return 0.00271;
            }
            case -52 -> {
                return 0.00307;
            }
            case -51 -> {
                return 0.00348;
            }
            case -50 -> {
                return 0.00394;
            }
            case -49 -> {
                return 0.00445;
            }
            case -48 -> {
                return 0.00503;
            }
            case -47 -> {
                return 0.00568;
            }
            case -46 -> {
                return 0.0064;
            }
            case -45 -> {
                return 0.00721;
            }
            case -44 -> {
                return 0.00811;
            }
            case -43 -> {
                return 0.00911;
            }
            case -42 -> {
                return 0.01022;
            }
            case -41 -> {
                return 0.01147;
            }
            case -40 -> {
                return 0.01285;
            }
            case -39 -> {
                return 0.01438;
            }
            case -38 -> {
                return 0.01608;
            }
            case -37 -> {
                return 0.01796;
            }
            case -36 -> {
                return 0.02004;
            }
            case -35 -> {
                return 0.02235;
            }
            case -34 -> {
                return 0.0249;
            }
            case -33 -> {
                return 0.02771;
            }
            case -32 -> {
                return 0.03082;
            }
            case -31 -> {
                return 0.03424;
            }
            case -30 -> {
                return 0.03802;
            }
            case -29 -> {
                return 0.04217;
            }
            case -28 -> {
                return 0.04673;
            }
            case -27 -> {
                return 0.05174;
            }
            case -26 -> {
                return 0.05725;
            }
            case -25 -> {
                return 0.06329;
            }
            case -24 -> {
                return 0.06991;
            }
            case -23 -> {
                return 0.07716;
            }
            case -22 -> {
                return 0.0851;
            }
            case -21 -> {
                return 0.09378;
            }
            case -20 -> {
                return 0.10326;
            }
            case -19 -> {
                return 0.11362;
            }
            case -18 -> {
                return 0.12492;
            }
            case -17 -> {
                return 0.13725;
            }
            case -16 -> {
                return 0.15068;
            }
            case -15 -> {
                return 0.1653;
            }
            case -14 -> {
                return 0.18192;
            }
            case -13 -> {
                return 0.19852;
            }
            case -12 -> {
                return 0.21732;
            }
            case -11 -> {
                return 0.23774;
            }
            case -10 -> {
                return 0.2599;
            }
            case -9 -> {
                return 0.28393;
            }
            case -8 -> {
                return 0.30998;
            }
            case -7 -> {
                return 0.33819;
            }
            case -6 -> {
                return 0.36874;
            }
            case -5 -> {
                return 0.40176;
            }
            case -4 -> {
                return 0.43747;
            }
            case -3 -> {
                return 0.47606;
            }
            case -2 -> {
                return 0.51772;
            }
            case -1 -> {
                return 0.56267;
            }
            case 0 -> {
                return 0.61115;
            }
            case 1 -> {
                return 0.6571;
            }
            case 2 -> {
                return 0.706;
            }
            case 3 -> {
                return 0.758;
            }
            case 4 -> {
                return 0.8135;
            }
            case 5 -> {
                return 0.8725;
            }
            case 6 -> {
                return 0.9373;
            }
            case 7 -> {
                return 1.002;
            }
            case 8 -> {
                return 1.0728;
            }
            case 9 -> {
                return 1.1481;
            }
            case 10 -> {
                return 1.228;
            }
            case 11 -> {
                return 1.3127;
            }
            case 12 -> {
                return 1.4026;
            }
            case 13 -> {
                return 1.4978;
            }
            case 14 -> {
                return 1.5987;
            }
            case 15 -> {
                return 1.7055;
            }
            case 16 -> {
                return 1.8184;
            }
            case 17 -> {
                return 1.938;
            }
            case 18 -> {
                return 2.0643;
            }
            case 19 -> {
                return 2.1978;
            }
            case 20 -> {
                return 2.3388;
            }
            case 21 -> {
                return 2.4877;
            }
            case 22 -> {
                return 2.6448;
            }
            case 23 -> {
                return 2.8104;
            }
            case 24 -> {
                return 2.9851;
            }
            case 25 -> {
                return 3.1692;
            }
            case 26 -> {
                return 3.3631;
            }
            case 27 -> {
                return 3.5673;
            }
            case 28 -> {
                return 3.7822;
            }
            case 29 -> {
                return 4.0083;
            }
            case 30 -> {
                return 4.246;
            }
            case 31 -> {
                return 4.4959;
            }
            case 32 -> {
                return 4.7585;
            }
            case 33 -> {
                return 5.0343;
            }
            case 34 -> {
                return 5.3239;
            }
            case 35 -> {
                return 5.6278;
            }
            case 36 -> {
                return 5.9466;
            }
            case 37 -> {
                return 6.281;
            }
            case 38 -> {
                return 6.6315;
            }
            case 39 -> {
                return 6.9987;
            }
            case 40 -> {
                return 7.3835;
            }
            case 41 -> {
                return 7.7863;
            }
            case 42 -> {
                return 8.208;
            }
            case 43 -> {
                return 8.6492;
            }
            case 44 -> {
                return 9.1107;
            }
            case 45 -> {
                return 9.5932;
            }
            case 46 -> {
                return 10.0976;
            }
            case 47 -> {
                return 10.6246;
            }
            case 48 -> {
                return 11.1751;
            }
            case 49 -> {
                return 11.75;
            }
            case 50 -> {
                return 12.3499;
            }
            case 51 -> {
                return 12.9759;
            }
            case 52 -> {
                return 13.629;
            }
            case 53 -> {
                return 14.31;
            }
            case 54 -> {
                return 15.02;
            }
            case 55 -> {
                return 15.7597;
            }
            case 56 -> {
                return 16.5304;
            }
            case 57 -> {
                return 17.3331;
            }
            case 58 -> {
                return 18.169;
            }
            case 59 -> {
                return 19.0387;
            }
            case 60 -> {
                return 19.944;
            }
            case 61 -> {
                return 20.885;
            }
            case 62 -> {
                return 21.864;
            }
            case 63 -> {
                return 22.882;
            }
            case 64 -> {
                return 23.94;
            }
            case 65 -> {
                return 25.04;
            }
            case 66 -> {
                return 26.18;
            }
            case 67 -> {
                return 27.366;
            }
            case 68 -> {
                return 28.596;
            }
            case 69 -> {
                return 29.873;
            }
            default -> {
                throw new UnsupportedOperationException("Error: Dry bulb temperature out of bounds: " + dry_bulb_temperature);
            }
        }
    }
}
