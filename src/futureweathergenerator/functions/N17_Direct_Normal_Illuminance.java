/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.PerezFunctions;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;

/**
 * Calculates N17 direct normal illuminance from solar model, future
 * extraterrestrial direct normal radiation, future direct normal radiation,
 * future diffuse horizontal radiation, and future dew point temperature.
 *
 * @author eugenio
 */
public class N17_Direct_Normal_Illuminance {

    /**
     * Calculates N17 direct normal illuminance from solar model, future
     * extraterrestrial direct normal radiation, future direct normal radiation,
     * future diffuse horizontal radiation, and future dew point temperature.
     *
     * @param epw current EPW object
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW epw, EPW morphedEPW) {
        // Requires pre-processing of:
        // N7 Dew Point Temperature
        // N11 Extraterrestrial Direct Normal Radiation
        // N14 Direct Normal Radiation
        // N15 Diffuse Horizontal Radiation
        //
        // NOTICE:
        // CCWorldWeatherGen outputs different values for the 
        // illuminances and luminance (N16 to N19 fields).
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            double longitude = epw.getEpw_location().getN3_longitude();
            double time_zone = epw.getEpw_location().getN4_time_zone();
            double latitude = epw.getEpw_location().getN2_latitude();
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double future_diffuse_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN15_diffuse_horizontal_radiation();
                double future_extraterrestrial_direct_normal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN11_extraterrestrial_direct_normal_radiation();
                if (future_extraterrestrial_direct_normal_radiation > 0 && future_diffuse_horizontal_radiation > 0) {
                    double future_direct_normal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN14_direct_normal_radiation();
                    double future_dew_point_temperature = morphedEPW.getEpw_data_fields().get(row_id).getN7_dew_point_temperature();
                    double day_number = (int) Math.floor(row_id / 24.0) + 1;
                    double day_angle = SolarFunctions.getDayAngle(day_number);
                    double equation_of_time = SolarFunctions.getEquationOfTime(day_angle);
                    double declination = SolarFunctions.getDeclination(day_angle);
                    double local_standard_time = epw.getEpw_data_fields().get(row_id).getN4_hour();
                    double sin_Solar_Altitude_Angle_sum = 0.0;
                    double minutes = 0.0;
                    for (int min = 1; min <= 60; min++) {
                        double solar_time = SolarFunctions.getSolarTime(local_standard_time - 1 + (min / 60.0), longitude, time_zone, equation_of_time);
                        double hour_angle = SolarFunctions.getHourAngle(solar_time);
                        double sin_Solar_Altitude_Angle_step = SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                        if (sin_Solar_Altitude_Angle_step > 0) {
                            sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                            minutes++;
                        }
                    }
                    if (minutes != 0) {
                        double sin_Solar_Altitude_Angle = sin_Solar_Altitude_Angle_sum / minutes;
                        double solar_altitude_angle = SolarFunctions.getSolarAltitudeAngle(sin_Solar_Altitude_Angle);
                        solar_altitude_angle = solar_altitude_angle < 0.0 ? 0.0 : solar_altitude_angle;
                        double solar_zenith_angle = SolarFunctions.getSolarZenithAngle(solar_altitude_angle);
                        double precipitable_water_content = Math.exp((0.07 * future_dew_point_temperature) - 0.075);
                        double relative_optical_air_mass = PerezFunctions.getRelativeOpticalAirMass(solar_altitude_angle);
                        double sky_brightness = future_diffuse_horizontal_radiation * relative_optical_air_mass / future_extraterrestrial_direct_normal_radiation;
                        double sky_clearness = ((future_diffuse_horizontal_radiation + future_direct_normal_radiation) / future_diffuse_horizontal_radiation +
                                (1.041 * Math.pow(solar_zenith_angle, 3))) /
                                (1 + 1.041 * Math.pow(solar_zenith_angle, 3));
                        int sky_clearness_category = PerezFunctions.getDiscreteSkyClearnessCategory(sky_clearness);
                        double[] c = PerezFunctions.getDirectLuminousEfficacy(sky_clearness_category);
                        double calculated_direct_normal_illuminance = future_direct_normal_radiation
                                * (c[0]
                                + (c[1] * precipitable_water_content)
                                + (c[2] * Math.exp(5.73 * solar_zenith_angle - 5.0))
                                + (c[3] * sky_brightness));
                        morphedEPW.getEpw_data_fields().get(row_id).setN17_direct_normal_illuminance((float) calculated_direct_normal_illuminance);
                        if (morphedEPW.getEpw_data_fields().get(row_id).getN17_direct_normal_illuminance().isNaN()) {
                            morphedEPW.getEpw_data_fields().get(row_id).setN17_direct_normal_illuminance(0.0f);
                        }
                    } else {
                        morphedEPW.getEpw_data_fields().get(row_id).setN17_direct_normal_illuminance(0.0f);
                    }
                } else {
                    morphedEPW.getEpw_data_fields().get(row_id).setN17_direct_normal_illuminance(0.0f);
                }
            }
        }
    }
}
