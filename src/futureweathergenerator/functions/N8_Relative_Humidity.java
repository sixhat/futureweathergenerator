/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.Months;

/**
 * Morphs N8 relative humidity.
 *
 * @author eugenio
 */
public class N8_Relative_Humidity {

    /**
     * Morphs N8 relative humidity.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, AverageScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float month_relative_humidity_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_relative_humidity(), numberOfHoursToSmooth);
                float month_maximum_daily_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_relative_humidity_step, numberOfHoursToSmooth);
                float month_relative_humidity = scenario.getInterpolated_relative_humidity()[i] + month_maximum_daily_temperature_delta;
                float calculated_relative_humidity = Math.round(epw.getEpw_data_fields().get(row_id).getN8_relative_humidity() + month_relative_humidity);
                morphedEPW.getEpw_data_fields().get(row_id).setN8_relative_humidity((float) calculated_relative_humidity);
            }
        }
    }
}
