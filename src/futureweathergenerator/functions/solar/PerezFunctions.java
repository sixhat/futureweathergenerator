/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions.solar;

/**
 * Presents a set of Perez Model functions. The source of these functions is
 * ﻿Perez R, Ineichen P, Seals R, Michalsky J, Stewart R. Modeling daylight
 * availability and irradiance components from direct and global irradiance. Sol
 * Energy 1990;44:271–89. https://doi.org/10.1016/0038-092X(90)90055-H. and
 * relative optical air mass from Kasten F, Young AT. Revised Optical Air Mass
 * Tables. Appl Opt 2000;28:4735–8.
 *
 * @author eugenio
 */
public class PerezFunctions {

    /**
     * Returns discrete sky clearness category [1 to 8] from sky clearness.
     *
     * @param sky_clearness sky clearness
     * @return discrete sky clearness category
     */
    public static int getDiscreteSkyClearnessCategory(double sky_clearness) {
        if (sky_clearness < 1.065) { // Perez paper actually states sky clearness above minimum 1.0.
            return 1;
        } else if (sky_clearness >= 1.065 && sky_clearness < 1.230) {
            return 2;
        } else if (sky_clearness >= 1.230 && sky_clearness < 1.500) {
            return 3;
        } else if (sky_clearness >= 1.500 && sky_clearness < 1.950) {
            return 4;
        } else if (sky_clearness >= 1.950 && sky_clearness < 2.800) {
            return 5;
        } else if (sky_clearness >= 2.800 && sky_clearness < 4.500) {
            return 6;
        } else if (sky_clearness >= 4.500 && sky_clearness < 6.200) {
            return 7;
        } else if (sky_clearness >= 6.200) {
            return 8;
        } else {
            throw new UnsupportedOperationException("Error: Sky Clearness out of bounds: " + sky_clearness);
        }
    }

    /**
     * Returns the global luminous efficacy coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return global luminous efficacy coefficients
     */
    public static double[] getGlobalLuminousEfficacy(int sky_clearness_category) {
        double[][] gle = new double[8][];
        gle[0] = new double[]{96.63, -0.47, 11.5, -9.16};
        gle[1] = new double[]{107.54, 0.79, 1.79, -1.19};
        gle[2] = new double[]{98.73, 0.7, 4.4, -6.95};
        gle[3] = new double[]{92.72, 0.56, 8.36, -8.31};
        gle[4] = new double[]{86.73, 0.98, 7.1, -10.94};
        gle[5] = new double[]{88.34, 1.39, 6.06, -7.6};
        gle[6] = new double[]{78.63, 1.47, 4.93, -11.37};
        gle[7] = new double[]{99.65, 1.86, -4.46, -3.15};
        return gle[sky_clearness_category - 1];
    }

    /**
     * Returns diffuse luminous efficacy coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return diffuse luminous efficacy coefficients
     */
    public static double[] getDiffuseLuminousEfficacy(int sky_clearness_category) {
        double[][] dle = new double[8][];
        dle[0] = new double[]{97.24, -0.46, 12, -8.91};
        dle[1] = new double[]{107.22, 1.15, 0.59, -3.95};
        dle[2] = new double[]{104.97, 2.96, -5.53, -8.77};
        dle[3] = new double[]{102.39, 5.59, -13.95, -13.9};
        dle[4] = new double[]{100.71, 5.94, -22.75, -23.74};
        dle[5] = new double[]{106.42, 3.83, -36.15, -28.83};
        dle[6] = new double[]{141.88, 1.9, -53.24, -14.03};
        dle[7] = new double[]{152.23, 0.35, -45.27, -7.98};
        return dle[sky_clearness_category - 1];
    }

    /**
     * Returns direct luminous efficacy coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return direct luminous efficacy coefficients
     */
    public static double[] getDirectLuminousEfficacy(int sky_clearness_category) {
        double[][] dle = new double[8][];
        dle[0] = new double[]{57.2, -4.55, -2.98, 117.12};
        dle[1] = new double[]{98.99, -3.46, -1.21, 12.38};
        dle[2] = new double[]{109.83, -4.9, -1.71, -8.81};
        dle[3] = new double[]{110.34, -5.84, -1.99, -4.56};
        dle[4] = new double[]{106.36, -3.97, -1.75, -6.16};
        dle[5] = new double[]{107.19, -1.25, -1.51, -26.73};
        dle[6] = new double[]{105.75, 0.77, -1.26, -34.44};
        dle[7] = new double[]{101.18, 1.58, -1.1, -8.29};
        return dle[sky_clearness_category - 1];
    }

    /**
     * Returns zenith luminance prediction coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return zenith luminance prediction coefficients
     */
    public static double[] getZenithLuminancePrediction(int sky_clearness_category) {
        double[][] zlp = new double[8][];
        zlp[0] = new double[]{40.86, 26.77, -29.59, -45.75};
        zlp[1] = new double[]{26.58, 14.73, 58.46, -21.25};
        zlp[2] = new double[]{19.34, 2.28, 100, 0.25};
        zlp[3] = new double[]{13.25, -1.39, 124.79, 15.66};
        zlp[4] = new double[]{14.47, -5.09, 160.09, 9.13};
        zlp[5] = new double[]{19.76, -3.88, 154.61, -19.21};
        zlp[6] = new double[]{28.39, -9.67, 151.58, -69.39};
        zlp[7] = new double[]{42.91, -19.62, 130.8, -164.08};
        return zlp[sky_clearness_category - 1];
    }

    /**
     * Returns relative optical air mass from solar altitude angle.
     *
     * @param solar_altitude_angle solar altitude angle
     * @return relative optical air mass
     */
    public static double getRelativeOpticalAirMass(double solar_altitude_angle) {
        String val = String.format("%.1f", solar_altitude_angle);
        switch (val.replace(",", ".")) {
            case "0.0" -> {
                return 38.0868;
            }
            case "0.1" -> {
                return 36.5581;
            }
            case "0.2" -> {
                return 35.1223;
            }
            case "0.3" -> {
                return 33.7726;
            }
            case "0.4" -> {
                return 32.5026;
            }
            case "0.5" -> {
                return 31.3064;
            }
            case "0.6" -> {
                return 30.1786;
            }
            case "0.7" -> {
                return 29.1144;
            }
            case "0.8" -> {
                return 28.1091;
            }
            case "0.9" -> {
                return 27.1588;
            }
            case "1.0" -> {
                return 26.2595;
            }
            case "1.1" -> {
                return 25.4079;
            }
            case "1.2" -> {
                return 24.6006;
            }
            case "1.3" -> {
                return 23.8349;
            }
            case "1.4" -> {
                return 23.1078;
            }
            case "1.5" -> {
                return 22.417;
            }
            case "1.6" -> {
                return 21.7601;
            }
            case "1.7" -> {
                return 21.1349;
            }
            case "1.8" -> {
                return 20.5395;
            }
            case "1.9" -> {
                return 19.972;
            }
            case "2.0" -> {
                return 19.4308;
            }
            case "2.1" -> {
                return 18.9143;
            }
            case "2.2" -> {
                return 18.4209;
            }
            case "2.3" -> {
                return 17.9493;
            }
            case "2.4" -> {
                return 17.4983;
            }
            case "2.5" -> {
                return 17.0667;
            }
            case "2.6" -> {
                return 16.6534;
            }
            case "2.7" -> {
                return 16.2573;
            }
            case "2.8" -> {
                return 15.8775;
            }
            case "2.9" -> {
                return 15.5131;
            }
            case "3.0" -> {
                return 15.1633;
            }
            case "3.1" -> {
                return 14.8273;
            }
            case "3.2" -> {
                return 14.5043;
            }
            case "3.3" -> {
                return 14.1937;
            }
            case "3.4" -> {
                return 13.8949;
            }
            case "3.5" -> {
                return 13.6072;
            }
            case "3.6" -> {
                return 13.3301;
            }
            case "3.7" -> {
                return 13.063;
            }
            case "3.8" -> {
                return 12.8056;
            }
            case "3.9" -> {
                return 12.5572;
            }
            case "4.0" -> {
                return 12.3174;
            }
            case "4.1" -> {
                return 12.0859;
            }
            case "4.2" -> {
                return 11.8623;
            }
            case "4.3" -> {
                return 11.6462;
            }
            case "4.4" -> {
                return 11.4372;
            }
            case "4.5" -> {
                return 11.235;
            }
            case "4.6" -> {
                return 11.0394;
            }
            case "4.7" -> {
                return 10.85;
            }
            case "4.8" -> {
                return 10.6665;
            }
            case "4.9" -> {
                return 10.4887;
            }
            case "5.0" -> {
                return 10.3164;
            }
            case "5.1" -> {
                return 10.1493;
            }
            case "5.2" -> {
                return 9.9872;
            }
            case "5.3" -> {
                return 9.8299;
            }
            case "5.4" -> {
                return 9.6772;
            }
            case "5.5" -> {
                return 9.5289;
            }
            case "5.6" -> {
                return 9.3848;
            }
            case "5.7" -> {
                return 9.2448;
            }
            case "5.8" -> {
                return 9.1087;
            }
            case "5.9" -> {
                return 8.9763;
            }
            case "6.0" -> {
                return 8.8475;
            }
            case "6.1" -> {
                return 8.7222;
            }
            case "6.2" -> {
                return 8.6002;
            }
            case "6.3" -> {
                return 8.4815;
            }
            case "6.4" -> {
                return 8.3658;
            }
            case "6.5" -> {
                return 8.2531;
            }
            case "6.6" -> {
                return 8.1433;
            }
            case "6.7" -> {
                return 8.0363;
            }
            case "6.8" -> {
                return 7.9319;
            }
            case "6.9" -> {
                return 7.83;
            }
            case "7.0" -> {
                return 7.7307;
            }
            case "7.1" -> {
                return 7.6338;
            }
            case "7.2" -> {
                return 7.5392;
            }
            case "7.3" -> {
                return 7.4468;
            }
            case "7.4" -> {
                return 7.3566;
            }
            case "7.5" -> {
                return 7.2684;
            }
            case "7.6" -> {
                return 7.1823;
            }
            case "7.7" -> {
                return 7.0982;
            }
            case "7.8" -> {
                return 7.0159;
            }
            case "7.9" -> {
                return 6.9355;
            }
            case "8.0" -> {
                return 6.8568;
            }
            case "8.1" -> {
                return 6.7799;
            }
            case "8.2" -> {
                return 6.7046;
            }
            case "8.3" -> {
                return 6.631;
            }
            case "8.4" -> {
                return 6.5589;
            }
            case "8.5" -> {
                return 6.4883;
            }
            case "8.6" -> {
                return 6.4192;
            }
            case "8.7" -> {
                return 6.3515;
            }
            case "8.8" -> {
                return 6.2852;
            }
            case "8.9" -> {
                return 6.2202;
            }
            case "9.0" -> {
                return 6.1565;
            }
            case "9.1" -> {
                return 6.0942;
            }
            case "9.2" -> {
                return 6.033;
            }
            case "9.3" -> {
                return 5.973;
            }
            case "9.4" -> {
                return 5.9142;
            }
            case "9.5" -> {
                return 5.8565;
            }
            case "9.6" -> {
                return 5.8;
            }
            case "9.7" -> {
                return 5.7445;
            }
            case "9.8" -> {
                return 5.69;
            }
            case "9.9" -> {
                return 5.6365;
            }
            case "10.0" -> {
                return 5.5841;
            }
            case "10.1" -> {
                return 5.5325;
            }
            case "10.2" -> {
                return 5.482;
            }
            case "10.3" -> {
                return 5.4323;
            }
            case "10.4" -> {
                return 5.3835;
            }
            case "10.5" -> {
                return 5.3356;
            }
            case "10.6" -> {
                return 5.2885;
            }
            case "10.7" -> {
                return 5.2422;
            }
            case "10.8" -> {
                return 5.1968;
            }
            case "10.9" -> {
                return 5.1521;
            }
            case "11.0" -> {
                return 5.1081;
            }
            case "11.1" -> {
                return 5.0649;
            }
            case "11.2" -> {
                return 5.0225;
            }
            case "11.3" -> {
                return 4.9807;
            }
            case "11.4" -> {
                return 4.9396;
            }
            case "11.5" -> {
                return 4.8992;
            }
            case "11.6" -> {
                return 4.8595;
            }
            case "11.7" -> {
                return 4.8204;
            }
            case "11.8" -> {
                return 4.7819;
            }
            case "11.9" -> {
                return 4.744;
            }
            case "12.0" -> {
                return 4.7067;
            }
            case "12.1" -> {
                return 4.67;
            }
            case "12.2" -> {
                return 4.6339;
            }
            case "12.3" -> {
                return 4.5984;
            }
            case "12.4" -> {
                return 4.5633;
            }
            case "12.5" -> {
                return 4.5288;
            }
            case "12.6" -> {
                return 4.4949;
            }
            case "12.7" -> {
                return 4.4614;
            }
            case "12.8" -> {
                return 4.4285;
            }
            case "12.9" -> {
                return 4.396;
            }
            case "13.0" -> {
                return 4.364;
            }
            case "13.1" -> {
                return 4.3325;
            }
            case "13.2" -> {
                return 4.3014;
            }
            case "13.3" -> {
                return 4.2708;
            }
            case "13.4" -> {
                return 4.2406;
            }
            case "13.5" -> {
                return 4.2108;
            }
            case "13.6" -> {
                return 4.1815;
            }
            case "13.7" -> {
                return 4.1526;
            }
            case "13.8" -> {
                return 4.1241;
            }
            case "13.9" -> {
                return 4.0959;
            }
            case "14.0" -> {
                return 4.0682;
            }
            case "14.1" -> {
                return 4.0408;
            }
            case "14.2" -> {
                return 4.0138;
            }
            case "14.3" -> {
                return 3.9872;
            }
            case "14.4" -> {
                return 3.961;
            }
            case "14.5" -> {
                return 3.935;
            }
            case "14.6" -> {
                return 3.9095;
            }
            case "14.7" -> {
                return 3.8842;
            }
            case "14.8" -> {
                return 3.8593;
            }
            case "14.9" -> {
                return 3.8347;
            }
            case "15.0" -> {
                return 3.8105;
            }
            case "15.1" -> {
                return 3.7865;
            }
            case "15.2" -> {
                return 3.7629;
            }
            case "15.3" -> {
                return 3.7395;
            }
            case "15.4" -> {
                return 3.7165;
            }
            case "15.5" -> {
                return 3.6937;
            }
            case "15.6" -> {
                return 3.6713;
            }
            case "15.7" -> {
                return 3.6491;
            }
            case "15.8" -> {
                return 3.6271;
            }
            case "15.9" -> {
                return 3.6055;
            }
            case "16.0" -> {
                return 3.5841;
            }
            case "16.1" -> {
                return 3.563;
            }
            case "16.2" -> {
                return 3.5421;
            }
            case "16.3" -> {
                return 3.5215;
            }
            case "16.4" -> {
                return 3.5011;
            }
            case "16.5" -> {
                return 3.481;
            }
            case "16.6" -> {
                return 3.4611;
            }
            case "16.7" -> {
                return 3.4414;
            }
            case "16.8" -> {
                return 3.422;
            }
            case "16.9" -> {
                return 3.4028;
            }
            case "17.0" -> {
                return 3.3838;
            }
            case "17.1" -> {
                return 3.365;
            }
            case "17.2" -> {
                return 3.3465;
            }
            case "17.3" -> {
                return 3.3281;
            }
            case "17.4" -> {
                return 3.31;
            }
            case "17.5" -> {
                return 3.2921;
            }
            case "17.6" -> {
                return 3.2743;
            }
            case "17.7" -> {
                return 3.2568;
            }
            case "17.8" -> {
                return 3.2395;
            }
            case "17.9" -> {
                return 3.2223;
            }
            case "18.0" -> {
                return 3.2054;
            }
            case "18.1" -> {
                return 3.1886;
            }
            case "18.2" -> {
                return 3.172;
            }
            case "18.3" -> {
                return 3.1556;
            }
            case "18.4" -> {
                return 3.1394;
            }
            case "18.5" -> {
                return 3.1233;
            }
            case "18.6" -> {
                return 3.1074;
            }
            case "18.7" -> {
                return 3.0917;
            }
            case "18.8" -> {
                return 3.0762;
            }
            case "18.9" -> {
                return 3.0608;
            }
            case "19.0" -> {
                return 3.0455;
            }
            case "19.1" -> {
                return 3.0305;
            }
            case "19.2" -> {
                return 3.0156;
            }
            case "19.3" -> {
                return 3.0008;
            }
            case "19.4" -> {
                return 2.9862;
            }
            case "19.5" -> {
                return 2.9717;
            }
            case "19.6" -> {
                return 2.9574;
            }
            case "19.7" -> {
                return 2.9432;
            }
            case "19.8" -> {
                return 2.9292;
            }
            case "19.9" -> {
                return 2.9153;
            }
            case "20.0" -> {
                return 2.9016;
            }
            case "20.1" -> {
                return 2.88805;
            }
            case "20.2" -> {
                return 2.8745;
            }
            case "20.3" -> {
                return 2.8612;
            }
            case "20.4" -> {
                return 2.8479;
            }
            case "20.5" -> {
                return 2.8349;
            }
            case "20.6" -> {
                return 2.8219;
            }
            case "20.7" -> {
                return 2.80915;
            }
            case "20.8" -> {
                return 2.7964;
            }
            case "20.9" -> {
                return 2.78385;
            }
            case "21.0" -> {
                return 2.7713;
            }
            case "21.1" -> {
                return 2.759;
            }
            case "21.2" -> {
                return 2.7467;
            }
            case "21.3" -> {
                return 2.73465;
            }
            case "21.4" -> {
                return 2.7226;
            }
            case "21.5" -> {
                return 2.7108;
            }
            case "21.6" -> {
                return 2.699;
            }
            case "21.7" -> {
                return 2.68735;
            }
            case "21.8" -> {
                return 2.6757;
            }
            case "21.9" -> {
                return 2.6643;
            }
            case "22.0" -> {
                return 2.6529;
            }
            case "22.1" -> {
                return 2.6417;
            }
            case "22.2" -> {
                return 2.6305;
            }
            case "22.3" -> {
                return 2.61955;
            }
            case "22.4" -> {
                return 2.6086;
            }
            case "22.5" -> {
                return 2.5978;
            }
            case "22.6" -> {
                return 2.587;
            }
            case "22.7" -> {
                return 2.5764;
            }
            case "22.8" -> {
                return 2.5658;
            }
            case "22.9" -> {
                return 2.55535;
            }
            case "23.0" -> {
                return 2.5449;
            }
            case "23.1" -> {
                return 2.5347;
            }
            case "23.2" -> {
                return 2.5245;
            }
            case "23.3" -> {
                return 2.5144;
            }
            case "23.4" -> {
                return 2.5043;
            }
            case "23.5" -> {
                return 2.49445;
            }
            case "23.6" -> {
                return 2.4846;
            }
            case "23.7" -> {
                return 2.47485;
            }
            case "23.8" -> {
                return 2.4651;
            }
            case "23.9" -> {
                return 2.45555;
            }
            case "24.0" -> {
                return 2.446;
            }
            case "24.1" -> {
                return 2.4366;
            }
            case "24.2" -> {
                return 2.4272;
            }
            case "24.3" -> {
                return 2.418;
            }
            case "24.4" -> {
                return 2.4088;
            }
            case "24.5" -> {
                return 2.3997;
            }
            case "24.6" -> {
                return 2.3906;
            }
            case "24.7" -> {
                return 2.38165;
            }
            case "24.8" -> {
                return 2.3727;
            }
            case "24.9" -> {
                return 2.36395;
            }
            case "25.0" -> {
                return 2.3552;
            }
            case "25.1" -> {
                return 2.34655;
            }
            case "25.2" -> {
                return 2.3379;
            }
            case "25.3" -> {
                return 2.3294;
            }
            case "25.4" -> {
                return 2.3209;
            }
            case "25.5" -> {
                return 2.3125;
            }
            case "25.6" -> {
                return 2.3041;
            }
            case "25.7" -> {
                return 2.29585;
            }
            case "25.8" -> {
                return 2.2876;
            }
            case "25.9" -> {
                return 2.2795;
            }
            case "26.0" -> {
                return 2.2714;
            }
            case "26.1" -> {
                return 2.26345;
            }
            case "26.2" -> {
                return 2.2555;
            }
            case "26.3" -> {
                return 2.24765;
            }
            case "26.4" -> {
                return 2.2398;
            }
            case "26.5" -> {
                return 2.23205;
            }
            case "26.6" -> {
                return 2.2243;
            }
            case "26.7" -> {
                return 2.2167;
            }
            case "26.8" -> {
                return 2.2091;
            }
            case "26.9" -> {
                return 2.2016;
            }
            case "27.0" -> {
                return 2.1941;
            }
            case "27.1" -> {
                return 2.1867;
            }
            case "27.2" -> {
                return 2.1793;
            }
            case "27.3" -> {
                return 2.172;
            }
            case "27.4" -> {
                return 2.1647;
            }
            case "27.5" -> {
                return 2.15755;
            }
            case "27.6" -> {
                return 2.1504;
            }
            case "27.7" -> {
                return 2.14335;
            }
            case "27.8" -> {
                return 2.1363;
            }
            case "27.9" -> {
                return 2.12935;
            }
            case "28.0" -> {
                return 2.1224;
            }
            case "28.1" -> {
                return 2.11555;
            }
            case "28.2" -> {
                return 2.1087;
            }
            case "28.3" -> {
                return 2.10195;
            }
            case "28.4" -> {
                return 2.0952;
            }
            case "28.5" -> {
                return 2.08855;
            }
            case "28.6" -> {
                return 2.0819;
            }
            case "28.7" -> {
                return 2.07535;
            }
            case "28.8" -> {
                return 2.0688;
            }
            case "28.9" -> {
                return 2.0623;
            }
            case "29.0" -> {
                return 2.0558;
            }
            case "29.1" -> {
                return 2.04945;
            }
            case "29.2" -> {
                return 2.0431;
            }
            case "29.3" -> {
                return 2.0368;
            }
            case "29.4" -> {
                return 2.0305;
            }
            case "29.5" -> {
                return 2.0243;
            }
            case "29.6" -> {
                return 2.0181;
            }
            case "29.7" -> {
                return 2.012;
            }
            case "29.8" -> {
                return 2.0059;
            }
            case "29.9" -> {
                return 1.9999;
            }
            case "30.0" -> {
                return 1.9939;
            }
            case "30.1" -> {
                return 1.98802;
            }
            case "30.2" -> {
                return 1.98214;
            }
            case "30.3" -> {
                return 1.97626;
            }
            case "30.4" -> {
                return 1.97038;
            }
            case "30.5" -> {
                return 1.9645;
            }
            case "30.6" -> {
                return 1.95882;
            }
            case "30.7" -> {
                return 1.95314;
            }
            case "30.8" -> {
                return 1.94746;
            }
            case "30.9" -> {
                return 1.94178;
            }
            case "31.0" -> {
                return 1.9361;
            }
            case "31.1" -> {
                return 1.93062;
            }
            case "31.2" -> {
                return 1.92514;
            }
            case "31.3" -> {
                return 1.91966;
            }
            case "31.4" -> {
                return 1.91418;
            }
            case "31.5" -> {
                return 1.9087;
            }
            case "31.6" -> {
                return 1.90338;
            }
            case "31.7" -> {
                return 1.89806;
            }
            case "31.8" -> {
                return 1.89274;
            }
            case "31.9" -> {
                return 1.88742;
            }
            case "32.0" -> {
                return 1.8821;
            }
            case "32.1" -> {
                return 1.87698;
            }
            case "32.2" -> {
                return 1.87186;
            }
            case "32.3" -> {
                return 1.86674;
            }
            case "32.4" -> {
                return 1.86162;
            }
            case "32.5" -> {
                return 1.8565;
            }
            case "32.6" -> {
                return 1.85152;
            }
            case "32.7" -> {
                return 1.84654;
            }
            case "32.8" -> {
                return 1.84156;
            }
            case "32.9" -> {
                return 1.83658;
            }
            case "33.0" -> {
                return 1.8316;
            }
            case "33.1" -> {
                return 1.8268;
            }
            case "33.2" -> {
                return 1.822;
            }
            case "33.3" -> {
                return 1.8172;
            }
            case "33.4" -> {
                return 1.8124;
            }
            case "33.5" -> {
                return 1.8076;
            }
            case "33.6" -> {
                return 1.80294;
            }
            case "33.7" -> {
                return 1.79828;
            }
            case "33.8" -> {
                return 1.79362;
            }
            case "33.9" -> {
                return 1.78896;
            }
            case "34.0" -> {
                return 1.7843;
            }
            case "34.1" -> {
                return 1.77978;
            }
            case "34.2" -> {
                return 1.77526;
            }
            case "34.3" -> {
                return 1.77074;
            }
            case "34.4" -> {
                return 1.76622;
            }
            case "34.5" -> {
                return 1.7617;
            }
            case "34.6" -> {
                return 1.75732;
            }
            case "34.7" -> {
                return 1.75294;
            }
            case "34.8" -> {
                return 1.74856;
            }
            case "34.9" -> {
                return 1.74418;
            }
            case "35.0" -> {
                return 1.7398;
            }
            case "35.1" -> {
                return 1.73556;
            }
            case "35.2" -> {
                return 1.73132;
            }
            case "35.3" -> {
                return 1.72708;
            }
            case "35.4" -> {
                return 1.72284;
            }
            case "35.5" -> {
                return 1.7186;
            }
            case "35.6" -> {
                return 1.71448;
            }
            case "35.7" -> {
                return 1.71036;
            }
            case "35.8" -> {
                return 1.70624;
            }
            case "35.9" -> {
                return 1.70212;
            }
            case "36.0" -> {
                return 1.698;
            }
            case "36.1" -> {
                return 1.694;
            }
            case "36.2" -> {
                return 1.69;
            }
            case "36.3" -> {
                return 1.686;
            }
            case "36.4" -> {
                return 1.682;
            }
            case "36.5" -> {
                return 1.678;
            }
            case "36.6" -> {
                return 1.67412;
            }
            case "36.7" -> {
                return 1.67024;
            }
            case "36.8" -> {
                return 1.66636;
            }
            case "36.9" -> {
                return 1.66248;
            }
            case "37.0" -> {
                return 1.6586;
            }
            case "37.1" -> {
                return 1.65484;
            }
            case "37.2" -> {
                return 1.65108;
            }
            case "37.3" -> {
                return 1.64732;
            }
            case "37.4" -> {
                return 1.64356;
            }
            case "37.5" -> {
                return 1.6398;
            }
            case "37.6" -> {
                return 1.63614;
            }
            case "37.7" -> {
                return 1.63248;
            }
            case "37.8" -> {
                return 1.62882;
            }
            case "37.9" -> {
                return 1.62516;
            }
            case "38.0" -> {
                return 1.6215;
            }
            case "38.1" -> {
                return 1.61796;
            }
            case "38.2" -> {
                return 1.61442;
            }
            case "38.3" -> {
                return 1.61088;
            }
            case "38.4" -> {
                return 1.60734;
            }
            case "38.5" -> {
                return 1.6038;
            }
            case "38.6" -> {
                return 1.60034;
            }
            case "38.7" -> {
                return 1.59688;
            }
            case "38.8" -> {
                return 1.59342;
            }
            case "38.9" -> {
                return 1.58996;
            }
            case "39.0" -> {
                return 1.5865;
            }
            case "39.1" -> {
                return 1.58316;
            }
            case "39.2" -> {
                return 1.57982;
            }
            case "39.3" -> {
                return 1.57648;
            }
            case "39.4" -> {
                return 1.57314;
            }
            case "39.5" -> {
                return 1.5698;
            }
            case "39.6" -> {
                return 1.56654;
            }
            case "39.7" -> {
                return 1.56328;
            }
            case "39.8" -> {
                return 1.56002;
            }
            case "39.9" -> {
                return 1.55676;
            }
            case "40.0" -> {
                return 1.5535;
            }
            case "40.1" -> {
                return 1.55032;
            }
            case "40.2" -> {
                return 1.54714;
            }
            case "40.3" -> {
                return 1.54396;
            }
            case "40.4" -> {
                return 1.54078;
            }
            case "40.5" -> {
                return 1.5376;
            }
            case "40.6" -> {
                return 1.53452;
            }
            case "40.7" -> {
                return 1.53144;
            }
            case "40.8" -> {
                return 1.52836;
            }
            case "40.9" -> {
                return 1.52528;
            }
            case "41.0" -> {
                return 1.5222;
            }
            case "41.1" -> {
                return 1.5192;
            }
            case "41.2" -> {
                return 1.5162;
            }
            case "41.3" -> {
                return 1.5132;
            }
            case "41.4" -> {
                return 1.5102;
            }
            case "41.5" -> {
                return 1.5072;
            }
            case "41.6" -> {
                return 1.50428;
            }
            case "41.7" -> {
                return 1.50136;
            }
            case "41.8" -> {
                return 1.49844;
            }
            case "41.9" -> {
                return 1.49552;
            }
            case "42.0" -> {
                return 1.4926;
            }
            case "42.1" -> {
                return 1.48976;
            }
            case "42.2" -> {
                return 1.48692;
            }
            case "42.3" -> {
                return 1.48408;
            }
            case "42.4" -> {
                return 1.48124;
            }
            case "42.5" -> {
                return 1.4784;
            }
            case "42.6" -> {
                return 1.47562;
            }
            case "42.7" -> {
                return 1.47284;
            }
            case "42.8" -> {
                return 1.47006;
            }
            case "42.9" -> {
                return 1.46728;
            }
            case "43.0" -> {
                return 1.4645;
            }
            case "43.1" -> {
                return 1.46182;
            }
            case "43.2" -> {
                return 1.45914;
            }
            case "43.3" -> {
                return 1.45646;
            }
            case "43.4" -> {
                return 1.45378;
            }
            case "43.5" -> {
                return 1.4511;
            }
            case "43.6" -> {
                return 1.44848;
            }
            case "43.7" -> {
                return 1.44586;
            }
            case "43.8" -> {
                return 1.44324;
            }
            case "43.9" -> {
                return 1.44062;
            }
            case "44.0" -> {
                return 1.438;
            }
            case "44.1" -> {
                return 1.43544;
            }
            case "44.2" -> {
                return 1.43288;
            }
            case "44.3" -> {
                return 1.43032;
            }
            case "44.4" -> {
                return 1.42776;
            }
            case "44.5" -> {
                return 1.4252;
            }
            case "44.6" -> {
                return 1.42272;
            }
            case "44.7" -> {
                return 1.42024;
            }
            case "44.8" -> {
                return 1.41776;
            }
            case "44.9" -> {
                return 1.41528;
            }
            case "45.0" -> {
                return 1.4128;
            }
            case "45.1" -> {
                return 1.41036;
            }
            case "45.2" -> {
                return 1.40792;
            }
            case "45.3" -> {
                return 1.40548;
            }
            case "45.4" -> {
                return 1.40304;
            }
            case "45.5" -> {
                return 1.4006;
            }
            case "45.6" -> {
                return 1.39824;
            }
            case "45.7" -> {
                return 1.39588;
            }
            case "45.8" -> {
                return 1.39352;
            }
            case "45.9" -> {
                return 1.39116;
            }
            case "46.0" -> {
                return 1.3888;
            }
            case "46.1" -> {
                return 1.3865;
            }
            case "46.2" -> {
                return 1.3842;
            }
            case "46.3" -> {
                return 1.3819;
            }
            case "46.4" -> {
                return 1.3796;
            }
            case "46.5" -> {
                return 1.3773;
            }
            case "46.6" -> {
                return 1.37506;
            }
            case "46.7" -> {
                return 1.37282;
            }
            case "46.8" -> {
                return 1.37058;
            }
            case "46.9" -> {
                return 1.36834;
            }
            case "47.0" -> {
                return 1.3661;
            }
            case "47.1" -> {
                return 1.36392;
            }
            case "47.2" -> {
                return 1.36174;
            }
            case "47.3" -> {
                return 1.35956;
            }
            case "47.4" -> {
                return 1.35738;
            }
            case "47.5" -> {
                return 1.3552;
            }
            case "47.6" -> {
                return 1.35306;
            }
            case "47.7" -> {
                return 1.35092;
            }
            case "47.8" -> {
                return 1.34878;
            }
            case "47.9" -> {
                return 1.34664;
            }
            case "48.0" -> {
                return 1.3445;
            }
            case "48.1" -> {
                return 1.34242;
            }
            case "48.2" -> {
                return 1.34034;
            }
            case "48.3" -> {
                return 1.33826;
            }
            case "48.4" -> {
                return 1.33618;
            }
            case "48.5" -> {
                return 1.3341;
            }
            case "48.6" -> {
                return 1.33208;
            }
            case "48.7" -> {
                return 1.33006;
            }
            case "48.8" -> {
                return 1.32804;
            }
            case "48.9" -> {
                return 1.32602;
            }
            case "49.0" -> {
                return 1.324;
            }
            case "49.1" -> {
                return 1.32202;
            }
            case "49.2" -> {
                return 1.32004;
            }
            case "49.3" -> {
                return 1.31806;
            }
            case "49.4" -> {
                return 1.31608;
            }
            case "49.5" -> {
                return 1.3141;
            }
            case "49.6" -> {
                return 1.31218;
            }
            case "49.7" -> {
                return 1.31026;
            }
            case "49.8" -> {
                return 1.30834;
            }
            case "49.9" -> {
                return 1.30642;
            }
            case "50.0" -> {
                return 1.3045;
            }
            case "50.1" -> {
                return 1.30262;
            }
            case "50.2" -> {
                return 1.30074;
            }
            case "50.3" -> {
                return 1.29886;
            }
            case "50.4" -> {
                return 1.29698;
            }
            case "50.5" -> {
                return 1.2951;
            }
            case "50.6" -> {
                return 1.29326;
            }
            case "50.7" -> {
                return 1.29142;
            }
            case "50.8" -> {
                return 1.28958;
            }
            case "50.9" -> {
                return 1.28774;
            }
            case "51.0" -> {
                return 1.2859;
            }
            case "51.1" -> {
                return 1.2841;
            }
            case "51.2" -> {
                return 1.2823;
            }
            case "51.3" -> {
                return 1.2805;
            }
            case "51.4" -> {
                return 1.2787;
            }
            case "51.5" -> {
                return 1.2769;
            }
            case "51.6" -> {
                return 1.27516;
            }
            case "51.7" -> {
                return 1.27342;
            }
            case "51.8" -> {
                return 1.27168;
            }
            case "51.9" -> {
                return 1.26994;
            }
            case "52.0" -> {
                return 1.2682;
            }
            case "52.1" -> {
                return 1.2665;
            }
            case "52.2" -> {
                return 1.2648;
            }
            case "52.3" -> {
                return 1.2631;
            }
            case "52.4" -> {
                return 1.2614;
            }
            case "52.5" -> {
                return 1.2597;
            }
            case "52.6" -> {
                return 1.25804;
            }
            case "52.7" -> {
                return 1.25638;
            }
            case "52.8" -> {
                return 1.25472;
            }
            case "52.9" -> {
                return 1.25306;
            }
            case "53.0" -> {
                return 1.2514;
            }
            case "53.1" -> {
                return 1.24978;
            }
            case "53.2" -> {
                return 1.24816;
            }
            case "53.3" -> {
                return 1.24654;
            }
            case "53.4" -> {
                return 1.24492;
            }
            case "53.5" -> {
                return 1.2433;
            }
            case "53.6" -> {
                return 1.24172;
            }
            case "53.7" -> {
                return 1.24014;
            }
            case "53.8" -> {
                return 1.23856;
            }
            case "53.9" -> {
                return 1.23698;
            }
            case "54.0" -> {
                return 1.2354;
            }
            case "54.1" -> {
                return 1.23386;
            }
            case "54.2" -> {
                return 1.23232;
            }
            case "54.3" -> {
                return 1.23078;
            }
            case "54.4" -> {
                return 1.22924;
            }
            case "54.5" -> {
                return 1.2277;
            }
            case "54.6" -> {
                return 1.2262;
            }
            case "54.7" -> {
                return 1.2247;
            }
            case "54.8" -> {
                return 1.2232;
            }
            case "54.9" -> {
                return 1.2217;
            }
            case "55.0" -> {
                return 1.2202;
            }
            case "55.1" -> {
                return 1.21875;
            }
            case "55.2" -> {
                return 1.2173;
            }
            case "55.3" -> {
                return 1.21585;
            }
            case "55.4" -> {
                return 1.2144;
            }
            case "55.5" -> {
                return 1.21295;
            }
            case "55.6" -> {
                return 1.2115;
            }
            case "55.7" -> {
                return 1.21005;
            }
            case "55.8" -> {
                return 1.2086;
            }
            case "55.9" -> {
                return 1.20715;
            }
            case "56.0" -> {
                return 1.2057;
            }
            case "56.1" -> {
                return 1.20431;
            }
            case "56.2" -> {
                return 1.20292;
            }
            case "56.3" -> {
                return 1.20153;
            }
            case "56.4" -> {
                return 1.20014;
            }
            case "56.5" -> {
                return 1.19875;
            }
            case "56.6" -> {
                return 1.19736;
            }
            case "56.7" -> {
                return 1.19597;
            }
            case "56.8" -> {
                return 1.19458;
            }
            case "56.9" -> {
                return 1.19319;
            }
            case "57.0" -> {
                return 1.1918;
            }
            case "57.1" -> {
                return 1.19049;
            }
            case "57.2" -> {
                return 1.18918;
            }
            case "57.3" -> {
                return 1.18787;
            }
            case "57.4" -> {
                return 1.18656;
            }
            case "57.5" -> {
                return 1.18525;
            }
            case "57.6" -> {
                return 1.18394;
            }
            case "57.7" -> {
                return 1.18263;
            }
            case "57.8" -> {
                return 1.18132;
            }
            case "57.9" -> {
                return 1.18001;
            }
            case "58.0" -> {
                return 1.1787;
            }
            case "58.1" -> {
                return 1.17745;
            }
            case "58.2" -> {
                return 1.1762;
            }
            case "58.3" -> {
                return 1.17495;
            }
            case "58.4" -> {
                return 1.1737;
            }
            case "58.5" -> {
                return 1.17245;
            }
            case "58.6" -> {
                return 1.1712;
            }
            case "58.7" -> {
                return 1.16995;
            }
            case "58.8" -> {
                return 1.1687;
            }
            case "58.9" -> {
                return 1.16745;
            }
            case "59.0" -> {
                return 1.1662;
            }
            case "59.1" -> {
                return 1.16501;
            }
            case "59.2" -> {
                return 1.16382;
            }
            case "59.3" -> {
                return 1.16263;
            }
            case "59.4" -> {
                return 1.16144;
            }
            case "59.5" -> {
                return 1.16025;
            }
            case "59.6" -> {
                return 1.15906;
            }
            case "59.7" -> {
                return 1.15787;
            }
            case "59.8" -> {
                return 1.15668;
            }
            case "59.9" -> {
                return 1.15549;
            }
            case "60.0" -> {
                return 1.1543;
            }
            case "60.1" -> {
                return 1.15317;
            }
            case "60.2" -> {
                return 1.15204;
            }
            case "60.3" -> {
                return 1.15091;
            }
            case "60.4" -> {
                return 1.14978;
            }
            case "60.5" -> {
                return 1.14865;
            }
            case "60.6" -> {
                return 1.14752;
            }
            case "60.7" -> {
                return 1.14639;
            }
            case "60.8" -> {
                return 1.14526;
            }
            case "60.9" -> {
                return 1.14413;
            }
            case "61.0" -> {
                return 1.143;
            }
            case "61.1" -> {
                return 1.14192;
            }
            case "61.2" -> {
                return 1.14084;
            }
            case "61.3" -> {
                return 1.13976;
            }
            case "61.4" -> {
                return 1.13868;
            }
            case "61.5" -> {
                return 1.1376;
            }
            case "61.6" -> {
                return 1.13652;
            }
            case "61.7" -> {
                return 1.13544;
            }
            case "61.8" -> {
                return 1.13436;
            }
            case "61.9" -> {
                return 1.13328;
            }
            case "62.0" -> {
                return 1.1322;
            }
            case "62.1" -> {
                return 1.13118;
            }
            case "62.2" -> {
                return 1.13016;
            }
            case "62.3" -> {
                return 1.12914;
            }
            case "62.4" -> {
                return 1.12812;
            }
            case "62.5" -> {
                return 1.1271;
            }
            case "62.6" -> {
                return 1.12608;
            }
            case "62.7" -> {
                return 1.12506;
            }
            case "62.8" -> {
                return 1.12404;
            }
            case "62.9" -> {
                return 1.12302;
            }
            case "63.0" -> {
                return 1.122;
            }
            case "63.1" -> {
                return 1.12103;
            }
            case "63.2" -> {
                return 1.12006;
            }
            case "63.3" -> {
                return 1.11909;
            }
            case "63.4" -> {
                return 1.11812;
            }
            case "63.5" -> {
                return 1.11715;
            }
            case "63.6" -> {
                return 1.11618;
            }
            case "63.7" -> {
                return 1.11521;
            }
            case "63.8" -> {
                return 1.11424;
            }
            case "63.9" -> {
                return 1.11327;
            }
            case "64.0" -> {
                return 1.1123;
            }
            case "64.1" -> {
                return 1.11138;
            }
            case "64.2" -> {
                return 1.11046;
            }
            case "64.3" -> {
                return 1.10954;
            }
            case "64.4" -> {
                return 1.10862;
            }
            case "64.5" -> {
                return 1.1077;
            }
            case "64.6" -> {
                return 1.10678;
            }
            case "64.7" -> {
                return 1.10586;
            }
            case "64.8" -> {
                return 1.10494;
            }
            case "64.9" -> {
                return 1.10402;
            }
            case "65.0" -> {
                return 1.1031;
            }
            case "65.1" -> {
                return 1.10223;
            }
            case "65.2" -> {
                return 1.10136;
            }
            case "65.3" -> {
                return 1.10049;
            }
            case "65.4" -> {
                return 1.09962;
            }
            case "65.5" -> {
                return 1.09875;
            }
            case "65.6" -> {
                return 1.09788;
            }
            case "65.7" -> {
                return 1.09701;
            }
            case "65.8" -> {
                return 1.09614;
            }
            case "65.9" -> {
                return 1.09527;
            }
            case "66.0" -> {
                return 1.0944;
            }
            case "66.1" -> {
                return 1.09358;
            }
            case "66.2" -> {
                return 1.09276;
            }
            case "66.3" -> {
                return 1.09194;
            }
            case "66.4" -> {
                return 1.09112;
            }
            case "66.5" -> {
                return 1.0903;
            }
            case "66.6" -> {
                return 1.08948;
            }
            case "66.7" -> {
                return 1.08866;
            }
            case "66.8" -> {
                return 1.08784;
            }
            case "66.9" -> {
                return 1.08702;
            }
            case "67.0" -> {
                return 1.0862;
            }
            case "67.1" -> {
                return 1.08542;
            }
            case "67.2" -> {
                return 1.08464;
            }
            case "67.3" -> {
                return 1.08386;
            }
            case "67.4" -> {
                return 1.08308;
            }
            case "67.5" -> {
                return 1.0823;
            }
            case "67.6" -> {
                return 1.08152;
            }
            case "67.7" -> {
                return 1.08074;
            }
            case "67.8" -> {
                return 1.07996;
            }
            case "67.9" -> {
                return 1.07918;
            }
            case "68.0" -> {
                return 1.0784;
            }
            case "68.1" -> {
                return 1.07766;
            }
            case "68.2" -> {
                return 1.07692;
            }
            case "68.3" -> {
                return 1.07618;
            }
            case "68.4" -> {
                return 1.07544;
            }
            case "68.5" -> {
                return 1.0747;
            }
            case "68.6" -> {
                return 1.07396;
            }
            case "68.7" -> {
                return 1.07322;
            }
            case "68.8" -> {
                return 1.07248;
            }
            case "68.9" -> {
                return 1.07174;
            }
            case "69.0" -> {
                return 1.071;
            }
            case "69.1" -> {
                return 1.0703;
            }
            case "69.2" -> {
                return 1.0696;
            }
            case "69.3" -> {
                return 1.0689;
            }
            case "69.4" -> {
                return 1.0682;
            }
            case "69.5" -> {
                return 1.0675;
            }
            case "69.6" -> {
                return 1.0668;
            }
            case "69.7" -> {
                return 1.0661;
            }
            case "69.8" -> {
                return 1.0654;
            }
            case "69.9" -> {
                return 1.0647;
            }
            case "70.0" -> {
                return 1.064;
            }
            case "70.1" -> {
                return 1.06335;
            }
            case "70.2" -> {
                return 1.0627;
            }
            case "70.3" -> {
                return 1.06205;
            }
            case "70.4" -> {
                return 1.0614;
            }
            case "70.5" -> {
                return 1.06075;
            }
            case "70.6" -> {
                return 1.0601;
            }
            case "70.7" -> {
                return 1.05945;
            }
            case "70.8" -> {
                return 1.0588;
            }
            case "70.9" -> {
                return 1.05815;
            }
            case "71.0" -> {
                return 1.0575;
            }
            case "71.1" -> {
                return 1.05688;
            }
            case "71.2" -> {
                return 1.05626;
            }
            case "71.3" -> {
                return 1.05564;
            }
            case "71.4" -> {
                return 1.05502;
            }
            case "71.5" -> {
                return 1.0544;
            }
            case "71.6" -> {
                return 1.05378;
            }
            case "71.7" -> {
                return 1.05316;
            }
            case "71.8" -> {
                return 1.05254;
            }
            case "71.9" -> {
                return 1.05192;
            }
            case "72.0" -> {
                return 1.0513;
            }
            case "72.1" -> {
                return 1.05073;
            }
            case "72.2" -> {
                return 1.05016;
            }
            case "72.3" -> {
                return 1.04959;
            }
            case "72.4" -> {
                return 1.04902;
            }
            case "72.5" -> {
                return 1.04845;
            }
            case "72.6" -> {
                return 1.04788;
            }
            case "72.7" -> {
                return 1.04731;
            }
            case "72.8" -> {
                return 1.04674;
            }
            case "72.9" -> {
                return 1.04617;
            }
            case "73.0" -> {
                return 1.0456;
            }
            case "73.1" -> {
                return 1.04506;
            }
            case "73.2" -> {
                return 1.04452;
            }
            case "73.3" -> {
                return 1.04398;
            }
            case "73.4" -> {
                return 1.04344;
            }
            case "73.5" -> {
                return 1.0429;
            }
            case "73.6" -> {
                return 1.04236;
            }
            case "73.7" -> {
                return 1.04182;
            }
            case "73.8" -> {
                return 1.04128;
            }
            case "73.9" -> {
                return 1.04074;
            }
            case "74.0" -> {
                return 1.0402;
            }
            case "74.1" -> {
                return 1.0397;
            }
            case "74.2" -> {
                return 1.0392;
            }
            case "74.3" -> {
                return 1.0387;
            }
            case "74.4" -> {
                return 1.0382;
            }
            case "74.5" -> {
                return 1.0377;
            }
            case "74.6" -> {
                return 1.0372;
            }
            case "74.7" -> {
                return 1.0367;
            }
            case "74.8" -> {
                return 1.0362;
            }
            case "74.9" -> {
                return 1.0357;
            }
            case "75.0" -> {
                return 1.0352;
            }
            case "75.1" -> {
                return 1.03473;
            }
            case "75.2" -> {
                return 1.03426;
            }
            case "75.3" -> {
                return 1.03379;
            }
            case "75.4" -> {
                return 1.03332;
            }
            case "75.5" -> {
                return 1.03285;
            }
            case "75.6" -> {
                return 1.03238;
            }
            case "75.7" -> {
                return 1.03191;
            }
            case "75.8" -> {
                return 1.03144;
            }
            case "75.9" -> {
                return 1.03097;
            }
            case "76.0" -> {
                return 1.0305;
            }
            case "76.1" -> {
                return 1.03007;
            }
            case "76.2" -> {
                return 1.02964;
            }
            case "76.3" -> {
                return 1.02921;
            }
            case "76.4" -> {
                return 1.02878;
            }
            case "76.5" -> {
                return 1.02835;
            }
            case "76.6" -> {
                return 1.02792;
            }
            case "76.7" -> {
                return 1.02749;
            }
            case "76.8" -> {
                return 1.02706;
            }
            case "76.9" -> {
                return 1.02663;
            }
            case "77.0" -> {
                return 1.0262;
            }
            case "77.1" -> {
                return 1.02581;
            }
            case "77.2" -> {
                return 1.02542;
            }
            case "77.3" -> {
                return 1.02503;
            }
            case "77.4" -> {
                return 1.02464;
            }
            case "77.5" -> {
                return 1.02425;
            }
            case "77.6" -> {
                return 1.02386;
            }
            case "77.7" -> {
                return 1.02347;
            }
            case "77.8" -> {
                return 1.02308;
            }
            case "77.9" -> {
                return 1.02269;
            }
            case "78.0" -> {
                return 1.0223;
            }
            case "78.1" -> {
                return 1.02194;
            }
            case "78.2" -> {
                return 1.02158;
            }
            case "78.3" -> {
                return 1.02122;
            }
            case "78.4" -> {
                return 1.02086;
            }
            case "78.5" -> {
                return 1.0205;
            }
            case "78.6" -> {
                return 1.02014;
            }
            case "78.7" -> {
                return 1.01978;
            }
            case "78.8" -> {
                return 1.01942;
            }
            case "78.9" -> {
                return 1.01906;
            }
            case "79.0" -> {
                return 1.0187;
            }
            case "79.1" -> {
                return 1.01837;
            }
            case "79.2" -> {
                return 1.01804;
            }
            case "79.3" -> {
                return 1.01771;
            }
            case "79.4" -> {
                return 1.01738;
            }
            case "79.5" -> {
                return 1.01705;
            }
            case "79.6" -> {
                return 1.01672;
            }
            case "79.7" -> {
                return 1.01639;
            }
            case "79.8" -> {
                return 1.01606;
            }
            case "79.9" -> {
                return 1.01573;
            }
            case "80.0" -> {
                return 1.0154;
            }
            case "80.1" -> {
                return 1.0151;
            }
            case "80.2" -> {
                return 1.0148;
            }
            case "80.3" -> {
                return 1.0145;
            }
            case "80.4" -> {
                return 1.0142;
            }
            case "80.5" -> {
                return 1.0139;
            }
            case "80.6" -> {
                return 1.0136;
            }
            case "80.7" -> {
                return 1.0133;
            }
            case "80.8" -> {
                return 1.013;
            }
            case "80.9" -> {
                return 1.0127;
            }
            case "81.0" -> {
                return 1.0124;
            }
            case "81.1" -> {
                return 1.01214;
            }
            case "81.2" -> {
                return 1.01188;
            }
            case "81.3" -> {
                return 1.01162;
            }
            case "81.4" -> {
                return 1.01136;
            }
            case "81.5" -> {
                return 1.0111;
            }
            case "81.6" -> {
                return 1.01084;
            }
            case "81.7" -> {
                return 1.01058;
            }
            case "81.8" -> {
                return 1.01032;
            }
            case "81.9" -> {
                return 1.01006;
            }
            case "82.0" -> {
                return 1.0098;
            }
            case "82.1" -> {
                return 1.00957;
            }
            case "82.2" -> {
                return 1.00934;
            }
            case "82.3" -> {
                return 1.00911;
            }
            case "82.4" -> {
                return 1.00888;
            }
            case "82.5" -> {
                return 1.00865;
            }
            case "82.6" -> {
                return 1.00842;
            }
            case "82.7" -> {
                return 1.00819;
            }
            case "82.8" -> {
                return 1.00796;
            }
            case "82.9" -> {
                return 1.00773;
            }
            case "83.0" -> {
                return 1.0075;
            }
            case "83.1" -> {
                return 1.0073;
            }
            case "83.2" -> {
                return 1.0071;
            }
            case "83.3" -> {
                return 1.0069;
            }
            case "83.4" -> {
                return 1.0067;
            }
            case "83.5" -> {
                return 1.0065;
            }
            case "83.6" -> {
                return 1.0063;
            }
            case "83.7" -> {
                return 1.0061;
            }
            case "83.8" -> {
                return 1.0059;
            }
            case "83.9" -> {
                return 1.0057;
            }
            case "84.0" -> {
                return 1.0055;
            }
            case "84.1" -> {
                return 1.00533;
            }
            case "84.2" -> {
                return 1.00516;
            }
            case "84.3" -> {
                return 1.00499;
            }
            case "84.4" -> {
                return 1.00482;
            }
            case "84.5" -> {
                return 1.00465;
            }
            case "84.6" -> {
                return 1.00448;
            }
            case "84.7" -> {
                return 1.00431;
            }
            case "84.8" -> {
                return 1.00414;
            }
            case "84.9" -> {
                return 1.00397;
            }
            case "85.0" -> {
                return 1.0038;
            }
            case "85.1" -> {
                return 1.00366;
            }
            case "85.2" -> {
                return 1.00352;
            }
            case "85.3" -> {
                return 1.00338;
            }
            case "85.4" -> {
                return 1.00324;
            }
            case "85.5" -> {
                return 1.0031;
            }
            case "85.6" -> {
                return 1.00296;
            }
            case "85.7" -> {
                return 1.00282;
            }
            case "85.8" -> {
                return 1.00268;
            }
            case "85.9" -> {
                return 1.00254;
            }
            case "86.0" -> {
                return 1.0024;
            }
            case "86.1" -> {
                return 1.0023;
            }
            case "86.2" -> {
                return 1.0022;
            }
            case "86.3" -> {
                return 1.0021;
            }
            case "86.4" -> {
                return 1.002;
            }
            case "86.5" -> {
                return 1.0019;
            }
            case "86.6" -> {
                return 1.0018;
            }
            case "86.7" -> {
                return 1.0017;
            }
            case "86.8" -> {
                return 1.0016;
            }
            case "86.9" -> {
                return 1.0015;
            }
            case "87.0" -> {
                return 1.0014;
            }
            case "87.1" -> {
                return 1.00132;
            }
            case "87.2" -> {
                return 1.00124;
            }
            case "87.3" -> {
                return 1.00116;
            }
            case "87.4" -> {
                return 1.00108;
            }
            case "87.5" -> {
                return 1.001;
            }
            case "87.6" -> {
                return 1.00092;
            }
            case "87.7" -> {
                return 1.00084;
            }
            case "87.8" -> {
                return 1.00076;
            }
            case "87.9" -> {
                return 1.00068;
            }
            case "88.0" -> {
                return 1.0006;
            }
            case "88.1" -> {
                return 1.00056;
            }
            case "88.2" -> {
                return 1.00052;
            }
            case "88.3" -> {
                return 1.00048;
            }
            case "88.4" -> {
                return 1.00044;
            }
            case "88.5" -> {
                return 1.0004;
            }
            case "88.6" -> {
                return 1.00036;
            }
            case "88.7" -> {
                return 1.00032;
            }
            case "88.8" -> {
                return 1.00028;
            }
            case "88.9" -> {
                return 1.00024;
            }
            case "89.0" -> {
                return 1.0002;
            }
            case "89.1" -> {
                return 1.00018;
            }
            case "89.2" -> {
                return 1.00016;
            }
            case "89.3" -> {
                return 1.00014;
            }
            case "89.4" -> {
                return 1.00012;
            }
            case "89.5" -> {
                return 1.0001;
            }
            case "89.6" -> {
                return 1.00008;
            }
            case "89.7" -> {
                return 1.00006;
            }
            case "89.8" -> {
                return 1.00004;
            }
            case "89.9" -> {
                return 1.00002;
            }
            case "90.0" -> {
                return 1;
            }
            default ->
                throw new UnsupportedOperationException("Error: Unknown solar altitude angle: " + val);
        }
    }
}
