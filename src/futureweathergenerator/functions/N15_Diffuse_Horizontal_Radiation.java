/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.Months;

/**
 * Morphs N15 diffuse horizontal radiation.
 *
 * @author eugenio
 */
public class N15_Diffuse_Horizontal_Radiation {

    /**
     * Morphs N15 diffuse horizontal radiation.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     */
    public static void morph(EPW epw, AverageScenario scenario, EPW morphedEPW) {
        // Requires pre-processing of:
        // N10 Extraterrestrial Horizontal Radiation
        // N13 Global Horizontal Radiation
        double[] fehr = new double[365];
        double[] fghr = new double[365];
        double[] clear_index_day = new double[365];
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double day_number = (int) Math.floor(row_id / 24.0);
                fehr[(int) day_number] += morphedEPW.getEpw_data_fields().get(row_id).getN10_extraterrestrial_horizontal_radiation();
                fghr[(int) day_number] += morphedEPW.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
            }
        }
        for (int i = 0; i < 365; i++) {
            fehr[i] /= 24.0;
            fghr[i] /= 24.0;
            clear_index_day[i] = fghr[i] / fehr[i];
        }
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            double longitude = epw.getEpw_location().getN3_longitude();
            double time_zone = epw.getEpw_location().getN4_time_zone();
            double latitude = epw.getEpw_location().getN2_latitude();
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double future_extraterrestrial_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN10_extraterrestrial_horizontal_radiation();
                if (future_extraterrestrial_horizontal_radiation > 0) {
                    double day_number = (int) Math.floor(row_id / 24.0) + 1;
                    double day_angle = SolarFunctions.getDayAngle(day_number);
                    double equation_of_time = SolarFunctions.getEquationOfTime(day_angle);
                    double declination = SolarFunctions.getDeclination(day_angle);
                    double local_standard_time = epw.getEpw_data_fields().get(row_id).getN4_hour();
                    double sin_Solar_Altitude_Angle_sum = 0.0;
                    double minutes = 0.0;
                    for (int min = 1; min <= 60; min++) {
                        double solar_time = SolarFunctions.getSolarTime(local_standard_time - 1 + (min / 60.0), longitude, time_zone, equation_of_time);
                        double hour_angle = SolarFunctions.getHourAngle(solar_time);
                        double sin_Solar_Altitude_Angle_step = SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                        if (sin_Solar_Altitude_Angle_step > 0) {
                            sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                            minutes++;
                        }
                    }
                    if (minutes != 0) {
                        double sin_Solar_Altitude_Angle = sin_Solar_Altitude_Angle_sum / minutes;
                        double solar_altitude_angle = SolarFunctions.getSolarAltitudeAngle(sin_Solar_Altitude_Angle);
                        solar_altitude_angle = solar_altitude_angle < 0.0 ? 0.0 : solar_altitude_angle;
                        int hourPrevious = row_id - 1;
                        if (hourPrevious < 0) {
                            hourPrevious = 8759;
                        }
                        int hourNext = row_id + 1;
                        if (hourNext > 8759) {
                            hourNext = 0;
                        }
                        double future_global_horizontal_radiation_previous = morphedEPW.getEpw_data_fields().get(hourPrevious).getN13_global_horizontal_radiation();
                        double future_extraterrestrial_horizontal_radiation_previous = morphedEPW.getEpw_data_fields().get(hourPrevious).getN10_extraterrestrial_horizontal_radiation();
                        double clearIndexHour_previous = future_global_horizontal_radiation_previous / future_extraterrestrial_horizontal_radiation_previous;
                        double future_global_horizontal_radiation_next = morphedEPW.getEpw_data_fields().get(hourNext).getN13_global_horizontal_radiation();
                        double future_extraterrestrial_horizontal_radiation_next = morphedEPW.getEpw_data_fields().get(hourNext).getN10_extraterrestrial_horizontal_radiation();
                        double clearIndexHour_next = future_global_horizontal_radiation_next / future_extraterrestrial_horizontal_radiation_next;
                        double clear_persistence = 0.0;
                        if (future_extraterrestrial_horizontal_radiation_previous == 0) {
                            clear_persistence = clearIndexHour_next;
                        }
                        if (future_extraterrestrial_horizontal_radiation_next == 0) {
                            clear_persistence = clearIndexHour_previous;
                        }
                        if (future_extraterrestrial_horizontal_radiation_previous > 0
                                && future_extraterrestrial_horizontal_radiation_next > 0) {
                            clear_persistence = (clearIndexHour_previous + clearIndexHour_next) / 2.0;
                        }
                        double future_global_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
                        double clear_index_hour = future_global_horizontal_radiation / future_extraterrestrial_horizontal_radiation;
                        double solar_time = SolarFunctions.getSolarTime(local_standard_time, longitude, time_zone, equation_of_time);
                        double calculated_diffuse_horizontal_radiation = future_global_horizontal_radiation
                                * (1.0
                                / (1.0 + Math.exp(-5.38
                                        + (6.63 * clear_index_hour)
                                        + (0.006 * solar_time)
                                        - (0.007 * solar_altitude_angle)
                                        + (1.75 * clear_index_day[(int) day_number - 1])
                                        + (1.31 * clear_persistence)
                                )));
                        morphedEPW.getEpw_data_fields().get(row_id).setN15_diffuse_horizontal_radiation((float) calculated_diffuse_horizontal_radiation);
                    } else {
                        morphedEPW.getEpw_data_fields().get(row_id).setN15_diffuse_horizontal_radiation(0.0f);
                    }
                } else {
                    morphedEPW.getEpw_data_fields().get(row_id).setN15_diffuse_horizontal_radiation(0.0f);
                }
            }
        }
    }
}
