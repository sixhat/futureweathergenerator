/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.EPW.EPW_Typical_Extreme_Periods;

/**
 * Resets typical/extreme periods object.
 *
 * @author eugenio
 */
public class Typical_Extreme_Periods {

    /**
     * Resets typical/extreme periods object.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void reset(EPW morphedEPW) {
        morphedEPW.getEpw_typical_extreme_periods().setN1_number_of_typical_extreme_periods(0);
        morphedEPW.getEpw_typical_extreme_periods().setAn_typical_extreme_periods(new EPW_Typical_Extreme_Periods.EPW_Typical_Extreme_Period[0]);
    }
}
