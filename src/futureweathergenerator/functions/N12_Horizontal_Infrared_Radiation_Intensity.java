/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.psychrometric.PsychrometricFunctions;
import futureweathergenerator.Months;

/**
 * Calculates N12 horizontal infrared radiation intensity from solar model and
 * future dry bulb temperature.
 *
 * @author eugenio
 */
public class N12_Horizontal_Infrared_Radiation_Intensity {

    /**
     * Calculates N12 horizontal infrared radiation intensity from solar model
     * and future dry bulb temperature.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        // Requires pre-processing of:
        // N6 Dry Bulb Temperature
        // N8 Relative Humidty
        // N22 Total Sky Cover
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double future_dry_bulb_temperature = morphedEPW.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                double future_dry_bulb_temperature_in_kelvin = future_dry_bulb_temperature + 273.15;
                double future_relative_humidity = morphedEPW.getEpw_data_fields().get(row_id).getN8_relative_humidity();
                double future_dry_bulb_temperature_floor = Math.floor(future_dry_bulb_temperature);
                double future_dry_bulb_temperature_ceil = future_dry_bulb_temperature_floor + 1;
                double future_dry_bulb_temperature_dif = future_dry_bulb_temperature - future_dry_bulb_temperature_floor;
                double pws_value = PsychrometricFunctions.getAbsolutePressureOfWaterVapourAtSaturation(future_dry_bulb_temperature_floor);
                double pws_step = PsychrometricFunctions.getAbsolutePressureOfWaterVapourAtSaturation(future_dry_bulb_temperature_ceil) - pws_value;
                double pws = pws_value + (future_dry_bulb_temperature_dif * pws_step);
                double future_partial_pressure_of_water_vapour = (future_relative_humidity / 100.0) * pws;
                double future_clear_sky_atmospheric_emissivity = 1.24 * Math.pow((future_partial_pressure_of_water_vapour * 10.0) / future_dry_bulb_temperature_in_kelvin, 0.142857142857143);
                double future_total_sky_cover = morphedEPW.getEpw_data_fields().get(row_id).getN22_total_sky_cover();
                double future_actual_atmospheric_emissivity = future_total_sky_cover / 10.0 + (1.0 - future_total_sky_cover / 10.0) * future_clear_sky_atmospheric_emissivity;
                double calculated_horizontal_infrared_radiation_intensity = future_actual_atmospheric_emissivity * 5.67 * Math.pow(10, -8.0) * Math.pow(future_dry_bulb_temperature_in_kelvin, 4.0);
                morphedEPW.getEpw_data_fields().get(row_id).setN12_horizontal_infrared_radiation_intensity((float) calculated_horizontal_infrared_radiation_intensity);
            }
        }
    }
}
