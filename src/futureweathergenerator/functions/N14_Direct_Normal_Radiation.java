/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;

/**
 * Calculates N14 direct normal radiation using solar model, future global
 * horizontal radiation, and future diffuse horizontal radiation.
 *
 * @author eugenio
 */
public class N14_Direct_Normal_Radiation {

    /**
     * Calculates N14 direct normal radiation using solar model, future global
     * horizontal radiation, and future diffuse horizontal radiation.
     *
     * @param epw current EPW object
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW epw, EPW morphedEPW) {
        // Requires pre-processing of:
        // N13 Global Horizontal Radiation
        // N15 Diffuse Horizontal Radiation
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            double longitude = epw.getEpw_location().getN3_longitude();
            double time_zone = epw.getEpw_location().getN4_time_zone();
            double latitude = epw.getEpw_location().getN2_latitude();
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double day_number = (int) Math.floor(row_id / 24.0);
                double day_angle = SolarFunctions.getDayAngle(day_number + 1);
                double equation_of_time = SolarFunctions.getEquationOfTime(day_angle);
                double declination = SolarFunctions.getDeclination(day_angle);
                double local_standard_time = epw.getEpw_data_fields().get(row_id).getN4_hour();
                double sin_Solar_Altitude_Angle_sum = 0.0;
                double minutes = 0.0;
                for (int min = 1; min <= 60; min++) {
                    double solar_time = SolarFunctions.getSolarTime(local_standard_time - 1 + (min / 60.0), longitude, time_zone, equation_of_time);
                    double hour_angle = SolarFunctions.getHourAngle(solar_time);
                    double sin_Solar_Altitude_Angle_step = SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                    if (sin_Solar_Altitude_Angle_step > 0) {
                        sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                        minutes++;
                    }
                }
                if (minutes != 0) {
                    double sin_Solar_Altitude_Angle = sin_Solar_Altitude_Angle_sum / minutes;
                    double future_global_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
                    double future_diffuse_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN15_diffuse_horizontal_radiation();
                    double calculated_direct_normal_radiation = (future_global_horizontal_radiation - future_diffuse_horizontal_radiation) / sin_Solar_Altitude_Angle;
                    morphedEPW.getEpw_data_fields().get(row_id).setN14_direct_normal_radiation((float) calculated_direct_normal_radiation);
                } else {
                    morphedEPW.getEpw_data_fields().get(row_id).setN14_direct_normal_radiation(0.0f);
                }
            }
        }
    }
}
