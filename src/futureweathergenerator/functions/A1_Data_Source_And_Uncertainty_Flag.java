/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.Months;

/**
 * Sets the data source and uncertainty flag.
 *
 * @author eugenio
 */
public class A1_Data_Source_And_Uncertainty_Flag {

    /**
     * Sets the data source and uncertainty flag.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void update(EPW morphedEPW) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                morphedEPW.getEpw_data_fields().get(row_id).setA1_data_source_and_uncertainty_flag("*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?");
            }
        }
    }
}
