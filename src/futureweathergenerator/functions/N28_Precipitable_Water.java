/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.Months;

/**
 * Morphs N28 precipitable water.
 *
 * @author eugenio
 */
public class N28_Precipitable_Water {

    /**
     * Morphs N28 precipitable water.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, AverageScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float scaling_factor_monthly_precipitation_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_total_precipitation(), numberOfHoursToSmooth);
                float scaling_factor_monthly_precipitation_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, scaling_factor_monthly_precipitation_step, numberOfHoursToSmooth);
                float scaling_factor_monthly_precipitation = scenario.getInterpolated_total_precipitation()[i] + scaling_factor_monthly_precipitation_delta;
                float calculated_precipitable_water = scaling_factor_monthly_precipitation * epw.getEpw_data_fields().get(row_id).getN28_precipitable_water();
                morphedEPW.getEpw_data_fields().get(row_id).setN28_precipitable_water((float) calculated_precipitable_water);
            }
        }
    }
}
