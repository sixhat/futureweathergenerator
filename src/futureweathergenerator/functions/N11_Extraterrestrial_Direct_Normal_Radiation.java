/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;

/**
 * Calculates N11 extraterrestrial direct normal radiation from solar model and
 * future extraterrestrial direct normal radiation.
 *
 * @author eugenio
 */
public class N11_Extraterrestrial_Direct_Normal_Radiation {

    /**
     * Calculates N11 extraterrestrial direct normal radiation from solar model
     * and future extraterrestrial direct normal radiation.
     *
     * @param epw current EPW object
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW epw, EPW morphedEPW) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            double latitude = epw.getEpw_location().getN2_latitude();
            double longitude = epw.getEpw_location().getN3_longitude();
            double time_zone = epw.getEpw_location().getN4_time_zone();
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double day_number = (int) Math.floor(row_id / 24.0) + 1;
                double day_angle = SolarFunctions.getDayAngle(day_number);
                double equation_of_time = SolarFunctions.getEquationOfTime(day_angle);
                double declination = SolarFunctions.getDeclination(day_angle);
                double correction_factor_for_solar_distance = SolarFunctions.getCorrectionFactorForSolarDistance(day_angle);
                double calculated_extraterrestrial_direct_normal_radiation = SolarFunctions.getExtraterrestrialDirectNormalRadiation(correction_factor_for_solar_distance);
                double local_standard_time = epw.getEpw_data_fields().get(row_id).getN4_hour();
                int minutes = 0;
                for (int min = 1; min <= 60; min++) {
                    double solar_time = SolarFunctions.getSolarTime(local_standard_time - 1.0 + (min / 60.0), longitude, time_zone, equation_of_time);
                    double hour_angle = SolarFunctions.getHourAngle(solar_time);
                    double sin_Solar_Altitude_Angle = SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                    minutes = sin_Solar_Altitude_Angle > 0 ? minutes + 1 : minutes;
                }
                calculated_extraterrestrial_direct_normal_radiation = (minutes / 60.0) * calculated_extraterrestrial_direct_normal_radiation;
                morphedEPW.getEpw_data_fields().get(row_id).setN11_extraterrestrial_direct_normal_radiation((float) calculated_extraterrestrial_direct_normal_radiation);
            }
        }
    }
}
