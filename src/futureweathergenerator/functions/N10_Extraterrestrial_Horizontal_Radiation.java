/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;

/**
 * Calculates N10 extraterrestrial horizontal radiation from solar model.
 *
 * @author eugenio
 */
public class N10_Extraterrestrial_Horizontal_Radiation {

    /**
     * Calculates N10 extraterrestrial horizontal radiation from solar model.
     *
     * @param epw current EPW object
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW epw, EPW morphedEPW) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            double latitude = epw.getEpw_location().getN2_latitude();
            double longitude = epw.getEpw_location().getN3_longitude();
            double time_zone = epw.getEpw_location().getN4_time_zone();
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double day_number = (int) Math.floor(row_id / 24.0) + 1;
                double day_angle = SolarFunctions.getDayAngle(day_number);
                double equation_of_time = SolarFunctions.getEquationOfTime(day_angle);
                double correction_factor_for_solar_distance = SolarFunctions.getCorrectionFactorForSolarDistance(day_angle);
                double calculated_extraterrestrial_direct_normal_radiation = SolarFunctions.getExtraterrestrialDirectNormalRadiation(correction_factor_for_solar_distance);
                double local_standard_time = epw.getEpw_data_fields().get(row_id).getN4_hour();
                double declination = SolarFunctions.getDeclination(day_angle);
                double sin_Solar_Altitude_Angle_sum = 0.0;
                for (int min = 1; min <= 60; min++) {
                    double solar_time = SolarFunctions.getSolarTime(local_standard_time - 1.0 + (min / 60.0), longitude, time_zone, equation_of_time);
                    double hour_angle = SolarFunctions.getHourAngle(solar_time);
                    double sin_Solar_Altitude_Angle_step = SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                    if (sin_Solar_Altitude_Angle_step > 0) {
                        sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                    }
                }
                double solar_altitude_angle = SolarFunctions.getSolarAltitudeAngle(sin_Solar_Altitude_Angle_sum / 60.0);
                double total_calculated_extraterrestrial_horizontal_radiation = Math.sin(Math.toRadians(solar_altitude_angle)) * calculated_extraterrestrial_direct_normal_radiation;
                morphedEPW.getEpw_data_fields().get(row_id).setN10_extraterrestrial_horizontal_radiation((float)total_calculated_extraterrestrial_horizontal_radiation);
            }
        }
    }
}
