/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.psychrometric.PsychrometricFunctions;
import futureweathergenerator.Months;

/**
 * Calculates N7 dew point temperature from future dry bulb temperature and
 * future relative humidity.
 *
 * @author eugenio
 */
public class N7_Dew_Point_Temperature {

    /**
     * Calculates N7 dew point temperature from future dry bulb temperature and
     * future relative humidity.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        // Requires pre-processing of:
        // N6 Dry Bulb Temperature
        // N8 Relative Humidity
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                double future_dry_bulb_temperature_floor = Math.floor(morphedEPW.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature());
                double future_dry_bulb_temperature_ceil = future_dry_bulb_temperature_floor + 1;
                double future_dry_bulb_temperature_dif = morphedEPW.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature() - future_dry_bulb_temperature_floor;
                double future_relative_humidity = morphedEPW.getEpw_data_fields().get(row_id).getN8_relative_humidity();
                double pws_value = PsychrometricFunctions.getAbsolutePressureOfWaterVapourAtSaturation(future_dry_bulb_temperature_floor);
                double pws_step = PsychrometricFunctions.getAbsolutePressureOfWaterVapourAtSaturation(future_dry_bulb_temperature_ceil) - pws_value;
                double pws = pws_value + (future_dry_bulb_temperature_dif * pws_step);
                double future_partial_pressure_of_water_vapour = (future_relative_humidity / 100.0) * pws;
                double c14 = 6.54;
                double c15 = 14.526;
                double c16 = 0.7389;
                double c17 = 0.09486;
                double c18 = 0.4569;
                double alpha = Math.log(future_partial_pressure_of_water_vapour);
                double calculated_dew_point_temperature;
                if (future_partial_pressure_of_water_vapour >= 0.61115) {
                    calculated_dew_point_temperature = c14 + (c15 * alpha) + (c16 * Math.pow(alpha, 2)) + (c17 * Math.pow(alpha, 3)) + (c18 * Math.pow(future_partial_pressure_of_water_vapour, 0.1984));
                } else {
                    calculated_dew_point_temperature = 6.09 + (12.608 * alpha) + (0.4959 * Math.pow(alpha, 2));
                }
                morphedEPW.getEpw_data_fields().get(row_id).setN7_dew_point_temperature((float) calculated_dew_point_temperature);
            }
        }
    }

}
