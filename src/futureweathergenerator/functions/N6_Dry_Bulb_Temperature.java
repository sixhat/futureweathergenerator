/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.Months;

/**
 * Morphs N6 dry bulb temperature.
 *
 * @author eugenio
 */
public class N6_Dry_Bulb_Temperature {

    /**
     * Morphs N6 dry bulb temperature.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW objects
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, AverageScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        float[] weather_month_minimum = new float[12];
        float[] weather_month_mean = new float[12];
        float[] weather_month_maximum = new float[12];
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            float[] temperatures = Utilities.getDailyMinAvgMax_MonthlyAverageValues(month_row_ids, epw.getEpw_data_fields(), "N6_dry_bulb_temperature");
            weather_month_minimum[i] = temperatures[0];
            weather_month_mean[i] = temperatures[1];
            weather_month_maximum[i] = temperatures[2];
        }
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float weather_month_average_minimum_dry_bulb_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, weather_month_minimum, numberOfHoursToSmooth);
                float weather_month_average_dry_bulb_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, weather_month_mean, numberOfHoursToSmooth);
                float weather_month_average_maximum_dry_bulb_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, weather_month_maximum, numberOfHoursToSmooth);
                float weather_month_average_minimum_dry_bulb_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, weather_month_average_minimum_dry_bulb_temperature_step, numberOfHoursToSmooth);
                float weather_month_average_dry_bulb_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, weather_month_average_dry_bulb_temperature_step, numberOfHoursToSmooth);
                float weather_month_average_maximum_dry_bulb_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, weather_month_average_maximum_dry_bulb_temperature_step, numberOfHoursToSmooth);
                float weather_difference_dry_bulb_temperature = 
                        (weather_month_maximum[i] + weather_month_average_maximum_dry_bulb_temperature_delta) -
                        (weather_month_minimum[i] + weather_month_average_minimum_dry_bulb_temperature_delta);
                float month_maximum_daily_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_maximum_daily_temperature(), numberOfHoursToSmooth);
                float month_minimum_daily_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_minimum_daily_temperature(), numberOfHoursToSmooth);
                float month_mean_daily_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_mean_daily_temperature(), numberOfHoursToSmooth);
                float month_maximum_daily_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_maximum_daily_temperature_step, numberOfHoursToSmooth);
                float month_minimum_daily_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_minimum_daily_temperature_step, numberOfHoursToSmooth);
                float month_mean_daily_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_mean_daily_temperature_step, numberOfHoursToSmooth);
                float month_maximum_daily_temperature = scenario.getInterpolated_maximum_daily_temperature()[i] + month_maximum_daily_temperature_delta;
                float month_minimum_daily_temperature = scenario.getInterpolated_minimum_daily_temperature()[i] + month_minimum_daily_temperature_delta;
                float month_mean_daily_temperature = scenario.getInterpolated_mean_daily_temperature()[i] + month_mean_daily_temperature_delta;
                float timeframe_difference_daily_temperature = month_maximum_daily_temperature - month_minimum_daily_temperature;
                float month_scaling_factor = timeframe_difference_daily_temperature / weather_difference_dry_bulb_temperature;
                float weather_dry_bulb_temperature = epw.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                float calculated_dry_bulb_temperature = weather_dry_bulb_temperature + month_mean_daily_temperature
                        + month_scaling_factor * (weather_dry_bulb_temperature - (weather_month_mean[i] + weather_month_average_dry_bulb_temperature_delta));
                morphedEPW.getEpw_data_fields().get(row_id).setN6_dry_bulb_temperature((float) calculated_dry_bulb_temperature);
            }
        }
    }
}
