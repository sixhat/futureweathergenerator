/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

/**
 * Enumerates the implemented simulation results from general circulation
 * models.
 *
 * @author eugenio
 */
public enum GCModel {
    /**
     * Hadley Centre Coupled Model version 3. URL:
     * https://www.metoffice.gov.uk/research/approach/modelling-systems/unified-model/climate-models/hadcm3
     */
    HadCM3,
    /**
     * European Community Earth-System Model 3. URL: http://www.ec-earth.org
     */
    EC_Earth3;
}
