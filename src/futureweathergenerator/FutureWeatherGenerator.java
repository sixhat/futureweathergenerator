/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.EC_Earth3.EC_Earth3;
import futureweathergenerator.GCM.HadCM3.HadCM3;

/**
 * This software generates future weather by morphing current EPW files to match
 * simulated general circulation model scenarios. This software also allows to
 * compare two EPW files.
 *
 * @author eugenio
 */
public class FutureWeatherGenerator implements Runnable {

    /**
     * Path to directory where GCM data is stored.
     */
    public static final String PATH_TO_RESOURCES = "resources/";

    /**
     * Filename of the HadCM3 grid with latitudes and longitudes for 96 x 73
     * world grid.
     */
    public static final String HADCM3_PATH_TO_GRID_96_73 = FutureWeatherGenerator.PATH_TO_RESOURCES + "HadCM3_grid_centre.kml";
    /**
     * Filename of the HadCM3 grid with latitudes and longitudes for 96 x 72
     * world grid.
     */
    public static final String HADCM3_PATH_TO_GRID_96_72 = FutureWeatherGenerator.PATH_TO_RESOURCES + "HadCM3_grid_WIND_centre.kml";

    /**
     * Filename of the app logo 23 px x 23 px size.
     */
    public static final String PATH_TO_ICON_23_23 = PATH_TO_RESOURCES + "icon_23_23.png";
    /**
     * Filename of the app logo 50 px x 50 px size.
     */
    public static final String PATH_TO_ICON_50_50 = PATH_TO_RESOURCES + "icon_50_50.png";
    /**
     * Filename of the app logo 100 px x 100 px size.
     */
    public static final String PATH_TO_ICON_100_100 = PATH_TO_RESOURCES + "icon_100_100.png";
    /**
     * Filename of the app logo 200 px x 200 px size.
     */
    public static final String PATH_TO_ICON_200_200 = PATH_TO_RESOURCES + "icon_200_200.png";
    /**
     * Filename of the app logo 500 px x 500 px size.
     */
    public static final String PATH_TO_ICON_500_500 = PATH_TO_RESOURCES + "icon_500_500.png";
    /**
     * Filename of the app logo 500 px x 500 px size cropped to content.
     */
    public static final String PATH_TO_ICON_500_500_WIN = PATH_TO_RESOURCES + "icon_500_500_win.png";

    /**
     * Filename for EC-Earth3 monthly variables.
     */
    public static final String OUTPUT_FILE_VARIABLES_EC_EARTH3 = "Grid_point_variables_" + GCModel.EC_Earth3.toString().replace("_", "-");
    /**
     * Filename for EC-Earth3 EPW and original EPW comparison.
     */
    public static final String OUTPUT_FILE_COMPARISON_EC_EARTH3 = "Morphed_EPWs_Comparison_" + GCModel.EC_Earth3.toString().replace("_", "-");
    /**
     * Filename for HadCM3 monthly variables.
     */
    public static final String OUTPUT_FILE_VARIABLES_HADCM3 = "Grid_point_variables_" + GCModel.HadCM3.toString();
    /**
     * Filename for HadCM3 original EPW comparison.
     */
    public static final String OUTPUT_FILE_COMPARISON_HADCM3 = "Morphed_EPWs_Comparison_" + GCModel.HadCM3.toString();

    private static final String OUTPUT_FILE_COMPARISON_TWO_EPW_FILES = "EPWs_Comparison";

    private final int NUMBER_OF_CPU_THREADS;

    private final String ACTION;
    private final String PATH_EPW_FILE_1;
    private final String PATH_EPW_FILE_2;
    private final String GCM;
    private final float NUMBER_OF_HOURS_TO_SMOOTH;
    private final String PATH_OUTPUT_FOLDER;

    /**
     * Actual future weather generator engine. The array of strings are the
     * running options.
     *
     * Generation [0] -g generate [1] path to EPW file [2] GCM (EC-Earth3 or
     * HadCM3) [3] number of hours to smooth monthly transitions [4] path to
     * output folder [5] run in multi-thread (true or false)
     *
     * Comparison [0] -c compare [1] path to EPW #1 file [2] path to EPW #2 file
     * [3] path to output folder
     *
     * @param args array of strings
     */
    public FutureWeatherGenerator(String[] args) {
        this.ACTION = args[0];
        if (args[0].equals("-c")) {
            this.PATH_EPW_FILE_1 = args[1];
            this.PATH_EPW_FILE_2 = args[2];
            this.GCM = null;
            this.NUMBER_OF_HOURS_TO_SMOOTH = 0.0f;
            this.PATH_OUTPUT_FOLDER = args[3];
            this.NUMBER_OF_CPU_THREADS = Runtime.getRuntime().availableProcessors();
        } else if (args[0].equals("-g")) {
            this.PATH_EPW_FILE_1 = args[1];
            this.PATH_EPW_FILE_2 = null;
            this.GCM = args[2];
            this.NUMBER_OF_HOURS_TO_SMOOTH = Float.parseFloat(args[3]);
            this.PATH_OUTPUT_FOLDER = args[4];
            if (Boolean.parseBoolean(args[5])) {
                this.NUMBER_OF_CPU_THREADS = Runtime.getRuntime().availableProcessors();
            } else {
                this.NUMBER_OF_CPU_THREADS = 1;
            }
        } else {
            throw new UnsupportedOperationException("** ERROR ** No action argument defined correctly (-c compare or -g generate.)");
        }
    }

    /**
     * Start the comparison or the generation process.
     */
    @Override
    public void run() {
        Info.echoHeader();
        if (this.ACTION.equals("-c")) {
            // Compares the morphing variables of two given EPW files
            EPW epw1 = new EPW(this.PATH_EPW_FILE_1, true);
            EPW epw2 = new EPW(this.PATH_EPW_FILE_2, true);
            // Saves comparison table between original and morphed file
            StringBuffer printOutputs = new StringBuffer();
            epw1.saveEPWComparison(printOutputs,
                    this.PATH_OUTPUT_FOLDER
                    + epw1.getEpw_location().getA3_country()
                    + "_"
                    + epw1.getEpw_location().getA1_city()
                    + "_"
                    + OUTPUT_FILE_COMPARISON_TWO_EPW_FILES
                    + ".csv", epw2);
            System.out.println(printOutputs.toString());
        }
        if (this.ACTION.equals("-g")) {
            // Generates a new future weather file from a given EPW file
            generateFutureWeather();
        }
        Info.echoFooter();
    }

    private void generateFutureWeather() {
        // Loads EPW file to morph
        EPW epw = new EPW(PATH_EPW_FILE_1, true);

        // Loads GCM variables and grip points
        switch (GCModel.valueOf(GCM.replace("-", "_"))) {
            // 1961-1990
            case HadCM3 -> {
                HadCM3.generate(NUMBER_OF_CPU_THREADS, PATH_OUTPUT_FOLDER, epw, NUMBER_OF_HOURS_TO_SMOOTH);
            }
            // 1985-2014
            case EC_Earth3 -> {
                EC_Earth3.generate(NUMBER_OF_CPU_THREADS, PATH_OUTPUT_FOLDER, epw, NUMBER_OF_HOURS_TO_SMOOTH);
            }
            default -> {
                throw new UnsupportedOperationException("** ERROR ** Incorrect GCM defined (HadCM3 or EC-Earth3).");
            }
        }
    }
}
