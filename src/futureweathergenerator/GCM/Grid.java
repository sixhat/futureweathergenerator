/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.FutureWeatherGenerator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sets the world grid of points by latitude and longitude.
 *
 * @author eugenio
 */
public class Grid {

    private final GridPoint[][] grid;
    private final int number_of_x_points;
    private final int number_of_y_points;

    /**
     * Initiates the grid object.
     *
     * @param grid array of grid points
     */
    public Grid(GridPoint[][] grid) {
        this.grid = grid;
        this.number_of_x_points = grid.length;
        this.number_of_y_points = grid[0].length;
    }

    /**
     * Initiates the world grid of points.
     *
     * @param path_grid path to the file with world points
     * @param number_of_x_points number of x points
     * @param number_of_y_points number of y points
     */
    public Grid(String path_grid, int number_of_x_points, int number_of_y_points) {
        this.number_of_x_points = number_of_x_points;
        this.number_of_y_points = number_of_y_points;
        this.grid = new GridPoint[number_of_x_points][number_of_y_points];

        try ( InputStream in = FutureWeatherGenerator.class.getResourceAsStream(path_grid);  BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line;
            while ((line = reader.readLine()) != null) {
                float longitude;
                float latitude;
                Integer x;
                Integer y;
                if (line.contains("<description>")) {
                    String nl = line.replace("<description><![CDATA[<HadCM3 grid edge<br>", "").replace("]]></description><styleUrl>#small_dot</styleUrl>", "");
                    String[] splitnl = nl.split("<br>");
                    String[] gridpoint = splitnl[0].replace("Gridpoint: ", "").split(",");
                    x = Integer.parseInt(gridpoint[0]);
                    y = Integer.parseInt(gridpoint[1]);
                    longitude = Float.parseFloat(splitnl[1].replace("Longitude: ", ""));
                    latitude = Float.parseFloat(splitnl[2].replace("Latitude: ", ""));
                    this.grid[x - 1][y - 1] = new GridPoint(x, y, latitude, longitude);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Grid.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns the grid as a 2-dimensional array.
     *
     * @return 2D array
     */
    public GridPoint[][] getGrid() {
        return grid;
    }

    /**
     * Returns the number of points in the x direction.
     *
     * @return number of points
     */
    public int getNumber_of_x_points() {
        return number_of_x_points;
    }

    /**
     * Returns the number of points in the y direction.
     *
     * @return number of points
     */
    public int getNumber_of_y_points() {
        return number_of_y_points;
    }

    /**
     * Selects the nearest four points to a given latitude and longitude.
     *
     * @param latitude latitude
     * @param longitude longitude
     * @return list of the four nearest points
     */
    public ArrayList<GridPoint> selectNearestFourGridPoints(float latitude, float longitude) {
        System.out.println("\nGrid size: " + this.number_of_x_points + " x " + this.number_of_y_points);
        System.out.println("Selecting the four nearest points to the weather file latitude and longitude...");
        System.out.println("Weather file latitude: " + latitude + ", longitude: " + longitude);
        ArrayList<GridPoint> selected_points = new ArrayList<>();

        float latminneg = Float.MAX_VALUE;
        Float latneg = null;
        float lonminneg = Float.MAX_VALUE;
        Float lonneg = null;

        float latminpos = Float.MAX_VALUE;
        Float latpos = null;
        float lonminpos = Float.MAX_VALUE;
        Float lonpos = null;

        for (int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                GridPoint gp = this.grid[i][j];
                float tlat = gp.getLatitude();
                float tlon = gp.getLongitude();
                float latdist = latitude - tlat;
                if (Math.abs(latdist) <= latminneg) {
                    if (latdist <= 0) {
                        latminneg = Math.abs(latdist);
                        latneg = tlat;
                    }
                }
                if (Math.abs(latdist) <= latminpos) {
                    if (latdist >= 0) {
                        latminpos = Math.abs(latdist);
                        latpos = tlat;
                    }
                }

                float londist = longitude - tlon;
                if (Math.abs(londist) <= lonminneg) {
                    if (londist <= 0) {
                        lonminneg = Math.abs(londist);
                        lonneg = tlon;
                    }
                }
                if (Math.abs(londist) <= lonminpos) {
                    if (londist >= 0) {
                        lonminpos = Math.abs(londist);
                        lonpos = tlon;
                    }
                }
            }
        }

        if (latneg == null || lonneg == null || latpos == null || lonpos == null) {
            System.out.println("ERROR: neg and pos are null");
        }

        for (int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                GridPoint gp = this.grid[i][j];
                float tlat = gp.getLatitude();
                float tlon = gp.getLongitude();
                Integer x = gp.getX();
                Integer y = gp.getY();

                if (lonneg == tlon && latneg == tlat) {
                    selected_points.add(gp);
                    System.out.println("\tGrid nearest point: latitude " + tlat + ", longitude " + tlon + ", grid point " + x + ", " + y);
                }
                if (lonneg == tlon && latpos == tlat) {
                    selected_points.add(gp);
                    System.out.println("\tGrid nearest point: latitude " + tlat + ", longitude " + tlon + ", grid point " + x + ", " + y);
                }
                if (lonpos == tlon && latneg == tlat) {
                    selected_points.add(gp);
                    System.out.println("\tGrid nearest point: latitude " + tlat + ", longitude " + tlon + ", grid point " + x + ", " + y);
                }
                if (lonpos == tlon && latpos == tlat) {
                    selected_points.add(gp);
                    System.out.println("\tGrid nearest point: latitude " + tlat + ", longitude " + tlon + ", grid point " + x + ", " + y);
                }
            }
        }
        return selected_points;
    }

    /**
     * Determines the point nearest to the five points of the given latitude and
     * longitude.
     *
     * @param gridSize grid size
     * @param latitude latitude
     * @param longitude longitude
     * @param grid_centre_points four nearest points
     * @param centre centre point of the four nearest points
     * @return grid point
     */
    public static GridPoint getNearestPoint(String gridSize, float latitude, float longitude, ArrayList<GridPoint> grid_centre_points, GridPoint centre) {
        float[] distance = new float[5];
        distance[0] = (float) (Math.pow(Math.pow(centre.getLatitude() - latitude, 2.0) + Math.pow(centre.getLongitude() - longitude, 2.0), 0.5));
        float minDistance = distance[0];
        int k = 0;
        for (int i = 0; i < grid_centre_points.size(); i++) {
            distance[i + 1] = (float) (Math.pow(Math.pow(grid_centre_points.get(i).getLatitude() - latitude, 2.0) + Math.pow(grid_centre_points.get(i).getLongitude() - longitude, 2.0), 0.5));
            if (minDistance > distance[i + 1]) {
                minDistance = distance[i + 1];
                k = i + 1;
            }
        }
        switch (k) {
            default:
            case 0:
                System.out.println("\t" + gridSize + ": the average of the four nearest points will be used.");
                return centre;
            case 1:
            case 2:
            case 3:
            case 4:
                System.out.println("\t" + gridSize + ": the nearest point of the four nearest points will be used.");
                return grid_centre_points.get(k - 1);
        }
    }

    /**
     * Converts 0° to 360° East longitude to -180° (W) to +180° (E) longitude.
     * @param longitudePositiveEast360 0° to 360° East longitude
     * @return -180° (W) to +180° (E) longitude
     */
    public static float correctLongitude(float longitudePositiveEast360) {
        // Convert longitude as simply positive East 360°
        // to longitude negative East 180°
        float newLongitude = longitudePositiveEast360;
        newLongitude = newLongitude > 180.0f ? (newLongitude - 360.0f) : newLongitude;
        return newLongitude;
    }

    /**
     * Converts -180° (W) to +180° (E) to 0° to 360° East longitude.
     *
     * @param longitude -180° (W) to 180° (E) longitude
     * @return 0° to 360° East longitude
     */
    public static double convertLongitudeTo360East(double longitude) {
        return longitude < 0 ? longitude + 360 : longitude;
    }
}
