/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

/**
 * Sets the grid point object.
 *
 * @author eugenio
 */
public class GridPoint {

    private final Integer x;
    private final Integer y;
    private final float latitude;
    private final float longitude;

    /**
     * Initiates a grid point.
     *
     * @param x x point id
     * @param y y point id
     * @param latitude latitude
     * @param longitude longitude
     */
    public GridPoint(Integer x, Integer y, float latitude, float longitude) {
        this.x = x;
        this.y = y;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Returns the point grid x id.
     *
     * @return point grid x id
     */
    public Integer getX() {
        return x;
    }

    /**
     * Returns the point grid y id.
     *
     * @return point grid y id
     */
    public Integer getY() {
        return y;
    }

    /**
     * Returns point latitude.
     *
     * @return latitude
     */
    public float getLatitude() {
        return latitude;
    }

    /**
     * Returns point longitude
     *
     * @return longitude
     */
    public float getLongitude() {
        return longitude;
    }

}
