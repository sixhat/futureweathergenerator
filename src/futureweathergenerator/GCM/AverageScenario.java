/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.GCModel;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * Sets the average GCM model scenario for the HadCM3.
 *
 * @author eugenio
 */
public class AverageScenario {

    private final GCModel gcm;
    private final String scenario;
    private final String timeframe;

    /**
     * Monthly interpolated mean daily temperature (∆°C).
     */
    public float[] interpolated_mean_daily_temperature; // TEMP
    /**
     * Monthly interpolated maximum daily temperature (∆°C).
     */
    public float[] interpolated_maximum_daily_temperature; // TMAX A2a A2C
    /**
     * Monthly interpolated minimum daily temperature (∆°C).
     */
    public float[] interpolated_minimum_daily_temperature; // TMIN A2a A2C
    /**
     * Monthly interpolated total downward surface shortwave flux (∆W/m2).
     */
    public float[] interpolated_total_downward_surface_shortwave_flux; // DSWF
    /**
     * Monthly interpolated total cloud in longwave radiation (∆%).
     */
    public float[] interpolated_total_cloud_in_longwave_radiation; // TCLW A2b A2c
    /**
     * Monthly interpolated total precipitation (fraction).
     */
    public float[] interpolated_total_precipitation; // PREC
    /**
     * Monthly interpolated relative humidity (∆%).
     */
    public float[] interpolated_relative_humidity; // RHUM
    /**
     * Monthly interpolated mean sea level pressure (∆hPa).
     */
    public float[] interpolated_mean_sea_level_pressure; // MSLP A2a
    /**
     * Monthly interpolated wind speed (fraction).
     */
    public float[] interpolated_wind_speed; // WIND

    /**
     * Initiates the AverageScenario object, which will average A2a, A2b, A2c
     * emissions scenarios of the HadCM3 model.
     *
     * @param gcm general circulation model
     * @param timeframe future timeframe
     * @param scenario final scenario
     */
    public AverageScenario(GCModel gcm, String timeframe, String scenario) {
        this.gcm = gcm;
        this.timeframe = timeframe;
        this.scenario = scenario;
    }

    /**
     * Initiates the AverageScenario object from given scenarios.
     *
     * @param scenario scenario
     */
    public AverageScenario(futureweathergenerator.GCM.EC_Earth3.Scenario scenario) {
        this.gcm = GCModel.EC_Earth3;
        this.timeframe = scenario.getTimeframe();
        this.scenario = scenario.getScenario();
        this.interpolated_mean_daily_temperature = scenario.getInterpolated_mean_daily_temperature();
        this.interpolated_maximum_daily_temperature = scenario.getInterpolated_maximum_daily_temperature();
        this.interpolated_minimum_daily_temperature = scenario.getInterpolated_minimum_daily_temperature();
        this.interpolated_total_downward_surface_shortwave_flux = scenario.getInterpolated_total_downward_surface_shortwave_flux();
        this.interpolated_total_cloud_in_longwave_radiation = scenario.getInterpolated_total_cloud_in_longwave_radiation();
        this.interpolated_total_precipitation = scenario.getInterpolated_total_precipitation();
        this.interpolated_relative_humidity = scenario.getInterpolated_relative_humidity();
        this.interpolated_mean_sea_level_pressure = scenario.getInterpolated_mean_sea_level_pressure();
        this.interpolated_wind_speed = scenario.getInterpolated_wind_speed();
    }

    /**
     * Initiates the AverageScenario object from given scenarios.
     *
     * @param scenario_name final scenario_name
     * @param scenario_A2a A2a scenario_name
     * @param scenario_A2b A2b scenario_name
     * @param scenario_A2c A2c scenario_name
     */
    public AverageScenario(String scenario_name, futureweathergenerator.GCM.HadCM3.Scenario scenario_A2a, futureweathergenerator.GCM.HadCM3.Scenario scenario_A2b, futureweathergenerator.GCM.HadCM3.Scenario scenario_A2c) {
        this.gcm = GCModel.HadCM3;
        this.timeframe = scenario_A2a.getTimeframe();
        this.scenario = scenario_name;

        this.interpolated_mean_daily_temperature = getScenarioAverages(
                scenario_A2a.getInterpolated_mean_daily_temperature(),
                scenario_A2b.getInterpolated_mean_daily_temperature(),
                scenario_A2c.getInterpolated_mean_daily_temperature());
        this.interpolated_maximum_daily_temperature = getScenarioAverages(
                scenario_A2a.getInterpolated_maximum_daily_temperature(),
                scenario_A2b.getInterpolated_maximum_daily_temperature(),
                scenario_A2c.getInterpolated_maximum_daily_temperature());
        this.interpolated_minimum_daily_temperature = getScenarioAverages(
                scenario_A2a.getInterpolated_minimum_daily_temperature(),
                scenario_A2b.getInterpolated_minimum_daily_temperature(),
                scenario_A2c.getInterpolated_minimum_daily_temperature());
        this.interpolated_total_downward_surface_shortwave_flux = getScenarioAverages(
                scenario_A2a.getInterpolated_total_downward_surface_shortwave_flux(),
                scenario_A2b.getInterpolated_total_downward_surface_shortwave_flux(),
                scenario_A2c.getInterpolated_total_downward_surface_shortwave_flux());
        this.interpolated_total_cloud_in_longwave_radiation = getScenarioAverages(
                scenario_A2a.getInterpolated_total_cloud_in_longwave_radiation(),
                scenario_A2b.getInterpolated_total_cloud_in_longwave_radiation(),
                scenario_A2c.getInterpolated_total_cloud_in_longwave_radiation());
        this.interpolated_total_precipitation = getScenarioAverages(
                scenario_A2a.getInterpolated_total_precipitation(),
                scenario_A2b.getInterpolated_total_precipitation(),
                scenario_A2c.getInterpolated_total_precipitation());
        this.interpolated_relative_humidity = getScenarioAverages(
                scenario_A2a.getInterpolated_relative_humidity(),
                scenario_A2b.getInterpolated_relative_humidity(),
                scenario_A2c.getInterpolated_relative_humidity());
        this.interpolated_mean_sea_level_pressure = getScenarioAverages(
                scenario_A2a.getInterpolated_mean_sea_level_pressure(),
                scenario_A2b.getInterpolated_mean_sea_level_pressure(),
                scenario_A2c.getInterpolated_mean_sea_level_pressure());
        this.interpolated_wind_speed = getScenarioAverages(
                scenario_A2a.getInterpolated_wind_speed(),
                scenario_A2b.getInterpolated_wind_speed(),
                scenario_A2c.getInterpolated_wind_speed());
    }

    /**
     * Returns the general circulation model type.
     *
     * @return GCM type
     */
    public GCModel getGcm() {
        return gcm;
    }

    /**
     * Returns string name of the scenario.
     *
     * @return string
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Returns string name of the timeframe.
     *
     * @return string
     */
    public String getTimeframe() {
        return timeframe;
    }

    private float[] getScenarioAverages(float[] variable_A2a, float[] variable_A2b, float[] variable_A2c) {
        float[] var = new float[12];
        int count_nulls = 0;
        if (variable_A2a == null) {
            count_nulls++;
        }
        if (variable_A2b == null) {
            count_nulls++;
        }
        if (variable_A2c == null) {
            count_nulls++;
        }
        for (int i = 0; i < var.length; i++) {
            float vA = variable_A2a != null ? variable_A2a[i] : 0.0f;
            float vB = variable_A2b != null ? variable_A2b[i] : 0.0f;
            float vC = variable_A2c != null ? variable_A2c[i] : 0.0f;
            var[i] = (vA + vB + vC) / (3 - count_nulls);
        }
        return var;
    }

    /**
     * Returns an array of monthly interpolated total downward surface shortwave
     * flux (∆w/m2).
     *
     * @return array
     */
    public float[] getInterpolated_total_downward_surface_shortwave_flux() {
        return interpolated_total_downward_surface_shortwave_flux;
    }

    /**
     * Returns an array of monthly interpolated total precipitation (fraction).
     *
     * @return array
     */
    public float[] getInterpolated_total_precipitation() {
        return interpolated_total_precipitation;
    }

    /**
     * Returns an array of monthly interpolated relative humidity (∆%).
     *
     * @return array
     */
    public float[] getInterpolated_relative_humidity() {
        return interpolated_relative_humidity;
    }

    /**
     * Returns an array of monthly interpolated mean daily temperature (∆°C).
     *
     * @return array
     */
    public float[] getInterpolated_mean_daily_temperature() {
        return interpolated_mean_daily_temperature;
    }

    /**
     * Returns an array of monthly interpolated maximum daily temperature (∆°C).
     *
     * @return array
     */
    public float[] getInterpolated_maximum_daily_temperature() {
        return interpolated_maximum_daily_temperature;
    }

    /**
     * Returns an array of monthly interpolated minimum daily temperature (∆°C).
     *
     * @return array
     */
    public float[] getInterpolated_minimum_daily_temperature() {
        return interpolated_minimum_daily_temperature;
    }

    /**
     * Returns an array of monthly interpolated wind speed (fraction).
     *
     * @return array
     */
    public float[] getInterpolated_wind_speed() {
        return interpolated_wind_speed;
    }

    /**
     * Returns an array of monthly interpolated mean sea level pressure (∆hPa).
     *
     * @return array
     */
    public float[] getInterpolated_mean_sea_level_pressure() {
        return interpolated_mean_sea_level_pressure;
    }

    /**
     * Returns an array of monthly interpolated total cloud in longwave
     * radiation (∆W/m2).
     *
     * @return array
     */
    public float[] getInterpolated_total_cloud_in_longwave_radiation() {
        return interpolated_total_cloud_in_longwave_radiation;
    }

    /**
     * Saves a csv table of the GCM interpolated variables.
     *
     * @param printOutputs String buffer
     * @param gcm General Circulation Model
     * @param path_output path to csv file
     */
    public void saveTable_GCM_variables(StringBuffer printOutputs, GCModel gcm, String path_output) {
        printOutputs.append("\n\tSaving ").append(gcm.toString().replace("_", "-")).append(" monthly variable values to file...\n\t").append(path_output).append("\n");
        StringBuilder sb = new StringBuilder();
        sb.append("# ").append(gcm.toString().replace("_", "-")).append(" ").append(this.scenario).append(" ").append(this.timeframe).append("\n");
        sb.append("Variable,Abbreviation,Unit,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec\n");
        sb.append(String.format(Locale.ROOT, "Daily mean temperature,TEMP,∆°C,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n",
                interpolated_mean_daily_temperature[0],
                interpolated_mean_daily_temperature[1],
                interpolated_mean_daily_temperature[2],
                interpolated_mean_daily_temperature[3],
                interpolated_mean_daily_temperature[4],
                interpolated_mean_daily_temperature[5],
                interpolated_mean_daily_temperature[6],
                interpolated_mean_daily_temperature[7],
                interpolated_mean_daily_temperature[8],
                interpolated_mean_daily_temperature[9],
                interpolated_mean_daily_temperature[10],
                interpolated_mean_daily_temperature[11]));
        sb.append(String.format(Locale.ROOT, "Maximum temperature,TMAX,∆°C,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n",
                interpolated_maximum_daily_temperature[0],
                interpolated_maximum_daily_temperature[1],
                interpolated_maximum_daily_temperature[2],
                interpolated_maximum_daily_temperature[3],
                interpolated_maximum_daily_temperature[4],
                interpolated_maximum_daily_temperature[5],
                interpolated_maximum_daily_temperature[6],
                interpolated_maximum_daily_temperature[7],
                interpolated_maximum_daily_temperature[8],
                interpolated_maximum_daily_temperature[9],
                interpolated_maximum_daily_temperature[10],
                interpolated_maximum_daily_temperature[11]));
        sb.append(String.format(Locale.ROOT, "Minimum temperature,TMIN,∆°C,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n",
                interpolated_minimum_daily_temperature[0],
                interpolated_minimum_daily_temperature[1],
                interpolated_minimum_daily_temperature[2],
                interpolated_minimum_daily_temperature[3],
                interpolated_minimum_daily_temperature[4],
                interpolated_minimum_daily_temperature[5],
                interpolated_minimum_daily_temperature[6],
                interpolated_minimum_daily_temperature[7],
                interpolated_minimum_daily_temperature[8],
                interpolated_minimum_daily_temperature[9],
                interpolated_minimum_daily_temperature[10],
                interpolated_minimum_daily_temperature[11]));
        sb.append(String.format(Locale.ROOT, "Total downward surface shortwave flux,DSWF,∆W/m²,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n",
                interpolated_total_downward_surface_shortwave_flux[0],
                interpolated_total_downward_surface_shortwave_flux[1],
                interpolated_total_downward_surface_shortwave_flux[2],
                interpolated_total_downward_surface_shortwave_flux[3],
                interpolated_total_downward_surface_shortwave_flux[4],
                interpolated_total_downward_surface_shortwave_flux[5],
                interpolated_total_downward_surface_shortwave_flux[6],
                interpolated_total_downward_surface_shortwave_flux[7],
                interpolated_total_downward_surface_shortwave_flux[8],
                interpolated_total_downward_surface_shortwave_flux[9],
                interpolated_total_downward_surface_shortwave_flux[10],
                interpolated_total_downward_surface_shortwave_flux[11]));
        sb.append(String.format(Locale.ROOT, "Total cloud in longwave radiation,TCLW,∆%%,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n",
                interpolated_total_cloud_in_longwave_radiation[0],
                interpolated_total_cloud_in_longwave_radiation[1],
                interpolated_total_cloud_in_longwave_radiation[2],
                interpolated_total_cloud_in_longwave_radiation[3],
                interpolated_total_cloud_in_longwave_radiation[4],
                interpolated_total_cloud_in_longwave_radiation[5],
                interpolated_total_cloud_in_longwave_radiation[6],
                interpolated_total_cloud_in_longwave_radiation[7],
                interpolated_total_cloud_in_longwave_radiation[8],
                interpolated_total_cloud_in_longwave_radiation[9],
                interpolated_total_cloud_in_longwave_radiation[10],
                interpolated_total_cloud_in_longwave_radiation[11]));
        sb.append(String.format(Locale.ROOT, "Total precipitation,PREC,fraction,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f\n",
                interpolated_total_precipitation[0],
                interpolated_total_precipitation[1],
                interpolated_total_precipitation[2],
                interpolated_total_precipitation[3],
                interpolated_total_precipitation[4],
                interpolated_total_precipitation[5],
                interpolated_total_precipitation[6],
                interpolated_total_precipitation[7],
                interpolated_total_precipitation[8],
                interpolated_total_precipitation[9],
                interpolated_total_precipitation[10],
                interpolated_total_precipitation[11]));
        sb.append(String.format(Locale.ROOT, "Relative humidity,RHUM,∆%%,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n",
                interpolated_relative_humidity[0],
                interpolated_relative_humidity[1],
                interpolated_relative_humidity[2],
                interpolated_relative_humidity[3],
                interpolated_relative_humidity[4],
                interpolated_relative_humidity[5],
                interpolated_relative_humidity[6],
                interpolated_relative_humidity[7],
                interpolated_relative_humidity[8],
                interpolated_relative_humidity[9],
                interpolated_relative_humidity[10],
                interpolated_relative_humidity[11]));
        sb.append(String.format(Locale.ROOT, "Mean sea level pressure,MSLP,∆hPa,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n",
                interpolated_mean_sea_level_pressure[0],
                interpolated_mean_sea_level_pressure[1],
                interpolated_mean_sea_level_pressure[2],
                interpolated_mean_sea_level_pressure[3],
                interpolated_mean_sea_level_pressure[4],
                interpolated_mean_sea_level_pressure[5],
                interpolated_mean_sea_level_pressure[6],
                interpolated_mean_sea_level_pressure[7],
                interpolated_mean_sea_level_pressure[8],
                interpolated_mean_sea_level_pressure[9],
                interpolated_mean_sea_level_pressure[10],
                interpolated_mean_sea_level_pressure[11]));
        sb.append(String.format(Locale.ROOT, "Wind speed,WIND,fraction,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f\n",
                interpolated_wind_speed[0],
                interpolated_wind_speed[1],
                interpolated_wind_speed[2],
                interpolated_wind_speed[3],
                interpolated_wind_speed[4],
                interpolated_wind_speed[5],
                interpolated_wind_speed[6],
                interpolated_wind_speed[7],
                interpolated_wind_speed[8],
                interpolated_wind_speed[9],
                interpolated_wind_speed[10],
                interpolated_wind_speed[11]));

        try ( OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            writer.write(sb.toString());
            writer.close();
        } catch (FileNotFoundException ex) {
            throw new UnsupportedOperationException(ex);
        } catch (IOException ex) {
            throw new UnsupportedOperationException(ex);
        }
    }

}
