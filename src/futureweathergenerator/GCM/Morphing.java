/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.A1_Data_Source_And_Uncertainty_Flag;
import futureweathergenerator.functions.Comments;
import futureweathergenerator.functions.Design_Conditions;
import futureweathergenerator.functions.Ground_Temperatures;
import futureweathergenerator.functions.N10_Extraterrestrial_Horizontal_Radiation;
import futureweathergenerator.functions.N11_Extraterrestrial_Direct_Normal_Radiation;
import futureweathergenerator.functions.N12_Horizontal_Infrared_Radiation_Intensity;
import futureweathergenerator.functions.N13_Global_Horizontal_Radiation;
import futureweathergenerator.functions.N14_Direct_Normal_Radiation;
import futureweathergenerator.functions.N15_Diffuse_Horizontal_Radiation;
import futureweathergenerator.functions.N16_Global_Horizontal_Illuminance;
import futureweathergenerator.functions.N17_Direct_Normal_Illuminance;
import futureweathergenerator.functions.N18_Diffuse_Horizontal_Illuminance;
import futureweathergenerator.functions.N19_Zenith_Luminance;
import futureweathergenerator.functions.N1_Year;
import futureweathergenerator.functions.N20_Wind_Direction;
import futureweathergenerator.functions.N21_Wind_Speed;
import futureweathergenerator.functions.N22_Total_Sky_Cover;
import futureweathergenerator.functions.N23_Opaque_Sky_Cover;
import futureweathergenerator.functions.N24_Visibility;
import futureweathergenerator.functions.N25_Ceiling_Height;
import futureweathergenerator.functions.N26_Present_Weather_Observation;
import futureweathergenerator.functions.N27_Present_Weather_Code;
import futureweathergenerator.functions.N28_Precipitable_Water;
import futureweathergenerator.functions.N29_Aerosol_Optical_Depth;
import futureweathergenerator.functions.N30_Snow_Depth;
import futureweathergenerator.functions.N31_Days_Since_Last_Snowfall;
import futureweathergenerator.functions.N32_Albedo;
import futureweathergenerator.functions.N33_Liquid_Precipitation_Depth;
import futureweathergenerator.functions.N34_Liquid_Precipitation_Quantity;
import futureweathergenerator.functions.N6_Dry_Bulb_Temperature;
import futureweathergenerator.functions.N7_Dew_Point_Temperature;
import futureweathergenerator.functions.N8_Relative_Humidity;
import futureweathergenerator.functions.N9_Atmospheric_Pressure;
import futureweathergenerator.functions.Typical_Extreme_Periods;

/**
 * Morphs the current EPW data.
 * @author eugenio
 */
public class Morphing {

    /**
     * Morphs the current EPW file into the future EPW data.
     *
     * @param printOutputs String Buffer to store prints
     * @param epw current EPW file
     * @param scenario future scenario to use
     * @param morphedEPW morphed EPW data
     * @param numberOfHoursToSmooth number of hours to smooth month transition
     */
    public static void createMorphedEPWFile(StringBuffer printOutputs, EPW epw, AverageScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        printOutputs.append("\n\tGenerating future EPW file...").append("\n");

        // Non-environment Fields
        printOutputs.append("\t\tSetting year, data source and uncertainty flag fields...").append("\n");
        N1_Year.set(scenario, morphedEPW);
        A1_Data_Source_And_Uncertainty_Flag.update(morphedEPW);

        // Removing design conditions
        printOutputs.append("\t\tRemoving design conditions...").append("\n");
        Design_Conditions.reset(morphedEPW);

        // Removing typical/extreme periods
        printOutputs.append("\t\tRemoving typical/extreme periods...").append("\n");
        Typical_Extreme_Periods.reset(morphedEPW);

        // Add comments about the generated data
        printOutputs.append("\t\tAdding comments...").append("\n");
        Comments.update(scenario, morphedEPW);

        // Environment Fields
        // WARNING: The order in which the Fields are morphed must be kept! //
        printOutputs.append("\t\tMorphing environment fields...").append("\n");

        // Independent Future Variables
        N6_Dry_Bulb_Temperature.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N8_Relative_Humidity.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        // NOTICE:
        // CCWorldWeatherGen seems to have an incorrect unit conversion for the mean sea level pressure  
        // from hPa to Pa, which results in very low atmospheric pressure (N9) changes in the final EPW file.
        // Here, we correctly convert hPa to Pa during morphing and do not change the units 
        // during import of the HadCM3 data.
        N9_Atmospheric_Pressure.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N10_Extraterrestrial_Horizontal_Radiation.calculate(epw, morphedEPW);
        N11_Extraterrestrial_Direct_Normal_Radiation.calculate(epw, morphedEPW);
        N13_Global_Horizontal_Radiation.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N21_Wind_Speed.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N22_Total_Sky_Cover.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N28_Precipitable_Water.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        // Fields that are kept the same and not morphed:
        N20_Wind_Direction.keepOriginal(epw, morphedEPW);
        // Fields to be set as missing data:
        N24_Visibility.setMissing(morphedEPW);
        N25_Ceiling_Height.setMissing(morphedEPW);
        N26_Present_Weather_Observation.setMissing(morphedEPW);
        N27_Present_Weather_Code.setMissing(morphedEPW);
        N29_Aerosol_Optical_Depth.setMissing(morphedEPW);
        N30_Snow_Depth.setMissing(morphedEPW);
        N31_Days_Since_Last_Snowfall.setMissing(morphedEPW);
        N32_Albedo.setMissing(morphedEPW);
        N33_Liquid_Precipitation_Depth.setMissing(morphedEPW);
        N34_Liquid_Precipitation_Quantity.setMissing(morphedEPW);

        // Dependent Future Variables
        N7_Dew_Point_Temperature.calculate(morphedEPW);
        N12_Horizontal_Infrared_Radiation_Intensity.calculate(morphedEPW);
        N15_Diffuse_Horizontal_Radiation.morph(epw, scenario, morphedEPW);
        N14_Direct_Normal_Radiation.calculate(epw, morphedEPW);
        // NOTICE:
        // CCWorldWeatherGen outputs different values for the 
        // illuminances and luminance (N16 to N19 fields).
        N16_Global_Horizontal_Illuminance.calculate(epw, morphedEPW);
        N17_Direct_Normal_Illuminance.calculate(epw, morphedEPW);
        N18_Diffuse_Horizontal_Illuminance.calculate(epw, morphedEPW);
        N19_Zenith_Luminance.calculate(epw, morphedEPW);
        N23_Opaque_Sky_Cover.calculate(epw, morphedEPW);

        // Ground Temperatures at different depths
        printOutputs.append("\t\tGround temperatures at different depths...").append("\n");
        Ground_Temperatures.morph(morphedEPW);
    }

}
