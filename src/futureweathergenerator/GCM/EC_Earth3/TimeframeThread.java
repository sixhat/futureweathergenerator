/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.EC_Earth3;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.GCM.Morphing;
import futureweathergenerator.GCModel;
import java.util.concurrent.CountDownLatch;

/**
 * Initiates a timeframe thread.
 * 
 * @author eugenio
 */
public class TimeframeThread implements Runnable {

    private final String PATH_OUTPUT_FOLDER;
    private final EPW epw;
    private final String timeframe_trigger;
    private final String scenario_trigger;
    private final EC_Earth3 ec_Earth3;
    private final float numberOfHoursToSmooth;
    private final CountDownLatch dS;

    /**
     * Initializes the timeframe thread.
     *
     * @param PATH_OUTPUT_FOLDER path to the output folder
     * @param epw original EPW
     * @param timeframe_trigger timeframe name
     * @param scenario_trigger scenario name
     * @param ec_Earth3 EC-Earth3
     * @param numberOfHoursToSmooth number of hours to smooth month transition
     * @param dS thread count down latch
     */
    public TimeframeThread(String PATH_OUTPUT_FOLDER, EPW epw, String timeframe_trigger, String scenario_trigger, EC_Earth3 ec_Earth3, float numberOfHoursToSmooth, CountDownLatch dS) {
        this.PATH_OUTPUT_FOLDER = PATH_OUTPUT_FOLDER;
        this.epw = epw;
        this.timeframe_trigger = timeframe_trigger;
        this.scenario_trigger = scenario_trigger;
        this.ec_Earth3 = ec_Earth3;
        this.numberOfHoursToSmooth = numberOfHoursToSmooth;
        this.dS = dS;
    }

    /**
     * Runs this thread.
     */
    @Override
    public void run() {
        StringBuffer printOutputs = new StringBuffer();
        printOutputs.append("\nGenerating ").append(timeframe_trigger).append(" timeframe...\n");

        EPW morphedEPW = new EPW(epw.getPath_EPW(), false);

        // Selects the chosen timeframe
        Timeframe timeframe;
        switch (timeframe_trigger) {
            case "2050" ->
                timeframe = ec_Earth3.getTimeframe_2050();
            case "2080" ->
                timeframe = ec_Earth3.getTimeframe_2080();
            default ->
                throw new UnsupportedOperationException("Error: Unknown Timeframe: " + timeframe_trigger);
        }

        Scenario ssp;
        switch (scenario_trigger) {
            case "ssp126" ->
                ssp = timeframe.getSsp126();
            case "ssp245" ->
                ssp = timeframe.getSsp245();
            case "ssp370" ->
                ssp = timeframe.getSsp370();
            case "ssp585" ->
                ssp = timeframe.getSsp585();
            default ->
                throw new UnsupportedOperationException("Error: Unknown Timeframe: " + timeframe_trigger);
        }

        // Loads the SSPs
        printOutputs.append("\tLoading Shared Socioeconomic Pathway ").append(scenario_trigger).append("...\n");
        AverageScenario scenario = new AverageScenario(ssp);

        String path = PATH_OUTPUT_FOLDER
                + epw.getEpw_location().getA3_country().replace("/", "-").replace("\\", "-")
                + "_"
                + epw.getEpw_location().getA1_city().replace("/", "-").replace("\\", "-")
                + "_";
        
        // Saves EC-Earth3 interpolated variables
        scenario.saveTable_GCM_variables(printOutputs,
                GCModel.EC_Earth3,
                path
                + FutureWeatherGenerator.OUTPUT_FILE_VARIABLES_EC_EARTH3
                + "_"
                + scenario_trigger
                + "_"
                + timeframe_trigger
                + ".csv");

        // Generates future weather
        Morphing.createMorphedEPWFile(printOutputs, epw, scenario, morphedEPW, numberOfHoursToSmooth);

        // Saves comparison table between original and morphed file
        morphedEPW.saveEPWComparison(printOutputs,
                path
                + FutureWeatherGenerator.OUTPUT_FILE_COMPARISON_EC_EARTH3
                + "_"
                + scenario_trigger
                + "_"
                + timeframe_trigger
                + ".csv",
                epw);

        // Saves morphed weather to file
        morphedEPW.saveEPW(printOutputs,
                path
                + GCModel.EC_Earth3.toString().replace("_", "-")
                + "_"
                + scenario_trigger
                + "_"
                + timeframe_trigger
                + ".epw");

        System.out.println(printOutputs.toString());
        this.dS.countDown();
    }
}
