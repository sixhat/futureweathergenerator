/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.EC_Earth3;

import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.GCM.Grid;
import futureweathergenerator.GCM.Variable;
import futureweathergenerator.Months;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.nc2.NetcdfFile;
import ucar.nc2.NetcdfFiles;

/**
 * Creates a thread to read EC-Earth3 file.
 *
 * @author eugenio
 */
public class ReadFileThread implements Runnable {

    private final int i;
    private final Variable[][] variables;
    private final ArrayList<Settings> variableSettings;
    private final CountDownLatch dS;

    /**
     * Initiates a thread for the EC-Earth3 file reading.
     *
     * @param i current month
     * @param variables array of variables
     * @param variableSettings variable settings
     * @param dS thread count down latch
     */
    public ReadFileThread(int i, Variable[][] variables, ArrayList<Settings> variableSettings, CountDownLatch dS) {
        this.i = i;
        this.variables = variables;
        this.variableSettings = variableSettings;
        this.dS = dS;
    }

    /**
     * Runs this thread.
     */
    @Override
    public void run() {
        variables[i] = getVariable(
                variableSettings.get(i).getPath(),
                variableSettings.get(i).getScenario(),
                variableSettings.get(i).getVariable(),
                variableSettings.get(i).getTimeframe()
        );
        this.dS.countDown();
    }

    private static Variable[] getVariable(String path_EC_Earth3, String scenario, String variable_abbreviation, String timeframe) {
        String filename = "EC-Earth3_" + scenario + "_" + variable_abbreviation + "_" + timeframe + ".nc";
        URL url = FutureWeatherGenerator.class.getResource(path_EC_Earth3 + filename);
        try ( NetcdfFile ncfile = NetcdfFiles.openInMemory(url.toURI())) {
            ucar.nc2.Variable lon = ncfile.findVariable("lon");
            assert null != lon;
            Array lon_data = lon.read();
            assert (lon_data instanceof ucar.ma2.ArrayFloat);
            IndexIterator lon_dataI = lon_data.getIndexIterator();
            ArrayList<Float> longitudes = new ArrayList();
            while (lon_dataI.hasNext()) {
                longitudes.add(Grid.correctLongitude(lon_dataI.getFloatNext()));
            }

            ucar.nc2.Variable lat = ncfile.findVariable("lat");
            assert null != lat;
            Array lat_data = lat.read();
            assert (lat_data instanceof ucar.ma2.ArrayFloat);
            IndexIterator lat_dataI = lat_data.getIndexIterator();
            ArrayList<Double> latitudes = new ArrayList();
            while (lat_dataI.hasNext()) {
                latitudes.add(lat_dataI.getDoubleNext());
            }

            // Get Variable Values
            ucar.nc2.Variable v = ncfile.findVariable(variable_abbreviation);
            assert null != v;
            Array data = v.read();
            assert (data instanceof ucar.ma2.ArrayFloat);
            IndexIterator dataI = data.getIndexIterator();
            System.out.println("\tVariable File: " + filename);
            Variable[] variables = new Variable[12];
            for (int i = 0; i < Months.Abbreviation.values().length; i++) {
                Months.Abbreviation month = Months.Abbreviation.values()[i];
                float[][] grid_values = new float[latitudes.size()][longitudes.size()];
                for (int j = 0; j < grid_values.length; j++) {
                    for (int k = 0; k < grid_values[j].length; k++) {
                        grid_values[j][k] = dataI.getFloatNext();
                    }
                }

                variables[i] = new Variable(
                        latitudes.size(),
                        longitudes.size(),
                        month,
                        scenario,
                        timeframe,
                        grid_values);

            }
            ncfile.close();
            return variables;
        } catch (URISyntaxException | IOException ex) {
            return null;
        }
    }
}
