/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.EC_Earth3;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.GCM.Grid;
import futureweathergenerator.GCM.GridPoint;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.nc2.NetcdfFile;
import ucar.nc2.NetcdfFiles;

/**
 * Generates the future EPW file based on the EC-Earth3 model.
 *
 * @author eugenio
 */
public class EC_Earth3 {

    private static final ArrayList<GridPoint> grid_centre_points = new ArrayList<>();

    private final Timeframe timeframe_2050; // 2036-2065
    private final Timeframe timeframe_2080; // 2066-2095

    /**
     * Generates the future morphed EPW file based on the EC-Earth3 model.
     *
     * @param NUMBER_OF_CPU_THREADS number of threads
     * @param PATH_OUTPUT_FOLDER path to the output folder
     * @param epw current EPW file
     * @param numberOfHoursToSmooth number of hours to smooth the month
     * transition
     */
    public static void generate(int NUMBER_OF_CPU_THREADS, String PATH_OUTPUT_FOLDER, EPW epw, float numberOfHoursToSmooth) {
        System.out.println("\nInitializing EC-EARTH3...");
        EC_Earth3 ec_Earth3 = new EC_Earth3(
                NUMBER_OF_CPU_THREADS,
                epw.getEpw_location().getN2_latitude(),
                epw.getEpw_location().getN3_longitude(),
                FutureWeatherGenerator.PATH_TO_RESOURCES);
        String[] timeframe_triggers = new String[]{"2050", "2080"};
        String[] scenario_triggers = new String[]{"ssp126", "ssp245", "ssp370", "ssp585"};
        ExecutorService pool = Executors.newFixedThreadPool(NUMBER_OF_CPU_THREADS);
        CountDownLatch dS = new CountDownLatch(timeframe_triggers.length * scenario_triggers.length);
        for (String timeframe_trigger : timeframe_triggers) {
            for (String scenario_trigger : scenario_triggers) {
                pool.execute(new TimeframeThread(PATH_OUTPUT_FOLDER, epw, timeframe_trigger, scenario_trigger, ec_Earth3, numberOfHoursToSmooth, dS));
            }
        }
        try {
            dS.await();
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        pool.shutdown();
    }

    /**
     * Initiates the EC-Earth3 model.
     *
     * @param NUMBER_OF_CPU_THREADS number of threads
     * @param latitude current EPW file latitude
     * @param longitude current EPW file longitude
     * @param path_EC_EARTH3 path to the EC-Earth3 resources
     */
    public EC_Earth3(
            int NUMBER_OF_CPU_THREADS,
            float latitude,
            float longitude,
            String path_EC_EARTH3) {
        // Create World Grid
        createGrid(FutureWeatherGenerator.PATH_TO_RESOURCES, "ssp126", "tas", "2050", latitude, longitude, grid_centre_points);

        // 2036-2065 Mean monthly change fields -> 2050
        System.out.println("\nLoading EC-Earth3 timeframe 2050");
        this.timeframe_2050 = new Timeframe(NUMBER_OF_CPU_THREADS, path_EC_EARTH3, "2050", latitude, longitude, grid_centre_points);

        // 2066-2095 Mean monthly change fields -> 2080
        System.out.println("\nLoading EC-Earth3 timeframe 2080");
        this.timeframe_2080 = new Timeframe(NUMBER_OF_CPU_THREADS, path_EC_EARTH3, "2080", latitude, longitude, grid_centre_points);
    }

    private static void createGrid(String path_EC_Earth3, String scenario, String variable_abbreviation, String timeframe, float latitude, float longitude, ArrayList<GridPoint> grid_centre_points) {
        System.out.println("\nLoading grid...");
        String filename = "EC-Earth3_" + scenario + "_" + variable_abbreviation + "_" + timeframe + ".nc";
        URL url = FutureWeatherGenerator.class.getResource(path_EC_Earth3 + filename);
        try ( NetcdfFile ncfile = NetcdfFiles.openInMemory(url.toURI())) {
            ucar.nc2.Variable lon = ncfile.findVariable("lon");
            assert null != lon;
            Array lon_data = lon.read();
            assert (lon_data instanceof ucar.ma2.ArrayFloat);
            IndexIterator lon_dataI = lon_data.getIndexIterator();
            ArrayList<Float> longitudes = new ArrayList();
            while (lon_dataI.hasNext()) {
                longitudes.add(Grid.correctLongitude(lon_dataI.getFloatNext()));
            }

            ucar.nc2.Variable lat = ncfile.findVariable("lat");
            assert null != lat;
            Array lat_data = lat.read();
            assert (lat_data instanceof ucar.ma2.ArrayFloat);
            IndexIterator lat_dataI = lat_data.getIndexIterator();
            ArrayList<Float> latitudes = new ArrayList();
            while (lat_dataI.hasNext()) {
                latitudes.add(lat_dataI.getFloatNext());
            }

            // Create Grid
            if (grid_centre_points.isEmpty()) {
                GridPoint[][] gridPoints = new GridPoint[latitudes.size()][longitudes.size()];
                for (int i = 0; i < latitudes.size(); i++) {
                    for (int j = 0; j < longitudes.size(); j++) {
                        gridPoints[i][j] = new GridPoint(i + 1, j + 1, latitudes.get(i), longitudes.get(j));
                    }
                }
                Grid grid = new Grid(gridPoints);
                grid_centre_points.addAll(grid.selectNearestFourGridPoints(latitude, longitude));
                System.out.println();
            }
            ncfile.close();
        } catch (URISyntaxException | IOException ex) {
            throw new UnsupportedOperationException(ex);
        }
    }

    /**
     * Returns the timeframe 2050.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe_2050() {
        return timeframe_2050;
    }

    /**
     * Returns the timeframe 2080.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe_2080() {
        return timeframe_2080;
    }

}
