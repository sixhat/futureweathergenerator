/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.EC_Earth3;

import futureweathergenerator.GCM.GridPoint;
import futureweathergenerator.GCM.Variable;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Creates a timeframe with the four SSP EC-Earth3 scenarios.
 *
 * @author eugenio
 */
public class Timeframe {

    private final Scenario ssp126;
    private final Scenario ssp245;
    private final Scenario ssp370;
    private final Scenario ssp585;

    /**
     * Initiates the timeframe.
     *
     * @param NUMBER_OF_CPU_THREADS number of threads to read data
     * @param path_EC_Earth3 path to EC-Earth3 resources
     * @param timeframe timeframe name
     * @param latitude latitude of the EPW
     * @param longitude longitude of the EPW
     * @param grid_centre_points list of grid points
     */
    public Timeframe(int NUMBER_OF_CPU_THREADS, String path_EC_Earth3, String timeframe, float latitude, float longitude, ArrayList<GridPoint> grid_centre_points) {
        this.ssp126 = getTimeframeVariables(NUMBER_OF_CPU_THREADS, path_EC_Earth3, latitude, longitude, grid_centre_points, timeframe, "ssp126");
        this.ssp245 = getTimeframeVariables(NUMBER_OF_CPU_THREADS, path_EC_Earth3, latitude, longitude, grid_centre_points, timeframe, "ssp245");
        this.ssp370 = getTimeframeVariables(NUMBER_OF_CPU_THREADS, path_EC_Earth3, latitude, longitude, grid_centre_points, timeframe, "ssp370");
        this.ssp585 = getTimeframeVariables(NUMBER_OF_CPU_THREADS, path_EC_Earth3, latitude, longitude, grid_centre_points, timeframe, "ssp585");
    }

    private static futureweathergenerator.GCM.EC_Earth3.Scenario getTimeframeVariables(
            int NUMBER_OF_CPU_THREADS,
            String path_EC_Earth3,
            float latitude,
            float longitude,
            ArrayList<GridPoint> grid_centre_points,
            String timeframe,
            String scenario) {
        ArrayList<futureweathergenerator.GCM.EC_Earth3.Settings> variableSettings = new ArrayList<>();
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "tas", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "tasmax", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "tasmin", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "rsds", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "clt", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "pr", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "hurs", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "psl", timeframe));
        variableSettings.add(new futureweathergenerator.GCM.EC_Earth3.Settings(path_EC_Earth3, scenario, "sfcWind", timeframe));

        Variable[][] variables = threadsReadFiles(NUMBER_OF_CPU_THREADS, variableSettings);
        return new futureweathergenerator.GCM.EC_Earth3.Scenario(
                timeframe,
                scenario,
                latitude,
                longitude,
                grid_centre_points,
                variables[0],
                variables[1],
                variables[2],
                variables[3],
                variables[4],
                variables[5],
                variables[6],
                variables[7],
                variables[8]
        );
    }

    private static Variable[][] threadsReadFiles(int NUMBER_OF_CPU_THREADS, ArrayList<futureweathergenerator.GCM.EC_Earth3.Settings> variableSettings) {
        Variable[][] variables = new Variable[9][];
        ExecutorService pool = Executors.newFixedThreadPool(NUMBER_OF_CPU_THREADS);
        CountDownLatch dS = new CountDownLatch(variableSettings.size());
        for (int i = 0; i < variableSettings.size(); i++) {
            pool.execute(new futureweathergenerator.GCM.EC_Earth3.ReadFileThread(i, variables, variableSettings, dS));
        }
        try {
            dS.await();
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        pool.shutdown();
        return variables;
    }

    /**
     * Returns the SSP1-2.6 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp126() {
        return ssp126;
    }

    /**
     * Returns the SSP2-4.5 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp245() {
        return ssp245;
    }

    /**
     * Returns the SSP3-7.0 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp370() {
        return ssp370;
    }

    /**
     * Returns the SSP5-8.5 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp585() {
        return ssp585;
    }

}
