/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.EC_Earth3;

/**
 * Initiates the variable settings.
 *
 * @author eugenio
 */
public class Settings {
    
    private final String path;
    private final String scenario;
    private final String variable;
    private final String timeframe;

    /**
     * Initiates the variable settings.
     *
     * @param path path to the variable resource
     * @param scenario scenario name
     * @param variable variable name
     * @param timeframe timeframe name
     */
    public Settings(String path, String scenario, String variable, String timeframe) {
        this.path = path;
        this.scenario = scenario;
        this.variable = variable;
        this.timeframe = timeframe;
    }
    
    /**
     * Returns path to variable resource.
     *
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * Returns scenario name.
     *
     * @return scenario name
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Returns variable name.
     *
     * @return variable name
     */
    public String getVariable() {
        return variable;
    }

    /**
     * Returns timeframe name.
     *
     * @return timeframe name
     */
    public String getTimeframe() {
        return timeframe;
    }
    
}
