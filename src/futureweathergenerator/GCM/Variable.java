/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.Months;

/**
 * Sets a variable data.
 *
 * @author eugenio
 */
public class Variable {

    private int number_of_x_points;
    private int number_of_y_points;
    private Months.Abbreviation month;
    private String scenario;
    private String timeframe;
    private float[][] grid_values;

    /**
     * Initiates a variable object.
     *
     * @param number_of_x_points number of points in x direction
     * @param number_of_y_points number of points in y direction
     * @param month month abbreviation
     * @param scenario scenario name
     * @param timeframe timeframe name
     * @param grid_values values in the grid
     */
    public Variable(int number_of_x_points, int number_of_y_points, Months.Abbreviation month, String scenario, String timeframe, float[][] grid_values) {
        this.number_of_x_points = number_of_x_points;
        this.number_of_y_points = number_of_y_points;
        this.month = month;
        this.scenario = scenario;
        this.timeframe = timeframe;
        this.grid_values = grid_values;
    }

    /**
     * Returns the number of points in the x direction.
     *
     * @return number of points
     */
    public int getNumber_of_x_points() {
        return number_of_x_points;
    }

    /**
     * Returns the number of points in the y direction.
     *
     * @return number of points
     */
    public int getNumber_of_y_points() {
        return number_of_y_points;
    }

    /**
     * Returns the abbreviation of the month.
     *
     * @return month abbreviation
     */
    public Months.Abbreviation getMonth() {
        return month;
    }

    /**
     * Returns scenario name.
     *
     * @return string
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Returns timeframe name.
     *
     * @return string
     */
    public String getTimeframe() {
        return timeframe;
    }

    /**
     * Returns variable values in the world grid.
     *
     * @return grid values
     */
    public float[][] getGrid_values() {
        return grid_values;
    }

    /**
     * Sets number of points in x direction.
     *
     * @param number_of_x_points number of points
     */
    public void setNumber_of_x_points(int number_of_x_points) {
        this.number_of_x_points = number_of_x_points;
    }

    /**
     * Sets number of points in y direction.
     *
     * @param number_of_y_points number of points
     */
    public void setNumber_of_y_points(int number_of_y_points) {
        this.number_of_y_points = number_of_y_points;
    }

    /**
     * Sets month abbreviation.
     *
     * @param month month abbreviation
     */
    public void setMonth(Months.Abbreviation month) {
        this.month = month;
    }

    /**
     * Sets the scenario name.
     *
     * @param scenario string
     */
    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    /**
     * Sets the timeframe name.
     *
     * @param timeframe string
     */
    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }

    /**
     * Sets the variable values in the world grid.
     *
     * @param grid_values grid values
     */
    public void setGrid_values(float[][] grid_values) {
        this.grid_values = grid_values;
    }

}
