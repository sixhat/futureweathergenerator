/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.HadCM3;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.GCM.Morphing;
import futureweathergenerator.GCModel;
import java.util.concurrent.CountDownLatch;

/**
 * Sets a timeframe thread.
 * 
 * @author eugenio
 */
public class TimeframeThread implements Runnable {

    private final String PATH_OUTPUT_FOLDER;
    private final EPW epw;
    private final String timeframe_trigger;
    private final HadCM3 hadCM3;
    private final float numberOfHoursToSmooth;
    private final CountDownLatch dS;

    /**
     * Initializes the timeframe thread.
     *
     * @param PATH_OUTPUT_FOLDER path to the output folder
     * @param epw original EPW
     * @param timeframe_trigger timeframe name
     * @param hadCM3 HadCM3
     * @param numberOfHoursToSmooth number of hours to smooth month transition
     * @param dS thread count down latch
     */
    public TimeframeThread(String PATH_OUTPUT_FOLDER, EPW epw, String timeframe_trigger, HadCM3 hadCM3, float numberOfHoursToSmooth, CountDownLatch dS) {
        this.PATH_OUTPUT_FOLDER = PATH_OUTPUT_FOLDER;
        this.epw = epw;
        this.timeframe_trigger = timeframe_trigger;
        this.hadCM3 = hadCM3;
        this.numberOfHoursToSmooth = numberOfHoursToSmooth;
        this.dS = dS;
    }

    /**
     * Runs this thread.
     */
    @Override
    public void run() {
        StringBuffer printOutputs = new StringBuffer();
        printOutputs.append("\nGenerating ").append(timeframe_trigger).append(" timeframe...").append("\n");

        EPW morphedEPW = new EPW(epw.getPath_EPW(), false);

        // Selects the chosen timeframe
        Timeframe timeframe;
        switch (timeframe_trigger) {
            case "2020" ->
                timeframe = hadCM3.getTimeframe_2020();
            case "2050" ->
                timeframe = hadCM3.getTimeframe_2050();
            case "2080" ->
                timeframe = hadCM3.getTimeframe_2080();
            default ->
                throw new UnsupportedOperationException("Error: Unknown Timeframe: " + timeframe_trigger);
        }

        String path = PATH_OUTPUT_FOLDER
                + epw.getEpw_location().getA3_country().replace("/", "-").replace("\\", "-")
                + "_"
                + epw.getEpw_location().getA1_city().replace("/", "-").replace("\\", "-")
                + "_";

        // Averages A2a, A2b, and A2c into a single scenario
        printOutputs.append("\tAveraging scenarios A2a, A2b, A2c into a single scenario A2...");
        AverageScenario scenario = new AverageScenario("A2", timeframe.getA2a(), timeframe.getA2b(), timeframe.getA2c());

        // Saves HadCM3 interpolated variables
        scenario.saveTable_GCM_variables(printOutputs,
                GCModel.HadCM3,
                path
                + FutureWeatherGenerator.OUTPUT_FILE_VARIABLES_HADCM3
                + "_A2_"
                + timeframe_trigger
                + ".csv");

        // Generates future weather
        Morphing.createMorphedEPWFile(printOutputs, epw, scenario, morphedEPW, numberOfHoursToSmooth);

        // Saves comparison table between original and morphed file
        morphedEPW.saveEPWComparison(printOutputs,
                path
                + FutureWeatherGenerator.OUTPUT_FILE_COMPARISON_HADCM3
                + "_A2_"
                + timeframe_trigger
                + ".csv", epw);

        // Saves morphed weather to file
        morphedEPW.saveEPW(printOutputs,
                path
                + GCModel.HadCM3.toString()
                + "_A2_"
                + timeframe_trigger
                + ".epw");

        System.out.println(printOutputs.toString());
        this.dS.countDown();
    }
}
