/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.HadCM3;

import futureweathergenerator.GCM.GridPoint;
import futureweathergenerator.GCM.Grid;
import futureweathergenerator.EPW.EPW;
import futureweathergenerator.FutureWeatherGenerator;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Sets the HadCM3 object.
 *
 * @author eugenio
 */
public class HadCM3 {

    /**
     * Generates all morphed future EPW files.
     *
     * @param NUMBER_OF_CPU_THREADS number of threads to use
     * @param PATH_OUTPUT_FOLDER path to the output folder
     * @param epw original EPW file
     * @param numberOfHoursToSmooth number of hours to smooth month transition
     */
    public static void generate(int NUMBER_OF_CPU_THREADS, String PATH_OUTPUT_FOLDER, EPW epw, float numberOfHoursToSmooth) {
        System.out.println("\nInitializing HadCM3...");
        HadCM3 hadCM3 = new HadCM3(
                NUMBER_OF_CPU_THREADS,
                epw.getEpw_location().getN2_latitude(),
                epw.getEpw_location().getN3_longitude(),
                FutureWeatherGenerator.PATH_TO_RESOURCES,
                FutureWeatherGenerator.HADCM3_PATH_TO_GRID_96_73,
                FutureWeatherGenerator.HADCM3_PATH_TO_GRID_96_72
        );
        String[] timeframe_triggers = new String[]{"2020", "2050", "2080"};
        ExecutorService pool = Executors.newFixedThreadPool(NUMBER_OF_CPU_THREADS);
        CountDownLatch dS = new CountDownLatch(timeframe_triggers.length);
        for (String timeframe_trigger : timeframe_triggers) {
            pool.execute(new TimeframeThread(PATH_OUTPUT_FOLDER, epw, timeframe_trigger, hadCM3, numberOfHoursToSmooth, dS));
        }
        try {
            dS.await();
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        pool.shutdown();
    }

    private final Timeframe timeframe_2020;
    private final Timeframe timeframe_2050;
    private final Timeframe timeframe_2080;

    private final Grid grid_96_73_centre;
    private final Grid grid_96_72_centre;

    /**
     * Initiates the HadCM3 object.
     *
     * @param NUMBER_OF_CPU_THREADS number of CPU threads
     * @param latitude latitude of the location
     * @param longitude longitude of the location
     * @param path_HadCM3 path to the HadCM3 variables
     * @param path_HadCM3_grid_96_73_centre path to the world 96 x 73 grid
     * points
     * @param path_HadCM3_grid_96_72_centre path to the world 96 x 72 grid
     * points
     */
    public HadCM3(
            int NUMBER_OF_CPU_THREADS,
            float latitude,
            float longitude,
            String path_HadCM3,
            String path_HadCM3_grid_96_73_centre,
            String path_HadCM3_grid_96_72_centre) {
        System.out.println("\nLoading grids...");
        System.out.println("\tGrid 96 x 73 Centre");
        this.grid_96_73_centre = new Grid(path_HadCM3_grid_96_73_centre, 96, 73);
        System.out.println("\tGrid 96 x 72 Centre");
        this.grid_96_72_centre = new Grid(path_HadCM3_grid_96_72_centre, 96, 72);

        ArrayList<GridPoint> grid_96_73_centre_points = this.grid_96_73_centre.selectNearestFourGridPoints(latitude, longitude);
        // NOTICE:
        // CCWorldWeatherGen imports HadCM3 data using two incorrect points of the four nearest grip points.
        // Here, we correctly use the four nearest points.
        ArrayList<GridPoint> grid_96_72_centre_points = this.grid_96_72_centre.selectNearestFourGridPoints(latitude, longitude);

        // 1961-1990 Mean monthly values (reference metereological values)
        // 2010-2039 Mean monthly change fields -> 2020
        // 2040-2069 Mean monthly change fields -> 2050
        // 2070-2099 Mean monthly change fields -> 2080
        System.out.println("\nLoading HadCM3 timeframe 2020");
        this.timeframe_2020 = new Timeframe(NUMBER_OF_CPU_THREADS, path_HadCM3, "2020", latitude, longitude, grid_96_73_centre_points, grid_96_72_centre_points);
        System.out.println("\nLoading HadCM3 timeframe 2050");
        this.timeframe_2050 = new Timeframe(NUMBER_OF_CPU_THREADS, path_HadCM3, "2050", latitude, longitude, grid_96_73_centre_points, grid_96_72_centre_points);
        System.out.println("\nLoading HadCM3 timeframe 2080");
        this.timeframe_2080 = new Timeframe(NUMBER_OF_CPU_THREADS, path_HadCM3, "2080", latitude, longitude, grid_96_73_centre_points, grid_96_72_centre_points);
    }

    /**
     * Returns the timeframe 2020.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe_2020() {
        return timeframe_2020;
    }

    /**
     * Returns the timeframe 2050.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe_2050() {
        return timeframe_2050;
    }

    /**
     * Returns the timeframe 2080.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe_2080() {
        return timeframe_2080;
    }
}
