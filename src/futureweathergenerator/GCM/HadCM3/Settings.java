/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.HadCM3;

/**
 * Initiates the variable settings.
 *
 * @author eugenio
 */
public class Settings {

    private final String path;
    private final String scenario;
    private final String variable;
    private final String timeframe;
    private final boolean isMea;

    /**
     * Initiates the variable settings.
     *
     * @param path path to the variable resource
     * @param scenario scenario name
     * @param variable variable name
     * @param timeframe timeframe name
     * @param isMea is current time?
     */
    public Settings(String path, String scenario, String variable, String timeframe, boolean isMea) {
        this.path = path;
        this.scenario = scenario;
        this.variable = variable;
        this.timeframe = timeframe;
        this.isMea = isMea;
    }

    /**
     * Returns path to variable resource.
     *
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * Returns scenario name.
     *
     * @return scenario name
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Returns variable name.
     *
     * @return variable name
     */
    public String getVariable() {
        return variable;
    }

    /**
     * Returns timeframe name.
     *
     * @return timeframe name
     */
    public String getTimeframe() {
        return timeframe;
    }

    /**
     * Returns if the variable is a current time mean.
     *
     * @return boolean
     */
    public boolean isIsMea() {
        return isMea;
    }

}
