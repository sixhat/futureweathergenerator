/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.HadCM3;

import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.GCM.Variable;
import futureweathergenerator.Months;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread for reading file.
 *
 * @author eugenio
 */
public class ReadFileThread implements Runnable {

    private final int i;
    private final Variable[][] variables;
    private final ArrayList<Settings> variableSettings;
    private final CountDownLatch dS;

    /**
     * Initiates a thread to read a file.
     *
     * @param i month
     * @param variables variables array
     * @param variableSettings list of variable settings
     * @param dS thread count down latch
     */
    public ReadFileThread(int i, Variable[][] variables, ArrayList<Settings> variableSettings, CountDownLatch dS) {
        this.i = i;
        this.variables = variables;
        this.variableSettings = variableSettings;
        this.dS = dS;
    }

    /**
     * Runs this thread.
     */
    @Override
    public void run() {
        variables[i] = getVariable(
                variableSettings.get(i).getPath(),
                variableSettings.get(i).getScenario(),
                variableSettings.get(i).getVariable(),
                variableSettings.get(i).getTimeframe(),
                variableSettings.get(i).isIsMea()
        );
        this.dS.countDown();
    }

    private static Variable[] getVariable(String path_HadCM3, String scenario, String variable_abbreviation, String timeframe, boolean isMea) {
        String extension = "dif";
        int numberOfLinesInHeader = 6;
        int number_of_lines_by_month = 707;
        if (variable_abbreviation.equals("WIND")) {
            number_of_lines_by_month = 698;
        }
        if (isMea) {
            extension = "mea";
            numberOfLinesInHeader = 5;
            number_of_lines_by_month = 706;
            if (variable_abbreviation.equals("WIND")) {
                number_of_lines_by_month = 697;
            }
        }

        String filename = "HADCM3_" + scenario + "_" + variable_abbreviation + "_" + timeframe + "." + extension;
        ArrayList<String> lines = new ArrayList<>();

        InputStream fileStream = FutureWeatherGenerator.class.getResourceAsStream(path_HadCM3 + filename);
        if (fileStream == null) {
            return null;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Timeframe.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("\tVariable File: " + filename);
        Variable[] variables = new Variable[12];
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            Months.Abbreviation month = Months.Abbreviation.values()[i];

            int firstHeaderLine = number_of_lines_by_month * i;
            String ng = lines.get(firstHeaderLine + 1).replace("Grid is  ", "").replace("   Month is " + month, "");
            String[] grid = ng.split(" \\*  ");
            int grid_x = Integer.parseInt(grid[0]);
            int grid_y = Integer.parseInt(grid[1]);
            String value_description = isMea ? lines.get(firstHeaderLine + 4) : lines.get(firstHeaderLine + 5);
            String[] strings_missing_code = value_description.toLowerCase().split("missing code is");
            if (strings_missing_code.length == 1) {
                strings_missing_code = value_description.toLowerCase().split("missing value is");
            }
            String missing_code = strings_missing_code[1].replace(" ", "");

            ArrayList<Float> values = new ArrayList<>();
            int cnew = firstHeaderLine + numberOfLinesInHeader;
            int endcnew = cnew + number_of_lines_by_month - numberOfLinesInHeader;
            int nchar = lines.get(cnew).length() / 10;
            for (int c = cnew; c < endcnew; c++) {
                String l = lines.get(c);

                if (l.length() > 0) {
                    values.add(checkAndAddVariable(l, 0, nchar, missing_code));
                    values.add(checkAndAddVariable(l, nchar, 2 * nchar, missing_code));

                    if (l.length() > nchar * 2) {
                        values.add(checkAndAddVariable(l, 2 * nchar, 3 * nchar, missing_code));
                        values.add(checkAndAddVariable(l, 3 * nchar, 4 * nchar, missing_code));
                        values.add(checkAndAddVariable(l, 4 * nchar, 5 * nchar, missing_code));
                        values.add(checkAndAddVariable(l, 5 * nchar, 6 * nchar, missing_code));
                        values.add(checkAndAddVariable(l, 6 * nchar, 7 * nchar, missing_code));
                        values.add(checkAndAddVariable(l, 7 * nchar, 8 * nchar, missing_code));

                        if (l.length() > nchar * 8) {
                            values.add(checkAndAddVariable(l, 8 * nchar, 9 * nchar, missing_code));
                            values.add(checkAndAddVariable(l, 9 * nchar, 10 * nchar, missing_code));
                        }
                    }
                }
            }

            if (values.size() != grid_x * grid_y) {
                System.out.println("Error in number of values: " + values.size() + "\n\tFile: " + filename);
                throw new UnsupportedOperationException();
            }

            float[][] grid_values = new float[grid_x][grid_y];
            int count_grid = 0;
            for (int ii = 0; ii < grid_values[0].length; ii++) {
                for (int jj = 0; jj < grid_values.length; jj++) {
                    grid_values[jj][ii] = values.get(count_grid);
                    count_grid++;
                }
            }

            variables[i] = new Variable(
                    grid_x,
                    grid_y,
                    month,
                    scenario,
                    timeframe,
                    grid_values);
        }

        return variables;
    }

    private static Float checkAndAddVariable(String line, int nchar_1, int nchar_2, String missing_code) {
        String v0 = line.substring(nchar_1, nchar_2).replace(" ", "");
        if (v0.equals(missing_code)) {
            return Float.NaN;
        } else {
            return Float.parseFloat(v0);
        }
    }
}
