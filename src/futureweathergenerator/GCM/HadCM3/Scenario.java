/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.HadCM3;

import futureweathergenerator.GCM.Grid;
import futureweathergenerator.GCM.GridPoint;
import futureweathergenerator.GCM.Variable;
import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.GCModel;
import java.util.ArrayList;

/**
 * Sets a scenario based on the AverageScenario object.
 *
 * @author eugenio
 */
public class Scenario extends AverageScenario {

    private final Variable[] total_precipitation_1980; // PREC
    private final Variable[] wind_speed_1980; // WIND

    private final Variable[] mean_daily_temperature; // TEMP
    private final Variable[] maximum_daily_temperature; // TMAX A2a A2C
    private final Variable[] minimum_daily_temperature; // TMIN A2a A2C
    private final Variable[] total_downward_surface_shortwave_flux; // DSWF
    private final Variable[] total_cloud_in_longwave_radiation; // TCLW A2b A2c
    private final Variable[] total_precipitation; // PREC
    private final Variable[] relative_humidity; // RHUM
    private final Variable[] mean_sea_level_pressure; // MSLP A2a
    private final Variable[] wind_speed; // WIND

    /**
     * Initiates the Scenario object.
     *
     * @param timeframe timeframe name
     * @param scenario scenario name
     * @param latitude latitude of the location
     * @param longitude longitude of the location
     * @param grid_96_73_centre_points list of the 96 x 73 grid points
     * @param grid_96_72_centre_points list of the 96 x 72 grid points
     * @param total_precipitation_1980 monthly precipitation in 1980
     * @param wind_speed_1980 monthly wind speed in 1980
     * @param mean_daily_temperature monthly future mean daily temperature
     * change
     * @param maximum_daily_temperature monthly future maximum daily temperature
     * change
     * @param minimum_daily_temperature monthly future minimum daily temperature
     * change
     * @param total_downward_surface_shortwave_flux monthly future total
     * downward surface shortwave flux change
     * @param total_cloud_in_longwave_radiation monthly future total cloud in
     * longwave radiation change
     * @param total_precipitation monthly future total precipitation change
     * @param relative_humidity monthly future relative humidity change
     * @param mean_sea_level_pressure monthly future sea level pressure change
     * @param wind_speed monthly future wind speed change
     */
    public Scenario(String timeframe, String scenario, float latitude, float longitude, ArrayList<GridPoint> grid_96_73_centre_points, ArrayList<GridPoint> grid_96_72_centre_points, Variable[] total_precipitation_1980, Variable[] wind_speed_1980, Variable[] mean_daily_temperature, Variable[] maximum_daily_temperature, Variable[] minimum_daily_temperature, Variable[] total_downward_surface_shortwave_flux, Variable[] total_cloud_in_longwave_radiation, Variable[] total_precipitation, Variable[] relative_humidity, Variable[] mean_sea_level_pressure, Variable[] wind_speed) {
        super(GCModel.HadCM3, timeframe, scenario);
        this.total_precipitation_1980 = total_precipitation_1980;
        this.wind_speed_1980 = wind_speed_1980;
        this.mean_daily_temperature = mean_daily_temperature;
        this.maximum_daily_temperature = maximum_daily_temperature;
        this.minimum_daily_temperature = minimum_daily_temperature;
        this.total_downward_surface_shortwave_flux = total_downward_surface_shortwave_flux;
        this.total_cloud_in_longwave_radiation = total_cloud_in_longwave_radiation;
        this.total_precipitation = total_precipitation;
        this.relative_humidity = relative_humidity;
        this.mean_sea_level_pressure = mean_sea_level_pressure;
        this.wind_speed = wind_speed;

        System.out.println();
        float latitude_96_73 = (grid_96_73_centre_points.get(0).getLatitude() + grid_96_73_centre_points.get(1).getLatitude() + grid_96_73_centre_points.get(2).getLatitude() + grid_96_73_centre_points.get(3).getLatitude()) / 4.0f;
        float longitude_96_73 = (grid_96_73_centre_points.get(0).getLongitude() + grid_96_73_centre_points.get(1).getLongitude() + grid_96_73_centre_points.get(2).getLongitude() + grid_96_73_centre_points.get(3).getLongitude()) / 4.0f;
        GridPoint center_96_73 = new GridPoint(null, null, latitude_96_73, longitude_96_73);
        GridPoint nearest_Point_96_73 = Grid.getNearestPoint("Grid 96 x 73", latitude, longitude, grid_96_73_centre_points, center_96_73);

        float latitude_96_72 = (grid_96_72_centre_points.get(0).getLatitude() + grid_96_72_centre_points.get(1).getLatitude() + grid_96_72_centre_points.get(2).getLatitude() + grid_96_72_centre_points.get(3).getLatitude()) / 4.0f;
        float longitude_96_72 = (grid_96_72_centre_points.get(0).getLongitude() + grid_96_72_centre_points.get(1).getLongitude() + grid_96_72_centre_points.get(2).getLongitude() + grid_96_72_centre_points.get(3).getLongitude()) / 4.0f;
        GridPoint center_96_72 = new GridPoint(null, null, latitude_96_72, longitude_96_72);
        GridPoint nearest_Point_96_72 = Grid.getNearestPoint("Grid 96 x 72", latitude, longitude, grid_96_72_centre_points, center_96_72);
        System.out.println();
        
        // Sets the variables values by or (a) averaging the four nearest points
        // when the location is nearer to the center of those four points
        // or (b) the nearest point of the four if it is nearer than the center.
        if (this.mean_daily_temperature == null) {
            super.interpolated_mean_daily_temperature = null;
        } else {
            super.interpolated_mean_daily_temperature = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = this.mean_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1];
                    float b = this.mean_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1];
                    float c = this.mean_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1];
                    float d = this.mean_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1];
                    super.interpolated_mean_daily_temperature[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_mean_daily_temperature[i] = this.mean_daily_temperature[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        if (this.maximum_daily_temperature == null) {
            super.interpolated_maximum_daily_temperature = null;
        } else {
            super.interpolated_maximum_daily_temperature = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = this.maximum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1];
                    float b = this.maximum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1];
                    float c = this.maximum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1];
                    float d = this.maximum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1];
                    super.interpolated_maximum_daily_temperature[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_maximum_daily_temperature[i] = this.maximum_daily_temperature[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        if (this.minimum_daily_temperature == null) {
            super.interpolated_minimum_daily_temperature = null;
        } else {
            super.interpolated_minimum_daily_temperature = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = this.minimum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1];
                    float b = this.minimum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1];
                    float c = this.minimum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1];
                    float d = this.minimum_daily_temperature[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1];
                    super.interpolated_minimum_daily_temperature[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_minimum_daily_temperature[i] = this.minimum_daily_temperature[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        if (this.total_downward_surface_shortwave_flux == null) {
            super.interpolated_total_downward_surface_shortwave_flux = null;
        } else {
            super.interpolated_total_downward_surface_shortwave_flux = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = this.total_downward_surface_shortwave_flux[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1];
                    float b = this.total_downward_surface_shortwave_flux[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1];
                    float c = this.total_downward_surface_shortwave_flux[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1];
                    float d = this.total_downward_surface_shortwave_flux[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1];
                    super.interpolated_total_downward_surface_shortwave_flux[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_total_downward_surface_shortwave_flux[i] = this.total_downward_surface_shortwave_flux[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        if (this.total_cloud_in_longwave_radiation == null) {
            super.interpolated_total_cloud_in_longwave_radiation = null;
        } else {
            super.interpolated_total_cloud_in_longwave_radiation = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = this.total_cloud_in_longwave_radiation[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1] * 100.0f;
                    float b = this.total_cloud_in_longwave_radiation[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1] * 100.0f;
                    float c = this.total_cloud_in_longwave_radiation[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1] * 100.0f;
                    float d = this.total_cloud_in_longwave_radiation[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1] * 100.0f;
                    super.interpolated_total_cloud_in_longwave_radiation[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_total_cloud_in_longwave_radiation[i] = this.total_cloud_in_longwave_radiation[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        if (this.total_precipitation == null) {
            super.interpolated_total_precipitation = null;
        } else {
            super.interpolated_total_precipitation = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = 1 + this.total_precipitation[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1] / this.total_precipitation_1980[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1];
                    float b = 1 + this.total_precipitation[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1] / this.total_precipitation_1980[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1];
                    float c = 1 + this.total_precipitation[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1] / this.total_precipitation_1980[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1];
                    float d = 1 + this.total_precipitation[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1] / this.total_precipitation_1980[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1];
                    super.interpolated_total_precipitation[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_total_precipitation[i] = 1 + this.total_precipitation[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1] / this.total_precipitation_1980[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        if (this.relative_humidity == null) {
            super.interpolated_relative_humidity = null;
        } else {
            super.interpolated_relative_humidity = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = this.relative_humidity[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1];
                    float b = this.relative_humidity[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1];
                    float c = this.relative_humidity[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1];
                    float d = this.relative_humidity[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1];
                    super.interpolated_relative_humidity[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_relative_humidity[i] = this.relative_humidity[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        if (this.mean_sea_level_pressure == null) {
            super.interpolated_mean_sea_level_pressure = null;
        } else {
            super.interpolated_mean_sea_level_pressure = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_73.getX() == null) {
                    float a = this.mean_sea_level_pressure[i].getGrid_values()[grid_96_73_centre_points.get(0).getX() - 1][grid_96_73_centre_points.get(0).getY() - 1];
                    float b = this.mean_sea_level_pressure[i].getGrid_values()[grid_96_73_centre_points.get(1).getX() - 1][grid_96_73_centre_points.get(1).getY() - 1];
                    float c = this.mean_sea_level_pressure[i].getGrid_values()[grid_96_73_centre_points.get(2).getX() - 1][grid_96_73_centre_points.get(2).getY() - 1];
                    float d = this.mean_sea_level_pressure[i].getGrid_values()[grid_96_73_centre_points.get(3).getX() - 1][grid_96_73_centre_points.get(3).getY() - 1];
                    super.interpolated_mean_sea_level_pressure[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_mean_sea_level_pressure[i] = this.mean_sea_level_pressure[i].getGrid_values()[nearest_Point_96_73.getX() - 1][nearest_Point_96_73.getY() - 1];
                }
            }
        }

        // NOTICE:
        // CCWorldWeatherGen imports HadCM3 data using two incorrect points of the four nearest grip points.
        // Here, we correctly use the four nearest points.
        if (this.wind_speed == null) {
            super.interpolated_wind_speed = null;
        } else {
            super.interpolated_wind_speed = new float[12];
            for (int i = 0; i < 12; i++) {
                if (nearest_Point_96_72.getX() == null) {
                    float a = 1 + this.wind_speed[i].getGrid_values()[grid_96_72_centre_points.get(0).getX() - 1][grid_96_72_centre_points.get(0).getY() - 1] / this.wind_speed_1980[i].getGrid_values()[grid_96_72_centre_points.get(0).getX() - 1][grid_96_72_centre_points.get(0).getY() - 1];
                    float b = 1 + this.wind_speed[i].getGrid_values()[grid_96_72_centre_points.get(1).getX() - 1][grid_96_72_centre_points.get(1).getY() - 1] / this.wind_speed_1980[i].getGrid_values()[grid_96_72_centre_points.get(1).getX() - 1][grid_96_72_centre_points.get(1).getY() - 1];
                    float c = 1 + this.wind_speed[i].getGrid_values()[grid_96_72_centre_points.get(2).getX() - 1][grid_96_72_centre_points.get(2).getY() - 1] / this.wind_speed_1980[i].getGrid_values()[grid_96_72_centre_points.get(2).getX() - 1][grid_96_72_centre_points.get(2).getY() - 1];
                    float d = 1 + this.wind_speed[i].getGrid_values()[grid_96_72_centre_points.get(3).getX() - 1][grid_96_72_centre_points.get(3).getY() - 1] / this.wind_speed_1980[i].getGrid_values()[grid_96_72_centre_points.get(3).getX() - 1][grid_96_72_centre_points.get(3).getY() - 1];
                    super.interpolated_wind_speed[i] = (a + b + c + d) / 4.0f;
                } else {
                    super.interpolated_wind_speed[i] = 1 + this.wind_speed[i].getGrid_values()[nearest_Point_96_72.getX() - 1][nearest_Point_96_72.getY() - 1] / this.wind_speed_1980[i].getGrid_values()[nearest_Point_96_72.getX() - 1][nearest_Point_96_72.getY() - 1];
                }
            }
        }
    }

    /**
     * Returns an array of the monthly total precipitation in 1980.
     *
     * @return array
     */
    public Variable[] getTotal_precipitation_1980() {
        return total_precipitation_1980;
    }

    /**
     * Returns an array of the monthly wind speed in 1980.
     *
     * @return array
     */
    public Variable[] getWind_speed_1980() {
        return wind_speed_1980;
    }

    /**
     * Returns an array of the future monthly total downward surface shortwave
     * flux change.
     *
     * @return array
     */
    public Variable[] getTotal_downward_surface_shortwave_flux() {
        return total_downward_surface_shortwave_flux;
    }

    /**
     * Returns an array of the future monthly total precipitation change.
     *
     * @return array
     */
    public Variable[] getTotal_precipitation() {
        return total_precipitation;
    }

    /**
     * Returns an array of the future relative humidity change.
     *
     * @return array
     */
    public Variable[] getRelative_humidity() {
        return relative_humidity;
    }

    /**
     * Returns an array of the future monthly mean daily temperature change.
     *
     * @return array
     */
    public Variable[] getMean_daily_temperature() {
        return mean_daily_temperature;
    }

    /**
     * Returns an array of the future monthly maxim daily temperature change.
     *
     * @return array
     */
    public Variable[] getMaximum_daily_temperature() {
        return maximum_daily_temperature;
    }

    /**
     * Returns an array of the future monthly minimum daily temperature change.
     *
     * @return array
     */
    public Variable[] getMinimum_daily_temperature() {
        return minimum_daily_temperature;
    }

    /**
     * Returns an array of the future monthly wind speed change.
     *
     * @return array
     */
    public Variable[] getWind_speed() {
        return wind_speed;
    }

    /**
     * Returns an array of the future monthly mean sea level pressure change.
     *
     * @return array
     */
    public Variable[] getMean_sea_level_pressure() {
        return mean_sea_level_pressure;
    }

    /**
     * Returns an array of the future total cloud in longwave radiation change.
     *
     * @return array
     */
    public Variable[] getTotal_cloud_in_longwave_radiation() {
        return total_cloud_in_longwave_radiation;
    }

}
