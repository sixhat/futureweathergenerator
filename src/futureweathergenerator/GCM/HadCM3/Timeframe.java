/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM.HadCM3;

import futureweathergenerator.GCM.GridPoint;
import futureweathergenerator.GCM.Variable;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Sets A2a, A2b, and A2c scenarios for a specified timeframe.
 *
 * @author eugenio
 */
public final class Timeframe {

    private final Scenario a2a;
    private final Scenario a2b;
    private final Scenario a2c;

    /**
     * Sets the timeframe and its scenarios from world grid, latitude, and
     * longitude.
     *
     * @param NUMBER_OF_CPU_THREADS number of CPU threads
     * @param path_HadCM3 path to HadCM3 variables
     * @param timeframe timeframe name
     * @param latitude latitude of the location
     * @param longitude longituder of the location
     * @param grid_96_73_centre_points list of points for the 96 x 73 grid
     * @param grid_96_72_centre_points list of points for the 96 x 72 grid
     */
    public Timeframe(int NUMBER_OF_CPU_THREADS, String path_HadCM3, String timeframe, float latitude, float longitude, ArrayList<GridPoint> grid_96_73_centre_points, ArrayList<GridPoint> grid_96_72_centre_points) {
        this.a2a = getTimeframeVariables(
                NUMBER_OF_CPU_THREADS,
                path_HadCM3,
                latitude,
                longitude,
                grid_96_73_centre_points,
                grid_96_72_centre_points, 
                timeframe, 
                "A2a");
                
        this.a2b = getTimeframeVariables(
                NUMBER_OF_CPU_THREADS,
                path_HadCM3,
                latitude,
                longitude,
                grid_96_73_centre_points,
                grid_96_72_centre_points, 
                timeframe, 
                "A2b");

        this.a2c = getTimeframeVariables(
                NUMBER_OF_CPU_THREADS,
                path_HadCM3,
                latitude,
                longitude,
                grid_96_73_centre_points,
                grid_96_72_centre_points, 
                timeframe, 
                "A2c");
    }
    
    private static Scenario getTimeframeVariables(
            int NUMBER_OF_CPU_THREADS,
            String path_HadCM3, 
            float latitude, 
            float longitude,
            ArrayList<GridPoint> grid_96_73_centre_points,
            ArrayList<GridPoint> grid_96_72_centre_points,
            String timeframe, 
            String scenario) {
        ArrayList<Settings> variableSettings = new ArrayList<>();
        variableSettings.add(new Settings(path_HadCM3, scenario, "PREC", "1980", true));
        variableSettings.add(new Settings(path_HadCM3, scenario, "WIND", "1980", true));
        variableSettings.add(new Settings(path_HadCM3, scenario, "TEMP", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "TMAX", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "TMIN", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "DSWF", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "TCLW", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "PREC", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "RHUM", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "MSLP", timeframe, false));
        variableSettings.add(new Settings(path_HadCM3, scenario, "WIND", timeframe, false));
        
        Variable[][] variables = threadsReadFiles(NUMBER_OF_CPU_THREADS, variableSettings);
        return new Scenario(
                timeframe,
                scenario, 
                latitude, 
                longitude,
                grid_96_73_centre_points,
                grid_96_72_centre_points,
                variables[0],
                variables[1],
                variables[2],
                variables[3],
                variables[4],
                variables[5],
                variables[6],
                variables[7],
                variables[8],
                variables[9],
                variables[10]
        ); 
    }
    
    private static Variable[][] threadsReadFiles(int NUMBER_OF_CPU_THREADS, ArrayList<Settings> variableSettings) {
        Variable[][] variables = new Variable[11][];
        ExecutorService pool = Executors.newFixedThreadPool(NUMBER_OF_CPU_THREADS);
        CountDownLatch dS = new CountDownLatch(variableSettings.size());
        for(int i = 0; i < variableSettings.size(); i++) {
            pool.execute(new ReadFileThread(i, variables, variableSettings, dS));
        }
        try {
            dS.await();
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        pool.shutdown();
        return variables;
    }
    
    /**
     * Returns the A2a scenario.
     *
     * @return scenario
     */
    public Scenario getA2a() {
        return a2a;
    }

    /**
     * Returns the A2b scenario.
     *
     * @return scenario
     */
    public Scenario getA2b() {
        return a2b;
    }

    /**
     * Returns the A2c scenario.
     *
     * @return scenario
     */
    public Scenario getA2c() {
        return a2c;
    }

    
}
