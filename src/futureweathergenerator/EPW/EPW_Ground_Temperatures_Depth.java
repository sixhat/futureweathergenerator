/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

/**
 * EPW ground temperatures object.
 *
 * @author eugenio
 */
public class EPW_Ground_Temperatures_Depth {

    private float N1_depth;
    private String N2_soil_conductivity;
    private String N3_soil_density;
    private String N4_soil_specific_heat;
    private float N5_January_average_ground_temperature;
    private float N6_February_average_ground_temperature;
    private float N7_March_average_ground_temperature;
    private float N8_April_average_ground_temperature;
    private float N9_May_average_ground_temperature;
    private float N10_June_average_ground_temperature;
    private float N11_July_average_ground_temperature;
    private float N12_August_average_ground_temperature;
    private float N13_September_average_ground_temperature;
    private float N14_October_average_ground_temperature;
    private float N15_November_average_ground_temperature;
    private float N16_December_average_ground_temperature;

    /**
     * Initiates the ground temperatures object.
     *
     * @param N1_depth depth of the soil
     * @param N2_soil_conductivity soil conductivity
     * @param N3_soil_density soil density
     * @param N4_soil_specific_heat soil specific heat
     * @param N5_January_average_ground_temperature January average ground
     * temperature
     * @param N6_February_average_ground_temperature February average ground
     * temperature
     * @param N7_March_average_ground_temperature March average ground
     * temperature
     * @param N8_April_average_ground_temperature April average ground
     * temperature
     * @param N9_May_average_ground_temperature May average ground temperature
     * @param N10_June_average_ground_temperature June average ground
     * temperature
     * @param N11_July_average_ground_temperature July average ground
     * temperature
     * @param N12_August_average_ground_temperature August average ground
     * temperature
     * @param N13_September_average_ground_temperature September average ground
     * temperature
     * @param N14_October_average_ground_temperature October average ground
     * temperature
     * @param N15_November_average_ground_temperature November average ground
     * temperature
     * @param N16_December_average_ground_temperature December average ground
     * temperature
     */
    public EPW_Ground_Temperatures_Depth(float N1_depth, String N2_soil_conductivity, String N3_soil_density, String N4_soil_specific_heat, float N5_January_average_ground_temperature, float N6_February_average_ground_temperature, float N7_March_average_ground_temperature, float N8_April_average_ground_temperature, float N9_May_average_ground_temperature, float N10_June_average_ground_temperature, float N11_July_average_ground_temperature, float N12_August_average_ground_temperature, float N13_September_average_ground_temperature, float N14_October_average_ground_temperature, float N15_November_average_ground_temperature, float N16_December_average_ground_temperature) {
        this.N1_depth = N1_depth;
        this.N2_soil_conductivity = N2_soil_conductivity;
        this.N3_soil_density = N3_soil_density;
        this.N4_soil_specific_heat = N4_soil_specific_heat;
        this.N5_January_average_ground_temperature = N5_January_average_ground_temperature;
        this.N6_February_average_ground_temperature = N6_February_average_ground_temperature;
        this.N7_March_average_ground_temperature = N7_March_average_ground_temperature;
        this.N8_April_average_ground_temperature = N8_April_average_ground_temperature;
        this.N9_May_average_ground_temperature = N9_May_average_ground_temperature;
        this.N10_June_average_ground_temperature = N10_June_average_ground_temperature;
        this.N11_July_average_ground_temperature = N11_July_average_ground_temperature;
        this.N12_August_average_ground_temperature = N12_August_average_ground_temperature;
        this.N13_September_average_ground_temperature = N13_September_average_ground_temperature;
        this.N14_October_average_ground_temperature = N14_October_average_ground_temperature;
        this.N15_November_average_ground_temperature = N15_November_average_ground_temperature;
        this.N16_December_average_ground_temperature = N16_December_average_ground_temperature;
    }

    /**
     * Returns ground temperature depth.
     *
     * @return depth of ground temperature
     */
    public float getN1_depth() {
        return N1_depth;
    }

    /**
     * Returns the soil conductivity [W/m·K].
     *
     * @return soil conductivity
     */
    public String getN2_soil_conductivity() {
        return N2_soil_conductivity;
    }

    /**
     * Returns the soil density [kg/m3].
     *
     * @return soil density
     */
    public String getN3_soil_density() {
        return N3_soil_density;
    }

    /**
     * Returns soil specific heat [J/kg·K].
     *
     * @return soil specific heat
     */
    public String getN4_soil_specific_heat() {
        return N4_soil_specific_heat;
    }

    /**
     * January average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN5_January_average_ground_temperature() {
        return N5_January_average_ground_temperature;
    }

    /**
     * February average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN6_February_average_ground_temperature() {
        return N6_February_average_ground_temperature;
    }

    /**
     * March average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN7_March_average_ground_temperature() {
        return N7_March_average_ground_temperature;
    }

    /**
     * April average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN8_April_average_ground_temperature() {
        return N8_April_average_ground_temperature;
    }

    /**
     * May average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN9_May_average_ground_temperature() {
        return N9_May_average_ground_temperature;
    }

    /**
     * June average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN10_June_average_ground_temperature() {
        return N10_June_average_ground_temperature;
    }

    /**
     * July average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN11_July_average_ground_temperature() {
        return N11_July_average_ground_temperature;
    }

    /**
     * August average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN12_August_average_ground_temperature() {
        return N12_August_average_ground_temperature;
    }

    /**
     * September average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN13_September_average_ground_temperature() {
        return N13_September_average_ground_temperature;
    }

    /**
     * October average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN14_October_average_ground_temperature() {
        return N14_October_average_ground_temperature;
    }

    /**
     * November average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN15_November_average_ground_temperature() {
        return N15_November_average_ground_temperature;
    }

    /**
     * December average ground temperature [°C].
     *
     * @return month average ground temperature
     */
    public float getN16_December_average_ground_temperature() {
        return N16_December_average_ground_temperature;
    }

    /**
     * Depth [m] of the ground Temperatures.
     *
     * @param N1_depth depth
     */
    public void setN1_depth(float N1_depth) {
        this.N1_depth = N1_depth;
    }

    /**
     * Soil Conductivity [W/m·k].
     *
     * @param N2_soil_conductivity Soil Conductivity
     */
    public void setN2_soil_conductivity(String N2_soil_conductivity) {
        this.N2_soil_conductivity = N2_soil_conductivity;
    }

    /**
     * Soil Density [kg/m3].
     *
     * @param N3_soil_density Soil Density
     */
    public void setN3_soil_density(String N3_soil_density) {
        this.N3_soil_density = N3_soil_density;
    }

    /**
     * Soil Specific Heat [J/kg·K].
     *
     * @param N4_soil_specific_heat Soil Specific Heat
     */
    public void setN4_soil_specific_heat(String N4_soil_specific_heat) {
        this.N4_soil_specific_heat = N4_soil_specific_heat;
    }

    /**
     * Average soil temperature [°C] in January.
     *
     * @param N5_January_average_ground_temperature temperature
     */
    public void setN5_January_average_ground_temperature(float N5_January_average_ground_temperature) {
        this.N5_January_average_ground_temperature = N5_January_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in February.
     *
     * @param N6_February_average_ground_temperature temperature
     */
    public void setN6_February_average_ground_temperature(float N6_February_average_ground_temperature) {
        this.N6_February_average_ground_temperature = N6_February_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in March.
     *
     * @param N7_March_average_ground_temperature temperature
     */
    public void setN7_March_average_ground_temperature(float N7_March_average_ground_temperature) {
        this.N7_March_average_ground_temperature = N7_March_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in April.
     *
     * @param N8_April_average_ground_temperature temperature
     */
    public void setN8_April_average_ground_temperature(float N8_April_average_ground_temperature) {
        this.N8_April_average_ground_temperature = N8_April_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in May.
     *
     * @param N9_May_average_ground_temperature temperature
     */
    public void setN9_May_average_ground_temperature(float N9_May_average_ground_temperature) {
        this.N9_May_average_ground_temperature = N9_May_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in June.
     *
     * @param N10_June_average_ground_temperature temperature
     */
    public void setN10_June_average_ground_temperature(float N10_June_average_ground_temperature) {
        this.N10_June_average_ground_temperature = N10_June_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in July.
     *
     * @param N11_July_average_ground_temperature temperature
     */
    public void setN11_July_average_ground_temperature(float N11_July_average_ground_temperature) {
        this.N11_July_average_ground_temperature = N11_July_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in August.
     *
     * @param N12_August_average_ground_temperature temperature
     */
    public void setN12_August_average_ground_temperature(float N12_August_average_ground_temperature) {
        this.N12_August_average_ground_temperature = N12_August_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in September.
     *
     * @param N13_September_average_ground_temperature temperature
     */
    public void setN13_September_average_ground_temperature(float N13_September_average_ground_temperature) {
        this.N13_September_average_ground_temperature = N13_September_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in October.
     *
     * @param N14_October_average_ground_temperature temperature
     */
    public void setN14_October_average_ground_temperature(float N14_October_average_ground_temperature) {
        this.N14_October_average_ground_temperature = N14_October_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in November.
     *
     * @param N15_November_average_ground_temperature temperature
     */
    public void setN15_November_average_ground_temperature(float N15_November_average_ground_temperature) {
        this.N15_November_average_ground_temperature = N15_November_average_ground_temperature;
    }

    /**
     * Average soil temperature [°C] in December.
     *
     * @param N16_December_average_ground_temperature temperature
     */
    public void setN16_December_average_ground_temperature(float N16_December_average_ground_temperature) {
        this.N16_December_average_ground_temperature = N16_December_average_ground_temperature;
    }
}
