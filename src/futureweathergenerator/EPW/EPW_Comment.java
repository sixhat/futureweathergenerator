/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

/**
 * EPW comment object.
 *
 * @author eugenio
 */
public class EPW_Comment {

    /**
     * EPW object string id.
     */
    public static final String ID = "COMMENTS";

    private int ID_value;
    private String A1_comment;

    /**
     * Comment object.
     *
     * @param ID_value comment id
     * @param A1_comment comment text
     */
    public EPW_Comment(int ID_value, String A1_comment) {
        this.ID_value = ID_value;
        this.A1_comment = A1_comment;
    }

    /**
     * Returns this comment as a string.
     *
     * @return string
     */
    public String getString() {
        return ID + " " + this.ID_value + "," + this.A1_comment + "\n";
    }

    /**
     * Returns this comment id.
     *
     * @return comment id
     */
    public int getID_value() {
        return ID_value;
    }

    /**
     * Returns this comment text.
     *
     * @return text
     */
    public String getA1_comment() {
        return A1_comment;
    }

    /**
     * Comment number.
     *
     * @param ID_value number
     */
    public void setID_value(int ID_value) {
        this.ID_value = ID_value;
    }

    /**
     * Comment text.
     *
     * @param A1_comment text
     */
    public void setA1_comment(String A1_comment) {
        this.A1_comment = A1_comment;
    }

}
