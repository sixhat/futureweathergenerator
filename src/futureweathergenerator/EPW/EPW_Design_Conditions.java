/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

/**
 * Design conditions object.
 *
 * @author eugenio
 */
public class EPW_Design_Conditions {

    /**
     * EPW object string id.
     */
    public static final String ID = "DESIGN CONDITIONS";

    private int N1_number_of_design_conditions;
    private String[] An_Design_Condition_Sources;

    /**
     * Initiates the design conditions object.
     *
     * @param N1_number_of_design_conditions number of design conditions
     * @param An_Design_Condition_Sources array of design conditions
     */
    public EPW_Design_Conditions(int N1_number_of_design_conditions, String[] An_Design_Condition_Sources) {
        this.N1_number_of_design_conditions = N1_number_of_design_conditions;
        this.An_Design_Condition_Sources = An_Design_Condition_Sources;
    }

    /**
     * Initiates the design conditions object from an array of strings.
     *
     * @param args array of strings
     */
    public EPW_Design_Conditions(String[] args) {
        // args[0] is the object id
        this.N1_number_of_design_conditions = Integer.parseInt(args[1]);
        this.An_Design_Condition_Sources = new String[args.length - 2];
        for (int i = 2; i < args.length; i++) {
            this.An_Design_Condition_Sources[i - 2] = args[i];
        }
    }

    /**
     * Returns this design conditions object as a string.
     *
     * @return string
     */
    public String getString() {
        String line = "" + this.N1_number_of_design_conditions;
        for (String s : this.An_Design_Condition_Sources) {
            line += "," + s;
        }
        return ID + "," + line + "\n";
    }

    /**
     * Returns the number of design conditions.
     *
     * @return number of design conditions
     */
    public int getN1_number_of_design_conditions() {
        return N1_number_of_design_conditions;
    }

    /**
     * Returns the array of design conditions.
     *
     * @return array of design conditions
     */
    public String[] getAn_Design_Condition_Sources() {
        return An_Design_Condition_Sources;
    }

    /**
     * Number of Design Conditions.
     *
     * @param N1_number_of_design_conditions Number of design conditions.
     */
    public void setN1_number_of_design_conditions(int N1_number_of_design_conditions) {
        this.N1_number_of_design_conditions = N1_number_of_design_conditions;
    }

    /**
     * List of design conditions.
     *
     * @param An_Design_Condition_Sources List of design conditions.
     */
    public void setAn_Design_Condition_Sources(String[] An_Design_Condition_Sources) {
        this.An_Design_Condition_Sources = An_Design_Condition_Sources;
    }

}
