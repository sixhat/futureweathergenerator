/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

/**
 *
 * @author eugenio
 */
public class EPW_Typical_Extreme_Periods {

    /**
     * EPW object string id.
     */
    public static final String ID = "TYPICAL/EXTREME PERIODS";

    private int N1_number_of_typical_extreme_periods;
    private EPW_Typical_Extreme_Period[] An_typical_extreme_periods;

    /**
     * Initiates the typical extreme periods object.
     *
     * @param number_of_typical_extreme_periods number of extreme periods
     * @param typical_extreme_periods array of extreme periods
     */
    public EPW_Typical_Extreme_Periods(int number_of_typical_extreme_periods, EPW_Typical_Extreme_Period[] typical_extreme_periods) {
        this.N1_number_of_typical_extreme_periods = number_of_typical_extreme_periods;
        this.An_typical_extreme_periods = typical_extreme_periods;
    }

    /**
     * Initiates the typical extreme periods from an array of strings.
     *
     * @param args array of strings
     */
    public EPW_Typical_Extreme_Periods(String[] args) {
        this.N1_number_of_typical_extreme_periods = Integer.parseInt(args[1]);
        this.An_typical_extreme_periods = new EPW_Typical_Extreme_Period[this.N1_number_of_typical_extreme_periods];

        for (int i = 2; i < args.length; i += 4) {
            this.An_typical_extreme_periods[(i - 2) / 4] = new EPW_Typical_Extreme_Period(args[i], args[i + 1], args[i + 2], args[i + 3]);
        }
    }

    /**
     * Returns this typical extreme periods as a string.
     *
     * @return string
     */
    public String getString() {
        String line = "" + this.N1_number_of_typical_extreme_periods;
        for (EPW_Typical_Extreme_Period s : this.An_typical_extreme_periods) {
            line += "," + String.format("%s", s.getA1_Name()) + "," + String.format("%s", s.getA2_Type()) + "," + String.format("%s", s.getA3_start_day()) + "," + String.format("%s", s.getA4_end_day());
        }
        return ID + "," + line + "\n";
    }

    /**
     * Returns the number of typical extreme periods in this object.
     *
     * @return number of typical extreme periods
     */
    public int getN1_number_of_typical_extreme_periods() {
        return N1_number_of_typical_extreme_periods;
    }

    /**
     * Returns an array of the typical extreme periods.
     *
     * @return array of typical extreme periods
     */
    public EPW_Typical_Extreme_Period[] getAn_typical_extreme_periods() {
        return An_typical_extreme_periods;
    }

    /**
     * Number of Typical/Extreme Periods.
     *
     * @param N1_number_of_typical_extreme_periods Number of Typical/Extreme
     * Periods
     */
    public void setN1_number_of_typical_extreme_periods(int N1_number_of_typical_extreme_periods) {
        this.N1_number_of_typical_extreme_periods = N1_number_of_typical_extreme_periods;
    }

    /**
     * List of Typical/Extreme Periods.
     *
     * @param An_typical_extreme_periods List of Typical/Extreme Periods
     */
    public void setAn_typical_extreme_periods(EPW_Typical_Extreme_Period[] An_typical_extreme_periods) {
        this.An_typical_extreme_periods = An_typical_extreme_periods;
    }

    /**
     * Typical extreme period object.
     */
    public class EPW_Typical_Extreme_Period {

        private String A1_Name;
        private String A2_Type;
        private String A3_start_day;
        private String A4_end_day;

        /**
         * Initiates a typical extreme period object.
         *
         * @param A1_Name name of the typical extreme period
         * @param A2_Type type of the typical extreme period
         * @param A3_start_day starting day
         * @param A4_end_day ending day
         */
        public EPW_Typical_Extreme_Period(String A1_Name, String A2_Type, String A3_start_day, String A4_end_day) {
            this.A1_Name = A1_Name;
            this.A2_Type = A2_Type;
            this.A3_start_day = A3_start_day.replace(" ", "");
            this.A4_end_day = A4_end_day.replace(" ", "");
        }

        /**
         * Returns the name.
         *
         * @return name
         */
        public String getA1_Name() {
            return A1_Name;
        }

        /**
         * Returns the type.
         *
         * @return type
         */
        public String getA2_Type() {
            return A2_Type;
        }

        /**
         * Returns starting day.
         *
         * @return starting day
         */
        public String getA3_start_day() {
            return A3_start_day;
        }

        /**
         * Returns ending day.
         *
         * @return ending day
         */
        public String getA4_end_day() {
            return A4_end_day;
        }

        /**
         * Typical/Extreme Period Name.
         *
         * @param A1_Name Typical/Extreme Period Name
         */
        public void setA1_Name(String A1_Name) {
            this.A1_Name = A1_Name;
        }

        /**
         * Typical/Extreme Period Type.
         *
         * @param A2_Type Typical/Extreme Period Type
         */
        public void setA2_Type(String A2_Type) {
            this.A2_Type = A2_Type;
        }

        /**
         * Typical/Extreme Period start day.
         *
         * @param A3_start_day Start day
         */
        public void setA3_start_day(String A3_start_day) {
            this.A3_start_day = A3_start_day;
        }

        /**
         * Typical/Extreme Period end day.
         *
         * @param A4_end_day End day
         */
        public void setA4_end_day(String A4_end_day) {
            this.A4_end_day = A4_end_day;
        }

    }
}
