/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import futureweathergenerator.GCM.AverageScenario;
import futureweathergenerator.Months;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class allows to load, edit, and save EPW files.
 *
 * @author eugenio
 */
public class EPW {

    private final String path_EPW;
    private EPW_Location epw_location;
    private EPW_Design_Conditions epw_design_conditions;
    private EPW_Typical_Extreme_Periods epw_typical_extreme_periods;
    private EPW_Ground_Temperatures epw_ground_temperatures;
    private EPW_Holidays_Daylight_Savings epw_holidays_daylight_savings;
    private ArrayList<EPW_Comment> epw_comments = new ArrayList<>();
    private EPW_Data_Periods epw_data_periods;
    private ArrayList<EPW_Data_Fields> epw_data_fields = new ArrayList<>();

    /**
     * Initiates the EPW class with a path to a EPW file.
     *
     * @param path_EPW path to EPW file
     * @param doEcho prints if it loads EPW file
     */
    public EPW(String path_EPW, boolean doEcho) {
        this.path_EPW = path_EPW;
        if (doEcho) {
            System.out.println("Loading EPW File...\n" + path_EPW);
        }

        try {
            File myObj = new File(path_EPW);
            try ( Scanner myReader = new Scanner(myObj)) {
                int commentsCount = 1;
                while (myReader.hasNextLine()) {
                    String line = myReader.nextLine();
                    String[] fields = line.split(",");
                    switch (fields[0]) {
                        case EPW_Location.ID -> {
                            epw_location = new EPW_Location(fields);
                        }
                        case EPW_Typical_Extreme_Periods.ID -> {
                            epw_typical_extreme_periods = new EPW_Typical_Extreme_Periods(fields);
                        }
                        case EPW_Design_Conditions.ID -> {
                            epw_design_conditions = new EPW_Design_Conditions(fields);
                        }
                        case EPW_Ground_Temperatures.ID -> {
                            epw_ground_temperatures = new EPW_Ground_Temperatures(fields);
                        }
                        case EPW_Holidays_Daylight_Savings.ID -> {
                            epw_holidays_daylight_savings = new EPW_Holidays_Daylight_Savings(fields);
                        }
                        case EPW_Data_Periods.ID -> {
                            epw_data_periods = new EPW_Data_Periods(fields);
                        }
                        default -> {
                            if (fields[0].split(" ")[0].equals(EPW_Comment.ID)) {
                                epw_comments.add(new EPW_Comment(commentsCount, fields[1]));
                                commentsCount++;
                            } else {
                                epw_data_fields.add(new EPW_Data_Fields(fields));
                            }

                        }
                    }

                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + e);
        }
    }

    /**
     * Returns the EPW content in a string.
     *
     * @return string
     */
    public String getString() {
        StringBuilder sb = new StringBuilder();
        sb.append(epw_location.getString());
        sb.append(epw_design_conditions.getString());
        sb.append(epw_typical_extreme_periods.getString());
        sb.append(epw_ground_temperatures.getString());
        sb.append(epw_holidays_daylight_savings.getString());
        for (int i = 0; i < epw_comments.size(); i++) {
            sb.append(epw_comments.get(i).getString());
        }
        sb.append(epw_data_periods.getString());
        for (int i = 0; i < epw_data_fields.size(); i++) {
            sb.append(epw_data_fields.get(i).getString());
        }
        return sb.toString().replace(",-0,", ",0,").replace(",-0.0,", ",0.0,");
    }

    /**
     * Returns the path to the EPW file.
     *
     * @return path to EPW file
     */
    public String getPath_EPW() {
        return path_EPW;
    }

    /**
     * Returns the EPW location object.
     *
     * @return EPW location object
     */
    public EPW_Location getEpw_location() {
        return epw_location;
    }

    /**
     * Returns the EPW design conditions object.
     *
     * @return EPW design conditions object.
     */
    public EPW_Design_Conditions getEpw_design_conditions() {
        return epw_design_conditions;
    }

    /**
     * Returns the EPW typical/extreme periods object.
     *
     * @return EPW typical/extreme periods object
     */
    public EPW_Typical_Extreme_Periods getEpw_typical_extreme_periods() {
        return epw_typical_extreme_periods;
    }

    /**
     * Returns the EPW ground temperatures object.
     *
     * @return EPW ground temperatures
     */
    public EPW_Ground_Temperatures getEpw_ground_temperatures() {
        return epw_ground_temperatures;
    }

    /**
     * Returns the EPW holidays and daylight savings object.
     *
     * @return EPW holidays and daylight savings object
     */
    public EPW_Holidays_Daylight_Savings getEpw_holidays_daylight_savings() {
        return epw_holidays_daylight_savings;
    }

    /**
     * Return the EPW comments object.
     *
     * @return EPW comments object
     */
    public ArrayList<EPW_Comment> getEpw_comments() {
        return epw_comments;
    }

    /**
     * Returns the EPW data periods object.
     *
     * @return EPW data periods object
     */
    public EPW_Data_Periods getEpw_data_periods() {
        return epw_data_periods;
    }

    /**
     * Returns the EPW data fields objects list. Each element in the list is an
     * EPW data fields object.
     *
     * @return list of EPW data fields.
     */
    public ArrayList<EPW_Data_Fields> getEpw_data_fields() {
        return epw_data_fields;
    }

    /**
     * Sets the EPW location object.
     *
     * @param epw_location EPW location object
     */
    public void setEpw_location(EPW_Location epw_location) {
        this.epw_location = epw_location;
    }

    /**
     * Sets the EPW design conditions object.
     *
     * @param epw_design_conditions EPW design conditions
     */
    public void setEpw_design_conditions(EPW_Design_Conditions epw_design_conditions) {
        this.epw_design_conditions = epw_design_conditions;
    }

    /**
     * Sets the EPW typical/extreme periods object.
     *
     * @param epw_typical_extreme_periods EPW typical/extreme periods object
     */
    public void setEpw_typical_extreme_periods(EPW_Typical_Extreme_Periods epw_typical_extreme_periods) {
        this.epw_typical_extreme_periods = epw_typical_extreme_periods;
    }

    /**
     * Sets the EPW ground temperatures object.
     *
     * @param epw_ground_temperatures EPW ground temperatures
     */
    public void setEpw_ground_temperatures(EPW_Ground_Temperatures epw_ground_temperatures) {
        this.epw_ground_temperatures = epw_ground_temperatures;
    }

    /**
     * Sets the EPW holidays and daylight savings object.
     *
     * @param epw_holidays_daylight_savings EPW holidays and daylight savings
     */
    public void setEpw_holidays_daylight_savings(EPW_Holidays_Daylight_Savings epw_holidays_daylight_savings) {
        this.epw_holidays_daylight_savings = epw_holidays_daylight_savings;
    }

    /**
     * Sets the EPW comments object.
     *
     * @param epw_comments EPW comments
     */
    public void setEpw_comments(ArrayList<EPW_Comment> epw_comments) {
        this.epw_comments = epw_comments;
    }

    /**
     * Sets EPW data periods object.
     *
     * @param epw_data_periods EPW data periods
     */
    public void setEpw_data_periods(EPW_Data_Periods epw_data_periods) {
        this.epw_data_periods = epw_data_periods;
    }

    /**
     * Sets the list of EPW data fields. Each element of the list is an EPW data
     * fields object.
     *
     * @param epw_data_fields list of EPW data fields
     */
    public void setEpw_data_fields(ArrayList<EPW_Data_Fields> epw_data_fields) {
        this.epw_data_fields = epw_data_fields;
    }

    /**
     * Saves a comparison between this EPW and another given EPW file.The
     * comparison is the monthly daily average of the morphed variables.
     *
     * @param printOutputs String buffer
     * @param path_output path to the output folder
     * @param epw given EPW
     */
    public void saveEPWComparison(StringBuffer printOutputs, String path_output, EPW epw) {
        printOutputs.append("\n\tSaving comparison between the two EPW files...\n\t").append(path_output).append("\n");
        StringBuilder comparison = new StringBuilder();
        comparison.append("# Monthly average difference between variables of two EPW files (last EPW variables minus the first EPW variables).\n");
        comparison.append("Variable,Unit,Description,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec\n");
        for (int i = 0; i < this.getEpw_ground_temperatures().getNn_ground_temperatures_depth().length; i++) {
            Object[] args = new Object[]{
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN1_depth(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN5_January_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN5_January_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN6_February_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN6_February_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN7_March_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN7_March_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN8_April_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN8_April_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN9_May_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN9_May_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN10_June_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN10_June_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN11_July_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN11_July_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN12_August_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN12_August_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN13_September_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN13_September_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN14_October_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN14_October_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN15_November_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN15_November_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN16_December_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN16_December_average_ground_temperature()
            };
            comparison.append(String.format(Locale.ROOT, "Ground Temperatures,∆°C,depth %.1f m,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", args));
        }

        double[] thisEPWaverage_N6 = new double[12], otherEPWaverage_N6 = new double[12];
        double[] thisEPWaverage_N7 = new double[12], otherEPWaverage_N7 = new double[12];
        double[] thisEPWaverage_N8 = new double[12], otherEPWaverage_N8 = new double[12];
        double[] thisEPWaverage_N9 = new double[12], otherEPWaverage_N9 = new double[12];
        double[] thisEPWaverage_N10 = new double[12], otherEPWaverage_N10 = new double[12];
        double[] thisEPWaverage_N11 = new double[12], otherEPWaverage_N11 = new double[12];
        double[] thisEPWaverage_N12 = new double[12], otherEPWaverage_N12 = new double[12];
        double[] thisEPWaverage_N13 = new double[12], otherEPWaverage_N13 = new double[12];
        double[] thisEPWaverage_N14 = new double[12], otherEPWaverage_N14 = new double[12];
        double[] thisEPWaverage_N15 = new double[12], otherEPWaverage_N15 = new double[12];
        double[] thisEPWaverage_N16 = new double[12], otherEPWaverage_N16 = new double[12];
        double[] thisEPWaverage_N17 = new double[12], otherEPWaverage_N17 = new double[12];
        double[] thisEPWaverage_N18 = new double[12], otherEPWaverage_N18 = new double[12];
        double[] thisEPWaverage_N19 = new double[12], otherEPWaverage_N19 = new double[12];
        double[] thisEPWaverage_N21 = new double[12], otherEPWaverage_N21 = new double[12];
        double[] thisEPWaverage_N22 = new double[12], otherEPWaverage_N22 = new double[12];
        double[] thisEPWaverage_N23 = new double[12], otherEPWaverage_N23 = new double[12];
        double[] thisEPWaverage_N28 = new double[12], otherEPWaverage_N28 = new double[12];

        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            double number_of_rows_in_month = month_row_ids[0];
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                thisEPWaverage_N6[i] += this.epw_data_fields.get(row_id).getN6_dry_bulb_temperature();
                otherEPWaverage_N6[i] += epw.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                thisEPWaverage_N7[i] += this.epw_data_fields.get(row_id).getN7_dew_point_temperature();
                otherEPWaverage_N7[i] += epw.getEpw_data_fields().get(row_id).getN7_dew_point_temperature();
                thisEPWaverage_N8[i] += this.epw_data_fields.get(row_id).getN8_relative_humidity();
                otherEPWaverage_N8[i] += epw.getEpw_data_fields().get(row_id).getN8_relative_humidity();
                thisEPWaverage_N9[i] += this.epw_data_fields.get(row_id).getN9_atmospheric_station_pressure();
                otherEPWaverage_N9[i] += epw.getEpw_data_fields().get(row_id).getN9_atmospheric_station_pressure();
                thisEPWaverage_N10[i] += this.epw_data_fields.get(row_id).getN10_extraterrestrial_horizontal_radiation();
                otherEPWaverage_N10[i] += epw.getEpw_data_fields().get(row_id).getN10_extraterrestrial_horizontal_radiation();
                thisEPWaverage_N11[i] += this.epw_data_fields.get(row_id).getN11_extraterrestrial_direct_normal_radiation();
                otherEPWaverage_N11[i] += epw.getEpw_data_fields().get(row_id).getN11_extraterrestrial_direct_normal_radiation();
                thisEPWaverage_N12[i] += this.epw_data_fields.get(row_id).getN12_horizontal_infrared_radiation_intensity();
                otherEPWaverage_N12[i] += epw.getEpw_data_fields().get(row_id).getN12_horizontal_infrared_radiation_intensity();
                thisEPWaverage_N13[i] += this.epw_data_fields.get(row_id).getN13_global_horizontal_radiation();
                otherEPWaverage_N13[i] += epw.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
                thisEPWaverage_N14[i] += this.epw_data_fields.get(row_id).getN14_direct_normal_radiation();
                otherEPWaverage_N14[i] += epw.getEpw_data_fields().get(row_id).getN14_direct_normal_radiation();
                thisEPWaverage_N15[i] += this.epw_data_fields.get(row_id).getN15_diffuse_horizontal_radiation();
                otherEPWaverage_N15[i] += epw.getEpw_data_fields().get(row_id).getN15_diffuse_horizontal_radiation();

                thisEPWaverage_N16[i] += this.epw_data_fields.get(row_id).getN16_global_horizontal_illuminance();
                otherEPWaverage_N16[i] += epw.getEpw_data_fields().get(row_id).getN16_global_horizontal_illuminance();
                thisEPWaverage_N17[i] += this.epw_data_fields.get(row_id).getN17_direct_normal_illuminance();
                otherEPWaverage_N17[i] += epw.getEpw_data_fields().get(row_id).getN17_direct_normal_illuminance();
                thisEPWaverage_N18[i] += this.epw_data_fields.get(row_id).getN18_diffuse_horizontal_illuminance();
                otherEPWaverage_N18[i] += epw.getEpw_data_fields().get(row_id).getN18_diffuse_horizontal_illuminance();
                thisEPWaverage_N19[i] += this.epw_data_fields.get(row_id).getN19_zenith_luminance();
                otherEPWaverage_N19[i] += epw.getEpw_data_fields().get(row_id).getN19_zenith_luminance();

                thisEPWaverage_N21[i] += this.epw_data_fields.get(row_id).getN21_wind_speed();
                otherEPWaverage_N21[i] += epw.getEpw_data_fields().get(row_id).getN21_wind_speed();
                thisEPWaverage_N22[i] += this.epw_data_fields.get(row_id).getN22_total_sky_cover();
                otherEPWaverage_N22[i] += epw.getEpw_data_fields().get(row_id).getN22_total_sky_cover();
                thisEPWaverage_N23[i] += this.epw_data_fields.get(row_id).getN23_opaque_sky_cover();
                otherEPWaverage_N23[i] += epw.getEpw_data_fields().get(row_id).getN23_opaque_sky_cover();
                thisEPWaverage_N28[i] += this.epw_data_fields.get(row_id).getN28_precipitable_water();
                otherEPWaverage_N28[i] += epw.getEpw_data_fields().get(row_id).getN28_precipitable_water();
            }

            thisEPWaverage_N6[i] /= number_of_rows_in_month;
            otherEPWaverage_N6[i] /= number_of_rows_in_month;
            thisEPWaverage_N7[i] /= number_of_rows_in_month;
            otherEPWaverage_N7[i] /= number_of_rows_in_month;
            thisEPWaverage_N8[i] /= number_of_rows_in_month;
            otherEPWaverage_N8[i] /= number_of_rows_in_month;
            thisEPWaverage_N9[i] /= number_of_rows_in_month;
            otherEPWaverage_N9[i] /= number_of_rows_in_month;
            thisEPWaverage_N10[i] /= number_of_rows_in_month;
            otherEPWaverage_N10[i] /= number_of_rows_in_month;
            thisEPWaverage_N11[i] /= number_of_rows_in_month;
            otherEPWaverage_N11[i] /= number_of_rows_in_month;
            thisEPWaverage_N12[i] /= number_of_rows_in_month;
            otherEPWaverage_N12[i] /= number_of_rows_in_month;
            thisEPWaverage_N13[i] /= number_of_rows_in_month;
            otherEPWaverage_N13[i] /= number_of_rows_in_month;
            thisEPWaverage_N14[i] /= number_of_rows_in_month;
            otherEPWaverage_N14[i] /= number_of_rows_in_month;
            thisEPWaverage_N15[i] /= number_of_rows_in_month;
            otherEPWaverage_N15[i] /= number_of_rows_in_month;
            thisEPWaverage_N16[i] /= number_of_rows_in_month;
            otherEPWaverage_N16[i] /= number_of_rows_in_month;
            thisEPWaverage_N17[i] /= number_of_rows_in_month;
            otherEPWaverage_N17[i] /= number_of_rows_in_month;
            thisEPWaverage_N18[i] /= number_of_rows_in_month;
            otherEPWaverage_N18[i] /= number_of_rows_in_month;
            thisEPWaverage_N19[i] /= number_of_rows_in_month;
            otherEPWaverage_N19[i] /= number_of_rows_in_month;
            thisEPWaverage_N21[i] /= number_of_rows_in_month;
            otherEPWaverage_N21[i] /= number_of_rows_in_month;
            thisEPWaverage_N22[i] /= number_of_rows_in_month;
            otherEPWaverage_N22[i] /= number_of_rows_in_month;
            thisEPWaverage_N23[i] /= number_of_rows_in_month;
            otherEPWaverage_N23[i] /= number_of_rows_in_month;
            thisEPWaverage_N28[i] /= number_of_rows_in_month;
            otherEPWaverage_N28[i] /= number_of_rows_in_month;
        }

        addComparisonLine(comparison, "N6 Dry Bulb Temperature", "∆°C", "-", thisEPWaverage_N6, otherEPWaverage_N6);
        addComparisonLine(comparison, "N7 Dew Point Temperature", "∆°C", "-", thisEPWaverage_N7, otherEPWaverage_N7);
        addComparisonLine(comparison, "N8 Relative Humidity", "∆%", "-", thisEPWaverage_N8, otherEPWaverage_N8);
        addComparisonLine(comparison, "N9 Atmospheric Station Pressure", "∆Pa", "-", thisEPWaverage_N9, otherEPWaverage_N9);
        addComparisonLine(comparison, "N10 Extraterrestrial Horizontal Radiation", "∆Wh/m²", "-", thisEPWaverage_N10, otherEPWaverage_N10);
        addComparisonLine(comparison, "N11 Extraterrestrial Direct Normal Radiation", "∆Wh/m²", "-", thisEPWaverage_N11, otherEPWaverage_N11);
        addComparisonLine(comparison, "N12 Horizontal Infrared Radiation Intensity", "∆Wh/m²", "-", thisEPWaverage_N12, otherEPWaverage_N12);
        addComparisonLine(comparison, "N13 Global Horizontal Radiation", "∆Wh/m²", "-", thisEPWaverage_N13, otherEPWaverage_N13);
        addComparisonLine(comparison, "N14 Direct Normal Radiation", "∆Wh/m²", "-", thisEPWaverage_N14, otherEPWaverage_N14);
        addComparisonLine(comparison, "N15 Diffuse Horizontal Radiation", "∆Wh/m²", "-", thisEPWaverage_N15, otherEPWaverage_N15);
        addComparisonLine(comparison, "N16 Global Horizontal Illuminance", "∆lux", "-", thisEPWaverage_N16, otherEPWaverage_N16);
        addComparisonLine(comparison, "N17 Direct Normal Illuminance", "∆lux", "-", thisEPWaverage_N17, otherEPWaverage_N17);
        addComparisonLine(comparison, "N18 Diffuse Horizontal Illuminance", "∆lux", "-", thisEPWaverage_N18, otherEPWaverage_N18);
        addComparisonLine(comparison, "N19 Zenith Luminance", "∆Cd/m²", "-", thisEPWaverage_N19, otherEPWaverage_N19);
        addComparisonLine(comparison, "N21 Wind Speed", "∆m/s", "-", thisEPWaverage_N21, otherEPWaverage_N21);
        addComparisonLine(comparison, "N22 Total Sky Cover", "∆deca", "-", thisEPWaverage_N22, otherEPWaverage_N22);
        addComparisonLine(comparison, "N23 Opaque Sky Cover", "∆deca", "-", thisEPWaverage_N23, otherEPWaverage_N23);
        addComparisonLine(comparison, "N28 Precipitable Water", "∆mm", "-", thisEPWaverage_N23, otherEPWaverage_N23);

        try ( OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            writer.write(comparison.toString());
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AverageScenario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AverageScenario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addComparisonLine(StringBuilder comparison, String name, String unit, String description, double[] thisEPWaverageVariable, double[] otherEPWAverageVariable) {
        Object[] args = new Object[]{
            name,
            unit,
            description,
            thisEPWaverageVariable[0] - otherEPWAverageVariable[0],
            thisEPWaverageVariable[1] - otherEPWAverageVariable[1],
            thisEPWaverageVariable[2] - otherEPWAverageVariable[2],
            thisEPWaverageVariable[3] - otherEPWAverageVariable[3],
            thisEPWaverageVariable[4] - otherEPWAverageVariable[4],
            thisEPWaverageVariable[5] - otherEPWAverageVariable[5],
            thisEPWaverageVariable[6] - otherEPWAverageVariable[6],
            thisEPWaverageVariable[7] - otherEPWAverageVariable[7],
            thisEPWaverageVariable[8] - otherEPWAverageVariable[8],
            thisEPWaverageVariable[9] - otherEPWAverageVariable[9],
            thisEPWaverageVariable[10] - otherEPWAverageVariable[10],
            thisEPWaverageVariable[11] - otherEPWAverageVariable[11]
        };
        comparison.append(String.format(Locale.ROOT, "%s,%s,%s,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", args));
    }

    /**
     * Saves the EPW object as a EPW file (.epw) to the given path.
     *
     * @param printOutputs String buffer
     * @param path_output path to the file
     */
    public void saveEPW(StringBuffer printOutputs, String path_output) {
        printOutputs.append("\n\tSaving morphed EPW file...\n\t").append(path_output).append("\n");
        try ( OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            writer.write(this.getString());
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AverageScenario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AverageScenario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
